
<div class="title"><?php echo ($id > 0) ? 'Edit '.$title : 'New '.$title; ?></div>
<?php 
$attributes = array('class'=>'eventform');
echo form_open_multipart(base_url().'admin/tripplan/add/'.$id, $attributes);
if($error != '') $error = $error;
?>
<script type="text/javascript">
	$(document).ready(function(){
		$(".txtFromDate").datepicker({
			minDate: 0,
			maxDate: "+1530D",
			numberOfMonths: 1,
			onSelect: function(selected) {
			  $(".txtToDate").datepicker("option","minDate", selected)
			}
		});
		$(".txtToDate").datepicker({ 
			minDate: 0,
			maxDate:"+1530D",
			numberOfMonths: 1,
			onSelect: function(selected) {
			   $(".txtFromDate").datepicker("option","maxDate", selected)
			}
		});
	});
	/*tinymce.init({
			mode : "textareas",
			theme : "advanced",
			plugins : "autolink,lists,spellchecker,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template",	
			// Theme options
			theme_advanced_buttons1 : "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,styleselect,formatselect,fontselect,fontsizeselect",
			theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,insertdate,inserttime,preview,|,forecolor,backcolor",
			theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen",
			theme_advanced_buttons4 : "insertlayer,moveforward,movebackward,absolute,|,styleprops,spellchecker,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,blockquote,pagebreak,|,insertfile,insertimage",
			theme_advanced_toolbar_location : "top",
			theme_advanced_toolbar_align : "left",
			theme_advanced_statusbar_location : "bottom",
			theme_advanced_resizing : true,
	
			// Skin options
			skin : "o2k7",
			skin_variant : "silver",	
			// Example content CSS (should be your site CSS)
			content_css : "css/example.css",	
			// Drop lists for link/image/media/template dialogs
			template_external_list_url : "js/template_list.js",
			external_link_list_url : "js/link_list.js",
			external_image_list_url : "js/image_list.js",
			media_external_list_url : "js/media_list.js",	
			// Replace values for the template plugin
			template_replace_values : {
					username : "Some User",
					staffid : "991234"
			}		
         });*/
		 
	tinymce.init({
		mode : "exact",
		elements : "desc",
		forced_root_block: false,
	});
</script>
<table style="width:710px !important;">
	<tr><td class="star_red" colspan="3"><?php echo $error; ?></td><td></td></tr>
    <?php
    if($id > 0){
       // var_dump($fName);
	?>
    <tr>
    	<td>ID : </td>
        <td><?php echo $id; ?></td>
    </tr>
    <?php
	}
	?>
	<tr>    
    	<td style="padding-right:76px !important;"><?php echo $name; ?></td>
        <td><?php echo form_input($fName);
		          echo form_error('name', '<div class="error">', '</div>');
			?></td>
    </tr>
    <tr>
    	<td><?php echo $city; ?></td>
        <td><?php echo form_dropdown('city', $allCity, $selectedCity); ?></td>
    </tr>
     <tr>
    	<td valign="top"><?php echo $image; ?></td>
        <td><?php echo form_upload($fImage). ' '.$fileName; ?></td>
    </tr>
    <tr>
    	<td><?php echo $desc; ?></td>
        <td><?php echo form_textarea($fDesc); 
				  echo form_error('desc', '<div class="error">', '</div>');
		?></td>
    </tr>
    <tr>    
    	<td><?php echo $cDate; ?></td>
        <td><?php echo form_input($fCdate);
				  echo form_error('cDate', '<div class="error">', '</div>');
		?></td>
    </tr>
    <tr>
    	<td><?php echo $uDate; ?></td>
        <td><?php echo form_input($fUdate);
				  echo form_error('uDate', '<div class="error">', '</div>');
		?></td>
    </tr>
    <tr>
        <td colspan="2">
        <?php
		$submit = array('name' => 'submit', 'content'=> 'Save', 'id'=> 'submit','type'=> 'submit', 'value' => 'true', 'onclick'=>'', 'class'=>'btn btn-primary');
		echo form_button($submit);
		$cancel = array('name' => 'cancel', 'content'=> 'Cancel', 'id'=> 'submit','type'=> 'submit', 'value' => 'true', 'class'=>'btn');
		echo form_button($cancel);
		?>
        </td>
    </tr>
</table>
<?php echo form_close(); ?>