<div class="single">
			<div class="col-md-8 sing-img-text">
				<img src="<?php echo $info[0]['image_url']; ?>" alt=" ">
				<div class="sing-img-text1">
					<h3><?php echo $info[0]['name']; ?></h3>
					<p class="est"><?php echo $info[0]['description']; ?></p>
					<div class="list">
						<ul>
							<li><a href="#" class="a"> </a></li>
							<li><a href="#" class="b"> </a></li>
							<li><a href="#" class="c"> </a></li>
						</ul>
					</div>
					<!--<div class="com">
						<h3>Comments</h3>
						<ul class="media-list">
						  <li class="media">
							<div class="media-left">
							  <a href="#">
								<img class="media-object" src="images/11.jpg" alt="" />
							  </a>
							</div>
							<div class="media-body">
							  <h4 class="media-heading">Simmy</h4>
							  Cras sit amet nibh libero, in gravida nulla. Nulla vel metus 
							  scelerisque ante sollicitudin commodo. Cras purus odio, 
							  vestibulum in vulputate at, tempus viverra turpis. 
							  Fusce condimentum nunc ac nisi vulputate fringilla. 
							  Donec lacinia congue felis in faucibus.
							  <a href="#">Reply</a>
							</div>
						  </li>
						  <li class="media">
							<div class="media-left">
							  <a href="#">
								<img class="media-object" src="images/13.jpg" alt="" />
							  </a>
							</div>
							<div class="media-body">
							  <h4 class="media-heading">Sandra Rickon</h4>
							  Cras sit amet nibh libero, in gravida nulla. Nulla vel metus 
							  scelerisque ante sollicitudin commodo. Cras purus odio, 
							  vestibulum in vulputate at, tempus viverra turpis. 
							  Fusce condimentum nunc ac nisi vulputate fringilla. 
							  Donec lacinia congue felis in faucibus.
							  <a href="#">Reply</a>
							</div>
						  </li>
						  <li class="media">
							<div class="media-left">
							  <a href="#">
								<img class="media-object" src="images/12.jpg" alt="" />
							  </a>
							</div>
							<div class="media-body">
							  <h4 class="media-heading">Rita Rider</h4>
							  Cras sit amet nibh libero, in gravida nulla. Nulla vel metus 
							  scelerisque ante sollicitudin commodo. Cras purus odio, 
							  vestibulum in vulputate at, tempus viverra turpis. 
							  Fusce condimentum nunc ac nisi vulputate fringilla. 
							  Donec lacinia congue felis in faucibus.
							  <a href="#">Reply</a>
							</div>
						  </li>
						</ul>
					</div>-->
					
				</div>
			</div>
			<div class="col-md-4 sing-img-text-left">
				<div class="blog-right1">
					<div class="search">
						<h3>NEWSLETTER</h3>
						<form>
							<input type="text" value="Email..." onFocus="this.value = '';" onBlur="if (this.value == '') {this.value = 'Email...';}" required="">
							<input type="submit" value="Subscribe">
						</form>
					</div>
					<?php
					
					if($type == 'city'){
					?>
					<div class="categories">
						<h3>Categories</h3>
						<ul>
							<?php 
							
							/*if($type == 'city') $id = $id;
							else $id = $info[0]['city'];*/
							?>
							<li><a href="<?php echo base_url(); ?>wheretostay/show/<?php echo $id; ?>">WHERE TO STAY</a></li>
							<li><a href="<?php echo base_url(); ?>wheretovisit/show/<?php echo $id; ?>">WHERE TO VISIT</a></li>
							<li><a href="<?php echo base_url(); ?>wheretoeat/show/<?php echo $id; ?>">WHERE TO EAT</a></li>
							<li><a href="<?php echo base_url(); ?>whattodo/show/<?php echo $id; ?>">WHAT TO DO</a></li>
						</ul>
					</div>
					<?php
					}else if($type == 'information' || $type == 'trip_plan' || $type == 'hotel' || $type == 'attraction' || $type == 'restaurant' || $type == 'activities'){
						if(sizeof($releatedInfo) > 0){
						?>
						<div class="related-posts">
							<h3>Related Posts</h3>
						<?php
							$detailsUrl = '';
							foreach($releatedInfo as $one){
								extract($one);
								if($type == 'information') $detailsUrl = base_url().'Details/show/information/'.$id;
								else if($type == 'trip_plan') $detailsUrl = base_url().'Details/show/tripplan/'.$id;
								else if($type == 'hotel') $detailsUrl = base_url().'Details/show/hotel/'.$id;
								else if($type == 'attraction') $detailsUrl = base_url().'Details/show/attraction/'.$id;
								else if($type == 'restaurant') $detailsUrl = base_url().'Details/show/restaurant/'.$id;
								else if($type == 'activities') $detailsUrl = base_url().'Details/show/activities/'.$id;
							
						?>
							<div class="related-post">
								<div class="related-post-left">
									<a href="<?php echo $detailsUrl; ?>"><img src="<?php echo $image_url; ?>" alt=" " /></a>
								</div>
								<div class="related-post-right">
									<h4><a href="<?php echo $detailsUrl; ?>"><?php echo $name; ?></a></h4>
									<p><?php echo substr($description , 0,60)."...."; ?></p>
								</div>
								<div class="clearfix"> </div>
							</div>
						<?php
							}
						?>
							</div>	
						<?php
						}
					}
					?>
					<!--<div class="categories categories-mid">
						<h3>Archieves</h3>
						<ul>
							<li><a href="#">May 20,2009</a></li>
							<li><a href="#">July 31,2010</a></li>
							<li><a href="#">January 20,2012</a></li>
							<li><a href="#">November 2,2012</a></li>
							<li><a href="#">December 25,2014</a></li>
							<li><a href="#">January 14,2015</a></li>
						</ul>
					</div>-->
					<!--<div class="related-posts">
						<h3>Related Posts</h3>
						<div class="related-post">
							<div class="related-post-left">
								<a href="single.html"><img src="images/3.jpg" alt=" " /></a>
							</div>
							<div class="related-post-right">
								<h4><a href="single.html">Donec sollicitudin</a></h4>
								<p>Aliquam dapibus tincidunt metus. 
									<span>Praesent justo dolor, lobortis.</span>
								</p>
							</div>
							<div class="clearfix"> </div>
						</div>
						<div class="related-post">
							<div class="related-post-left">
								<a href="single.html"><img src="images/4.jpg" alt=" " /></a>
							</div>
							<div class="related-post-right">
								<h4><a href="single.html">Donec sollicitudin</a></h4>
								<p>Aliquam dapibus tincidunt metus. 
									<span>Praesent justo dolor, lobortis.</span>
								</p>
							</div>
							<div class="clearfix"> </div>
						</div>
						<div class="related-post">
							<div class="related-post-left">
								<a href="single.html"><img src="images/5.jpg" alt=" " /></a>
							</div>
							<div class="related-post-right">
								<h4><a href="single.html">Donec sollicitudin</a></h4>
								<p>Aliquam dapibus tincidunt metus. 
									<span>Praesent justo dolor, lobortis.</span>
								</p>
							</div>
							<div class="clearfix"> </div>
						</div>
						<div class="related-post">
							<div class="related-post-left">
								<a href="single.html"><img src="images/6.jpg" alt=" " /></a>
							</div>
							<div class="related-post-right">
								<h4><a href="single.html">Donec sollicitudin</a></h4>
								<p>Aliquam dapibus tincidunt metus. 
									<span>Praesent justo dolor, lobortis.</span>
								</p>
							</div>
							<div class="clearfix"> </div>
						</div>
					</div>-->
				</div>
			</div>
			<div class="clearfix"> </div>
			<!--<div class="leave-a-comment">
				<h3>Leave your comment Here</h3> 
				<form>
					<input type="text" value="Name" onFocus="this.value = '';" onBlur="if (this.value == '') {this.value = 'Name';}" required="">
					<input type="text" value="Email..." onFocus="this.value = '';" onBlur="if (this.value == '') {this.value = 'Email...';}" required="">
					<input type="text" value="Phone Number" onFocus="this.value = '';" onBlur="if (this.value == '') {this.value = 'Phone Number';}" required="">
					<textarea type="text" onFocus="this.value = '';" onBlur="if (this.value == '') {this.value = 'Type Your Comment here...';}" required="">Type Your Comment here...</textarea>
					<input type="submit" value="Add Comment">
				</form>
			</div>-->
		</div>