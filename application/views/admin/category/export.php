<?php	
header("Content-type: application/csv");
header("Content-Disposition: attachment; filename=".$fileName.'.csv');
$output= '';
$output .= 'id,name,code,description,updated_date,created_date';
$output .="\r\n";
if(is_array($category) && sizeof($category) > 0){
	foreach($category as $key=>$value){	
		$created_date = redatetime($value['created_date']);
		$updated_date = redatetime($value['updated_date']);	
		$desc = strip_tags($value['description']);
		$desc = str_replace(",","-",$desc);
		$output .= $value['id'].','.$value['name'].','.$value['code'].','.$desc.','.$updated_date.','.$created_date;
		$output .="\r\n";
	}
}
echo $output;
exit;
?>
