<?php
/**	
 * @file
 *  Program		: info_model.php 
 * 	Author		: CMS
 * 	Date		: 20/07/2015
 * 	Abstract	: info
*/
class Info_model extends CI_Model{
	function attraction_model(){
		parent::__construct();
	}
	// create form
	function form(){
		$data['city']    ='city <span class="star_red">*</span> :';		
		$data['tag']	    = 'Tag <span class="star_red">*</span> :';
		$data['name']		='Name <span class="star_red">*</span>:';
		$data['category']		='Category <span class="star_red">*</span>:';
		$data['desc']	='Description <span class="star_red">*</span> :';	
		$data['longitude']	='Longitude <span class="star_red">*</span> :';
		$data['latitude']	='Latitude <span class="star_red">*</span> :';
		$data['image']	='Image <span class="star_red">*</span> :';	
		$data['cDate']     ='created Date <span class="star_red">*</span> :';
		$data['uDate']     ='updated Date <span class="star_red">*</span> :';
		$data['fImage']	= array('name'=>'upload', 'id'=>'upload', 'value'=>'');
		$data['selectedTag'] = '';
		$data['selectedCategory'] = '';
		$data['selectedCity'] 		= '';
		$data['fName']		= array('name'=>'name', 'id'=>'name', 'value'=>'', 'rows'=>'8');
		$data['fDesc']= array('name'=>'desc', 'id'=>'desc', 'value'=>'', 'rows'=>'8');
		$data['fLongitude']= array('name'=>'longitude', 'id'=>'longitude', 'value'=>'', 'rows'=>'5');
		$data['fLatitude']	= array('name'=>'latitude', 'id'=>'latitude', 'value'=>'', 'rows'=>'5');
		$data['fCdate']	= array('name'=>'cDate', 'id'=>'cDate', 'placeholder'=>'dd/mm/yyyy', 'value'=>'', 'class'=>'txtFromDate c_in');
		$data['fUdate']	= array('name'=>'uDate', 'id'=>'uDate', 'placeholder'=>'dd/mm/yyyy', 'value'=>'', 'class'=>'txtToDate c_out');
		return $data;
	}
	// save info data
	function save($id,$filepath){
		$finaltagid = '';
		$tagid = '';
		$tags = $this->input->post('tags');
		$insertresult = false;
		foreach($tags as $key){
			$query = $this->db->get_where('tag',array('keyword'=>$key));
			if($query->num_rows() > 0){
				$result = $query->result_array();
				$tagid = $result[0]['id'];
			}else{
				$data = array('keyword'=>$key,'created_date'=>date('Y-m-d'),'updated_date'=>date('Y-m-d'));
				$queryresult = $this->db->insert('tag', $data);
				$insertresult = true;
			}
			if($insertresult){
				$tagid = $this->db->insert_id();
				$insertresult = false;
			}
			 $finaltagid .= $tagid.",";
		}
		$name 		= $this->input->post('name');
		$category 	= $this->input->post('category');
		$desc 		= $this->input->post('desc');
		$longitude 	= $this->input->post('longitude');
		$latitude 	= $this->input->post('latitude');
		$tag		= $finaltagid;
		$city		= $this->input->post('city');
		$cDate 		= $this->input->post('cDate');
		$uDate 		= $this->input->post('uDate');
		if(!empty($cDate)){
			$created = explode('/', $cDate);	
			$cDate = $created[2].'-'.$created[1].'-'.$created[0];
		}
		if(!empty($uDate)){
			$updated = explode('/', $uDate);	
			$uDate = $updated[2].'-'.$updated[1].'-'.$updated[0];
		}
		$data = array(
					'category'=>$category, 
					'name'=>$name, 
					'description'=>$desc,
					'longitude'=>$longitude,  
					'latitude'=>$latitude, 
					'city'=>$city,
					'image_url'=>$filepath, 
					'tag'=>$tag, 
					'created_date'=>$cDate,
					'updated_date'=>$uDate
				);
		
		if(!empty($id) && $id > 0){
			$query1= $this->db->get_where('information',array('id'=>$id));
			$result = $query1->result_array();			
			if($filepath != $result[0]['image_url']){
				$img = explode('upload/',$result[0]['image_url']);
				$file = $_SERVER['DOCUMENT_ROOT'] . '/travel/images/info/upload/' . $img[1];
				if (file_exists($file)) {
					unlink($file);
				}
			}
			$this->db->where('id',$id);
			$result = $this->db->update('information', $data);	
		}else{
				$query = $this->db->get_where('information',array('category'=>$name));
				if($query->num_rows() > 0){
					$result = false;
				}else{
					$result = $this->db->insert('information', $data);	
				}
		}
		
		if($result){
			return $result;
		}else{
			return false;
		}
	}	
	// delete function
	function delete($id){
		$query1= $this->db->get_where('information',array('id'=>$id));
		$result = $query1->result_array();
		if($result){
			$img = explode('upload/',$result[0]['image_url']);
			$file = $_SERVER['DOCUMENT_ROOT'] . '/travel/images/info/upload/' . $img[1];
			if (file_exists($file)) {
			unlink($file);
			}
		}
		$this->db->where('id',$id);
		$query = $this->db->delete('information');
		if($query){
			return TRUE;
		}else{
			return FALSE;
		}
	}
}
?>