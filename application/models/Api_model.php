<?php
/**	
 * @file
 *  Program		: api_model.php 
 * 	Author		: ETK
 * 	Date		: 23/05/2014
 * 	Abstract	: api
*/
class Api_model extends CI_Model{
	function api_model(){
		parent::__construct();
		$this->load->library('phpass');
	}
	function checkToken($token=false){
		$check = false;
		if($token == false) $token = $this->input->post('token');
		$query = $this->db->get_where('user_token',array('user_token'=>$token));
		if($query->num_rows() > 0){
			$check = true;
		}
		return $check;
	}
	function checkDate(){
		$ludate = $this->input->post('date');  //date format must be d/m/y
		$check = false;
		if(!empty($ludate)){
			if (strpos($ludate,'/') !== false) {
    			$ludate = explode('/',$ludate);	
				$ludate = $ludate[2].'-'.$ludate[1].'-'.$ludate[0];				
				if(preg_match('/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/', $ludate)){
					$check = true;
				}
			}			
		}else{
			$check = true;
		}
		return $check;
	}
	
	
	function getcity(){
		$date = $this->input->post('date');
		$detail = $this->input->post('detail');
		if(!empty($date)){
			$ludate = datetime($this->input->post('date')); // 2015-7-23
		}else{
			$ludate = 0;
		}
		$city_id = $this->input->post('city_id');
		if(!empty($city_id)){
			$query_city = $this->db->get_where('city',array('id'=>$city_id));
		}else{		
			$query_city = $this->db->get('city');
		}
		$result = $query_city->result_array();
		$finalresult = array();	
		if(!empty($detail) && $detail == 1){
			foreach($result as $key=>$value){			
				$id = $result[$key]['id'];	
				// for attraction
				$attraction_query = $this->db->get_where('attraction',array('city'=>$id,'updated_date >'=>$ludate));
				$attraction_result = $attraction_query->result_array();			
				foreach($attraction_result as $key1=>$value1){
					$tagids = $attraction_result[$key1]['tag'];
					$tagids = explode(',',$tagids);
					$keyword = '';
					foreach($tagids as $tagid){
						$tag_query = $this->db->get_where('tag',array('id'=>$tagid));
						$tag_result = $tag_query->result_array();
						if($tag_result){
						$keyword []= array('id'=>$tagid,'keyword'=>$tag_result[0]['keyword']);
						}
					}
					$attraction_result[$key1]['tag'] = $keyword;			
				}	
				$result[$key]['attraction'] = $attraction_result;
				//for activities
				$activities_query = $this->db->get_where('activities',array('city'=>$id,'updated_date >'=>$ludate));
				$activities_result = $activities_query->result_array();			
				foreach($activities_result as $key3=>$value3){
					$tagids = $activities_result[$key3]['tag'];
					$tagids = explode(',',$tagids);
					$keyword = '';
					foreach($tagids as $tagid){
						$tag_query = $this->db->get_where('tag',array('id'=>$tagid));
						$tag_result = $tag_query->result_array();
						if($tag_result){
						$keyword []= array('id'=>$tagid,'keyword'=>$tag_result[0]['keyword']);
						}
					}
					$activities_result[$key3]['tag'] = $keyword;							
				}	
				$result[$key]['activities'] = $activities_result;	
				// for hotel
				$hotel_query = $this->db->get_where('hotel',array('city'=>$id,'updated_date >'=>$ludate));
				$hotel_result = $hotel_query->result_array();			
				foreach($hotel_result as $key4=>$value4){				
					$tagids = $hotel_result[$key4]['tag'];
					$tagids = explode(',',$tagids);
					$keyword = '';
					foreach($tagids as $tagid){
						$tag_query = $this->db->get_where('tag',array('id'=>$tagid));
						$tag_result = $tag_query->result_array();
						if($tag_result){
						$keyword []= array('id'=>$tagid,'keyword'=>$tag_result[0]['keyword']);
						}
					}
					$hotel_result[$key4]['tag'] = $keyword;
				}	
				$result[$key]['hotel'] = $hotel_result;
				// for restaurant
				$res_query = $this->db->get_where('restaurant',array('city'=>$id,'updated_date >'=>$ludate));
				$res_result = $res_query->result_array();			
				foreach($res_result as $key5=>$value5){
					$tagids = $res_result[$key5]['tag'];
					$tagids = explode(',',$tagids);
					$keyword = '';
					foreach($tagids as $tagid){
						$tag_query = $this->db->get_where('tag',array('id'=>$tagid));
						$tag_result = $tag_query->result_array();
						if($tag_result){
						$keyword []= array('id'=>$tagid,'keyword'=>$tag_result[0]['keyword']);
						}
					}
					$res_result[$key5]['tag'] = $keyword;	
					
				}	
				$result[$key]['restaurant'] = $res_result;				
				// for essential information
				$result_category = gettable('category');
				
				foreach($result_category as $key6=>$value6){			
					$category_id = $result_category[$key6]['id'];				
					$info_query = $this->db->get_where('information',array('category'=>$category_id,'city'=>$id,'updated_date >'=>$ludate));
					$info_result = $info_query->result_array();			
					foreach($info_result as $key7=>$value7){
						$tagids = $info_result[$key7]['tag'];
						$tagids = explode(',',$tagids);
						$keyword = '';
						foreach($tagids as $tagid){
							$tag_query = $this->db->get_where('tag',array('id'=>$tagid));
							$tag_result = $tag_query->result_array();
							if($tag_result){
							$keyword []= array('id'=>$tagid,'keyword'=>$tag_result[0]['keyword']);
							}
						}
						$info_result[$key7]['tag'] = $keyword;
						$info_result[$key7]['map_icon_id'] = $result_category[$key6]['map_icon_id'];				
					}	
					$result_category[$key6]['items'] = $info_result;
					$result[$key]['essential_information'] = $result_category;
				}
								
			}
		}
		if(isset($result)){
			$finalresult['city'] = $result;
		}
		$phrase_query = $this->db->get_where('phrase_book',array('updated_date >'=>$ludate));		
		$phrase_result = $phrase_query->result_array();
		$finalresult['others'] ['phrase_book'] = $phrase_result;	
		//for trip_plan
		$tripplan_query = $this->db->get_where('trip_plan',array('updated_date >'=>$ludate));
		$tripplan_result = $tripplan_query->result_array();			
		foreach($tripplan_result as $key8=>$value8){
			$tagids = $tripplan_result[$key8]['tag'];
			$tagids = explode(',',$tagids);
			$keyword = '';
			foreach($tagids as $tagid){
				$tag_query = $this->db->get_where('tag',array('id'=>$tagid));
				$tag_result = $tag_query->result_array();
				if($tag_result){
				$keyword []= array('id'=>$tagid,'keyword'=>$tag_result[0]['keyword']);
				}
			}
			$tripplan_result[$key8]['tag'] = $keyword;						
		}
		$finalresult['others'] ['trip_plan'] = $tripplan_result;	
		
		$query_history = $this->db->get_where('history',array('updated_date >'=>$ludate));
		$historyresult = $query_history->result_array();
		$finalresult['others'] ['history'] = $historyresult;
		
		$query_culture = $this->db->get_where('culture',array('updated_date >'=>$ludate));
		$cultureresult = $query_culture->result_array();
		$finalresult['others'] ['culture'] = $cultureresult;
		
		$query_tip = $this->db->get_where('tip',array('updated_date >'=>$ludate));
		$tipresult = $query_tip->result_array();
		$finalresult['others'] ['tip'] = $tipresult;
		
		return $finalresult;
	}
	
	function register(){
		$name 	= $this->input->post('name');
		$email 	= $this->input->post('email');
		$country = $this->input->post('country');
		$password = $this->input->post('password');	
		$password = $this->phpass->hash($password);
		$query = $this->db->get_where('user',array('email'=>$email));
		$data = array(
			'name'=>$name,
			'email'=>$email,
			'password'=>$password,
			'fb_register'=>false,
			'country'=>$country,
			'created_date'=>date('Y-m-d')
		);
		if($query->num_rows() > 0){
			$result = $query->result_array();
			$fb_id = $result[0]['fb_id'];
			$fb_register  = $result[0]['fb_register'];
			$user_id = $result[0]['id'];
			if($fb_id > 0 && $fb_register == true){
				$this->db->where('email',$email);
				$result_update = $this->db->update('user',$data);	
			}else{
				return 'email_err';
			}			
		}else{
			$result = $this->db->insert('user', $data);
			$user_id = $this->db->insert_id();
		}
		if(isset($user_id)){
			$finalresult['user_id'] = $user_id;
			return $finalresult;
		}else{
			return false;
		}		
	}
	
	function facebook(){
		$name 	= $this->input->post('name');
		$email 	= $this->input->post('email');
		$fb_id = $this->input->post('fb_id');
		$address = $this->input->post('address');	
		$query = $this->db->get_where('user',array('email'=>$email));
		$data1 = array(
			'name'=>$name,
			'email'=>$email,
			'fb_id'=>$fb_id,
			'fb_register'=>true,
			'country'=>$address,
			'created_date'=>date('Y-m-d')
		);
		$data2 = array(
			'name'=>$name,
			'email'=>$email,
			'fb_id'=>$fb_id,
			'fb_register'=>false,
			'country'=>$address,
			'created_date'=>date('Y-m-d')
		);
		if($query->num_rows() > 0){
			$this->db->where('email',$email);
			$result_update = $this->db->update('user',$data2);
			$result = $query->result_array();
			$user_id = $result[0]['id'];
		}else{
			$result = $this->db->insert('user', $data1);
			$user_id = $this->db->insert_id();
		}
		if($result){
			$finalresult['user_id'] = $user_id;
			return $finalresult;
		}else{
			return false;
		}		
	}
	
	function login(){
		$email 	= $this->input->post('email');
		$this->load->library('phpass');
		$password = $this->input->post('password');	
		$query = $this->db->get_where('user',array('email'=>$email));
		$result = $query->result_array();	
		if($query->num_rows() > 0){
			$hashed = $result[0]['password'];			
			if ($this->phpass->check($password, $hashed)){
				$finalresult['user_id'] = $result[0]['id'];
				return  $finalresult;
			}else{
				return  'wrong password';
			}
		}else{
			return 'email not exitst';
		}
	}
	
	// to accept favourite data
	function postfavourite(){
		$userid = $this->input->post('user_id');		
		$new_install = $this->input->post('new_install');
		$postapi 	= $this->input->post('api');	
		$apiencode = json_encode($postapi);
		$api = json_decode($apiencode,true);
		if($new_install == 0){
					
			$datafavourite = array(
						'user_id'=>$userid,					
						'api'=>$apiencode,
						'created_date'=>date('Y-m-d')
			);
			$query = $this->db->get_where('favourite',array('user_id'=>$userid));	
			if($query->num_rows() > 0){
				$this->db->where('user_id',$userid);
				$query = $this->db->update('favourite',$datafavourite);
			}else{	
				$query = $this->db->insert('favourite', $datafavourite);
			}
		}
		$query = $this->db->get_where('favourite',array('user_id'=>$userid));	
		if($query->num_rows() > 0){
			$result_favourite = $query->row();
			$result_favourite->api = json_decode($result_favourite->api);
			if($result_favourite){
				return $result_favourite;
			}
		}else{
			return false;
		}
	}
	// to accept rating data
	function postrating(){
		$rating = $this->input->post('rating');	
		$rating_id = $this->input->post('rating_id');	
		$api = $this->input->post('api');					
		// start calculating rating star
		$star1 = '';
		$star2 = '';
		$star3 = '';
		$star4 = '';
		$star5 = '';
		
		$querystar = $this->db->get_where('rating',array('rating_id'=>$rating_id,'api'=>$api));			
		if($querystar->num_rows() > 0){	 //  check row is already exist or not
			$resultstar = $querystar->result_array();
			$star1 = $resultstar[0]['star1'];
			$star2 = $resultstar[0]['star2'];
			$star3 = $resultstar[0]['star3'];
			$star4 = $resultstar[0]['star4'];
			$star5 = $resultstar[0]['star5'];
			if($rating == 1){				
				$star1 = $star1 + 1;
			}elseif($rating == 2){				
				$star2 = $star2 + 1;
			}elseif($rating == 3){				
				$star3 = $star3 + 1;
			}elseif($rating == 4){				
				$star4 = $star4 + 1;
			}elseif($rating == 5){				
				$star5 = $star5 + 1;
			}
			$data = array(
						'api'=>$api, 
						'rating_id'=>$rating_id,
						'rating'=>$rating,
						'star1'=>$star1,
						'star2'=>$star2,
						'star3'=>$star3,
						'star4'=>$star4,
						'star5'=>$star5,
						'created_date'=>date('Y-m-d')
					);			
			$array = array('rating_id'=>$rating_id,'api'=>$api);
			$this->db->where($array); 
			$result = $this->db->update('rating',$data);
		}else{
			if($rating == 1){
				$star1 = 1;
			}elseif($rating == 2){
				$star2 = 1;
			}elseif($rating == 3){
				$star3 = 1;
			}elseif($rating == 4){
				$star4 = 1;
			}elseif($rating == 5){
				$star5 = 1;
			}
			$data = array(
						'api'=>$api, 
						'rating_id'=>$rating_id,
						'rating'=>$rating,
						'star1'=>$star1,
						'star2'=>$star2,
						'star3'=>$star3,
						'star4'=>$star4,
						'star5'=>$star5,
						'created_date'=>date('Y-m-d')
					);	
			$result = $this->db->insert('rating', $data);
			//$rating_inserted_id =$this->db->insert_id(); // to connect respective data						
		}
		$querystar1 = $this->db->get_where('rating',array('rating_id'=>$rating_id,'api'=>$api));	
		if($querystar1->num_rows() > 0)	{
			$resultstar1 = $querystar1->result_array();
			$totalstar = ($resultstar1[0]['star1'] * 1) + ($resultstar1[0]['star2'] * 2)+ ($resultstar1[0]['star3'] * 3)+ ($resultstar1[0]['star4'] * 4)+ ($resultstar1[0]['star5'] * 5) ;
			$basetotalstar  =  $resultstar1[0]['star1'] + $resultstar1[0]['star2'] + $resultstar1[0]['star3']+$resultstar1[0]['star4']+$resultstar1[0]['star5'];
			
			$totalrating = $totalstar / $basetotalstar ; // calculating rating
			//echo $totalstar."<br/>".$basetotalstar. "<br/>".$totalrating."<br/>";
			$totalstardata = array('total'=>$totalrating);
			$array = array('rating_id'=>$rating_id,'api'=>$api);
			$this->db->where($array); 
			$result = $this->db->update('rating',$totalstardata);		
			//$data1 = array('rating_id'=>$rating_inserted_id,'rating'=>$totalstardata); // connect rating table and respective table
			$data1 = array('rating'=>$totalrating);
				if($api == 'restaurant'){
					$this->db->where('id',$rating_id);
					$result1 = $this->db->update('restaurant',$data1);
				}else if($api == 'hotel'){
					$this->db->where('id',$rating_id);
					$result1 = $this->db->update('hotel',$data1);
				}else if($api == 'activities'){
					$this->db->where('id',$rating_id);
					$result1 = $this->db->update('activities',$data1);
				}else if($api == 'attraction'){
					$this->db->where('id',$rating_id);
					$result1 = $this->db->update('attraction',$data1);
				}else if($api == 'information'){
					$this->db->where('id',$rating_id);
					$result1 = $this->db->update('information',$data1);
				}
		}else{
			$totalstardata = '';
		}
		// end calculating rating star			
		if($result){	
			return $totalstardata;
		}else{
			return false;
		}	
	}	
}
?>