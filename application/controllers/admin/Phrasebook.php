<?php
/**	
 * @file
 *  Program		: phrasebook.php 
 * 	Author		: CMS
 * 	Date		: 21/7/2015
 * 	Abstract	: phrasebook
*/
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Phrasebook extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model('admin/phrasebook_model');
		$this->load->library(array('form_validation'));	
	}
	function index()
    {
		if(!$this->session->userdata('admin_logged_in')){
			redirect(base_url().'admin/login/');
		}else{
		$data['title'] = 'Phrase book List';
        $this->load->view('admin/template/header',$data);
		$this->load->view('admin/phrasebook/phrasebooklist');	
		$this->load->view('admin/template/footer');	
		}
    }
	function getdatatable()
    {
		header('Content-type: text/html; charset=UTF-8');
		mb_internal_encoding('UTF-8');
		if(!$this->session->userdata('admin_logged_in')){
			redirect(base_url().'admin/login/');
		}else{
			$aColumns = array('id', 'name_mm','name','audio','phonetic','audio','created_date');
			// DB table to use
			$sTable = 'phrase_book';
		
			$iDisplayStart = $this->input->get_post('iDisplayStart', true);
			$iDisplayLength = $this->input->get_post('iDisplayLength', true);
			$iSortCol_0 = $this->input->get_post('iSortCol_0', true);
			$iSortingCols = $this->input->get_post('iSortingCols', true);
			$sSearch = $this->input->get_post('sSearch', true);
			$sEcho = $this->input->get_post('sEcho', true);
		
			// Paging
			if(isset($iDisplayStart) && $iDisplayLength != '-1')
			{
				$this->db->limit($this->db->escape_str($iDisplayLength), $this->db->escape_str($iDisplayStart));
			}
			
			// Ordering
			if(isset($iSortCol_0))
			{
				for($i=0; $i<intval($iSortingCols); $i++)
				{
					$iSortCol = $this->input->get_post('iSortCol_'.$i, true);
					$bSortable = $this->input->get_post('bSortable_'.intval($iSortCol), true);
					$sSortDir = $this->input->get_post('sSortDir_'.$i, true);
		
					if($bSortable == 'true')
					{
						$this->db->order_by($aColumns[intval($this->db->escape_str($iSortCol))], $this->db->escape_str($sSortDir));
					}
				}
			}
			//Filtering
			if(isset($sSearch) && !empty($sSearch))
			{
				for($i=0; $i<count($aColumns); $i++)
				{
					$bSearchable = $this->input->get_post('bSearchable_'.$i, true);
					
					// Individual column filtering
					if(isset($bSearchable) && $bSearchable == 'true')
					{
						$this->db->or_like($aColumns[$i], $this->db->escape_like_str($sSearch));
					}
				}
			}
			
			// Select Data
			$this->db->select('SQL_CALC_FOUND_ROWS '.str_replace(' , ', ' ', implode(', ', $aColumns)), false);
			$rResult = $this->db->get($sTable);
		
			// Data set length after filtering
			$this->db->select('FOUND_ROWS() AS found_rows');
			$iFilteredTotal = $this->db->get()->row()->found_rows;
		
			// Total data set length
			$iTotal = $this->db->count_all($sTable);
		
			// Output
			$output = array(
				'sEcho' => intval($sEcho),
				'iTotalRecords' => $iTotal,
				'iTotalDisplayRecords' => $iFilteredTotal,
				'aaData' => array()
			);     
			foreach ($rResult->result_array()  as $r) {
				$audio = '<audio controls id="audio">
						  <source src="' .  $r['audio'] . '" loop="loop" type="audio/mpeg">						  
						  </audio>';
				$data = array();
				$id = $r['id'];
				$baseurl =  base_url();	
				$delUrl =  $baseurl.'admin/phrasebook/delete/';
				$idurl = $id.'-'.$delUrl;
				$rr['delete'] = "<a id='" . $idurl. "' class='del' onclick='del(this.id)'> Delete </a>";	
				$data[] =$r['id'];
				$data[] =$r['name_mm'];
				$data[] =$r['name'];
				$data[] =$r['phonetic'];
				$data[] = $audio;
				$data[] =$r['created_date'];			
				$data[] =  anchor('admin/phrasebook/add/' . $r['id'], 'Edit');
				$data[] =  $rr['delete'];
				$output['aaData'][] = $data;
			}
			echo json_encode($output,JSON_UNESCAPED_UNICODE);
		}
    }
	// end server side processing
	
	// to add/edit phrasebook  data
	function add($id=''){ 
		if(!$this->session->userdata('admin_logged_in')){
			redirect(base_url().'admin/login/');
		}else{	
			if($this->input->post('cancel'))
			{
				redirect(base_url().'admin/phrasebook/');
			}
			$data = $this->phrasebook_model->form();
			$data['title'] = 'phrase';
			$data['id'] = $id;
			$data['error'] = '';
			$data['fileName'] = '';
			if($id > 0){
				$phrasebook = gettable('phrase_book','id',$id);
				if($phrasebook){
				$created = explode('-', $phrasebook[0]['created_date']);
				$updated = explode('-', $phrasebook[0]['updated_date']);
				$data['fName_mm']['value'] = $phrasebook[0]['name_mm'];
				$data['fName_en']['value'] = $phrasebook[0]['name'];
				$data['fPhontic']['value'] = $phrasebook[0]['phonetic'];
				$data['fDesc']['value'] = $phrasebook[0]['description'];
				$data['fileName'] = $phrasebook[0]['filename'];
				$data['fCdate']['value'] = $created[2].'/'.$created[1].'/'.$created[0];
				$data['fUdate']['value'] = $updated[2].'/'.$updated[1].'/'.$updated[0];	
				}else{
					redirect(base_url().'admin/phrasebook/');
				}
			}
			$this->form_validation->set_rules('namemm','Name MM','trim|required|xss_clean');
			$this->form_validation->set_rules('nameen','Name En','trim|required|xss_clean');
			$this->form_validation->set_rules('phonetic','Phonetic','trim|required|xss_clean');
			$this->form_validation->set_rules('desc','Description','trim|required|xss_clean');
			$this->form_validation->set_rules('cDate','Start Date','trim|required|xss_clean');
			$this->form_validation->set_rules('uDate','End Date','trim|required|xss_clean');
			
			if($this->input->post('submit')){
				if($this->form_validation->run()===TRUE){
					if($id == 0 || ($id > 0 && $_FILES['upload']['name'] != '')){																							
						$config['upload_path'] = './images/phrasebook/upload';
						$config['allowed_types'] = 'mp3';
						$config['max_size']	= '1024';
						$config['max_width']  = '1024';
						$config['max_height']  = '768';
						$this->load->library('upload', $config);
						$upload = $this->upload->do_upload('upload');
						if(!$upload){
							$data['error'] = $this->upload->display_errors();		
						}else{ 
							$fileName = $this->upload->data('file_name');
							$fullpath = $this->upload->data('full_path'); 
							$fullpath = explode('travel/',$fullpath);
							$fullpath = base_url().$fullpath[1];					
							$result = $this->phrasebook_model->save($id,$fileName,$fullpath);
							if($result)
							{
								redirect(base_url().'admin/phrasebook/');
							}				
						}
					}else{
						$fileName = $phrasebook[0]['filename'];
						$fullpath =$phrasebook[0]['audio'];					
						$result = $this->phrasebook_model->save($id,$fileName,$fullpath);
						if($result)
						{
							redirect(base_url().'admin/phrasebook/');
						}						
					}
				}
				$data['fName_mm']['value'] 	= $this->input->post('namemm');
				$data['fName_en']['value'] 	= $this->input->post('nameen');
				$data['fPhontic']['value'] 	= $this->input->post('phonetic');
				$data['fDesc']['value'] 	= $this->input->post('desc');
				$data['fCdate']['value'] 	= $this->input->post('cDate');
				$data['fUdate']['value'] 	= $this->input->post('uDate');	
			}
			$this->load->view('admin/template/header', $data); 
			$this->load->view('admin/phrasebook/index', $data); 
			$this->load->view('admin/template/footer');
		}
	}
	// export data as csv file
	function export(){
		if(!$this->session->userdata('admin_logged_in')){
			redirect(base_url().'admin/login/');
		}else{
			$phrasebook = gettable('phrase_book');
			$data['fileName'] = 'phrasebook'.date('dmy');
			$data['phrasebook'] = array();
			$data['phrasebook'] = $phrasebook;
			$this->load->view('admin/phrasebook/export', $data);
		}
		
	}
	// import data as excel file
	function import(){
		if(!$this->session->userdata('admin_logged_in')){
			redirect(base_url().'admin/login/');
		}else{
			if($this->input->post('cancel'))
			{
				redirect(base_url().'admin/phrasebook/');
			}
			$data['error'] = '';
			if($this->input->post('submit')){		
				$config['upload_path'] = './images/phrasebook/upload/table';
				$config['allowed_types'] = 'csv';
				$config['max_size']	= '1024';
				$config['max_width']  = '1024';
				$config['max_height']  = '768';
				$this->load->library('upload', $config);
				$upload = $this->upload->do_upload('upload');
				if(!$upload){
					$data['error'] = $this->upload->display_errors();
				}else{
					$fileName = $this->upload->data('file_name');
					$this->load->library('csvreader');	
					$fullpath = $this->upload->data('full_path');
					$data['csvData'] = $this->csvreader->parse_file($fullpath);
					
					foreach($data['csvData'] as $cd)
					{
						if (isset($cd['name_mm'],$cd['name'],$cd['phonetic'],$cd['audio'],$cd['filename'],$cd['description'])){
							//$created_date = datetime($cd['created_date']);
							//$updated_date = datetime($cd['updated_date']);
							$id = $cd['id'];
							$results_array = array(
												  'name_mm' => $cd['name_mm'],
												  'name' => $cd['name'],
												  'phonetic' => $cd['phonetic'],
												  'audio' => $cd['audio'],										 										 
												  'filename' => $cd['filename'],
												  'description' => $cd['description'],
												  'created_date' => date('Y-m-d'),
												  'updated_date' => date('Y-m-d')
												   );  
						  $query = $this->db->get_where('phrase_book',array('id'=>$id));
						  if($query->num_rows() > 0){
							  $this->db->where('id',$id);
							  $result = $this->db->update('phrase_book',$results_array);
						  }else{				  
							  $result = $this->db->insert('phrase_book', $results_array);
						  }
						}else{
							$data['error'] = "Please check the column of your csv file";
							$result = false;
						}					  			  
					}	
					if($result){
						 redirect(base_url().'admin/phrasebook/');
					}
				}	
			}
			$this->load->view('admin/template/header',$data); 
			$this->load->view('admin/phrasebook/import');
			$this->load->view('admin/template/footer');
		}
		
	}
	function delete(){
		if(!$this->session->userdata('admin_logged_in')){
			redirect(base_url().'admin/login/');
		}else{
			$id = $_REQUEST['id'];
			$result = $this->phrasebook_model->delete($id);
			echo json_encode(array('status'=>$result));	
		}
	}
}
?>