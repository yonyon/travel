<?php
header('Content-type: application/csv');
header("Content-Disposition: attachment; filename=".$fileName.'.csv');
echo "\xef\xbb\xbf";
$output= '';
$output .= 'id,name_mm,name,phonetic,audio,filename,description,updated_date,created_date';
$output .="\r\n";
if(is_array($phrasebook) && sizeof($phrasebook) > 0){
	foreach($phrasebook as $key=>$value){
		$created_date = redatetime($value['created_date']);
		$updated_date = redatetime($value['updated_date']);
		$desc = strip_tags($value['description']);
		$desc = str_replace(",","-",$desc);
		$output .= $value['id'].','.$value['name_mm'].','.$value['name'].','.$value['phonetic'].','.$value['audio'].','.$value['filename'].','.$desc.','.$updated_date.','.$created_date;
		$output .="\r\n";
	}
}
echo $output;
exit;
?>
