<?php
/**	
 * @file
 *  Program		: admin_model.php 
 * 	Author		: CMS
 * 	Date		: 23/7/2015
 * 	Abstract	: event
*/
class Admin_model extends CI_Model{
	function admin_model(){
		parent::__construct();
		$this->load->library('phpass');
	}
	function form(){ // to create form		
		$data['fname']			= 'First Name <span class="star_red">*</span> :';
		$data['lname']			= 'Last Name <span class="star_red">*</span> : ';
		$data['email']			= 'Email <span class="star_red">*</span> : ';
		$data['userName']		= 'User Name <span class="star_red">*</span> :';
		$data['pass']			= 'Password <span class="star_red">*</span> :';
		$data['conPass']		= 'Confirm Password <span class="star_red">*</span> :';
		$data['type']			= 'Level <span class="star_red">*</span> :';
		
		$data['fName']			= array('name'=>'name', 'id'=>'name', 'placeholder'=>'Enter first name', 'value'=>'');
		$data['fLname']			= array('name'=>'lName', 'id'=>'lName', 'value'=>'', 'placeholder'=>'Enter last name');
		$data['fEmail']		= array('name'=>'email', 'id'=>'email', 'placeholder'=>'Enter email', 'value'=>'');
		$data['fUserName']		= array('name'=>'userName', 'id'=>'userName', 'placeholder'=>'Enter user name');
		$data['fPass']		= array('name'=>'pass', 'id'=>'pass', 'value'=>'', 'placeholder'=>'Enter password');
		$data['fConPass']			= array('name'=>'conPass', 'id'=>'conPass', 'placeholder'=>'Enter confirm password', 'value'=>'');
		return $data;
	}
	
	function save($id){ // to save admin data
		$fname 		= $this->input->post('name');
		$lname 		= $this->input->post('lName');
		$email 	    = $this->input->post('email');
		$username	= $this->input->post('userName');
		$type 		= $this->input->post('type');
		$password = $this->input->post('pass');	
		$password = $this->phpass->hash($password);
		$data = array(
					'firstname'=>$fname, 
					'lastname'=>$lname,
					'email'=>$email, 
					'username'=>$username, 
					'password'=>$password,  
					'type'=>$type, 
				);		
		if(!empty($id) && $id > 0){
			$this->db->where('id',$id);
			$result = $this->db->update('admin',$data);	
		}else{
			$result = $this->db->insert('admin', $data);	
		}
		if($result){
			return $result;
		}else{
			return false;
		}
	}
	function delete($id){ // delete function
		$this->db->where('id',$id);
		$query = $this->db->delete('admin');
		if($query){
			return TRUE;
		}else{
			return FALSE;
		}
	}
}
?>