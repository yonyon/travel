<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('getcolname'))
{
	function getcolname($table,$colname,$id){
		$ci =& get_instance();	
		$id = explode(',',$id);
		$name  = '';
		foreach($id as $key){
		$query = $ci->db->get_where($table,array('id'=>$key));
		if($query->num_rows() > 0):
			$result = $query->result_array();
			$name .= $result[0][$colname]. ',';
		endif;
		}
		return $name;
	}
}
if ( ! function_exists('gettable'))
{
	function gettable($table,$colname='',$id=''){
		$ci =& get_instance();
		if($id!=''){
			$query = $ci->db->get_where($table,array($colname=>$id));
		}else{
			$query = $ci->db->get($table);
		}
		if($query->num_rows() > 0){
			$result = $query->result_array();
		}else{
			$result = '';
		}
		return $result;
	}
}
?>