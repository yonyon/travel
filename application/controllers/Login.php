<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model('Login_model');
		$this->load->helper(array('html', 'url', 'file'));
		$this->load->library(array('form_validation'));
	}
	public function index()
	{
		$data['fEmail'] = '';
		/*$this->load->view('login',$data);
		$this->load->view('template/footer');*/
		$data['menu'] = 'login';
		$this->load->view('template/header', $data);
		$this->load->view('login', $data);
		$this->load->view('template/footer');
	}
	public function save()
	{
		
		$this->form_validation->set_rules('email', 'Email', 'required|valid_email');
		$this->form_validation->set_rules('password','Password','trim|required|xss_clean');
		if($this->form_validation->run()===TRUE){	
			extract($this->input->post());
			$email = $this->input->post('email');
			$password = $this->input->post('password');
			$result = $this->Login_model->login();
			//var_dump($result);
			if($result){
				$sess_array = array();
				foreach($result as $row)
				{
					$travel_sess_array = array(
						'user_id' => $row->id,
						'user_name' => $row->name,
						'user_email' => $row->email,
						'user_country' => $row->country
					);
					$this->session->set_userdata('user_logged_in', $travel_sess_array);
				}	
					$data['menu'] = 'login';
					$this->load->view('template/header', $data);
					$this->load->view('template/footer');
			}else{
				$data['menu'] = 'login';
				$data['fEmail'] 	= $this->input->post('email');
				$data['error'] 	= 'incorrect username or password';
				/*$this->load->view('login',$data);
				$this->load->view('template/footer');*/
				$this->load->view('template/header', $data);
				$this->load->view('login', $data);
				$this->load->view('template/footer');
			}
		}else{
			$data['menu'] = 'login';
			$data['fEmail'] 	= $this->input->post('email');
			/*$this->load->view('login',$data);
			$this->load->view('template/footer');*/
			$this->load->view('template/header', $data);
			$this->load->view('login', $data);
			$this->load->view('template/footer');
		}
		
	}
	function out(){
		$this->session->unset_userdata('user_logged_in');
		$this->session->sess_destroy();
		$this->index();
	} 
}
