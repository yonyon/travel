<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model('home_model');
		$this->load->model("details_model");
		$this->load->helper(array('html', 'url'));
	}
	public function index()
	{
		$data['essInfo'] = $this->home_model->getData('information');
		$data['cities'] = $this->home_model->getData('city');
		$data['activities'] = $this->home_model->getData('activities');
		$data['attractions'] = $this->home_model->getData('attraction');
		$data['hotels'] = $this->home_model->getData('hotel');
		$data['restaurants'] = $this->home_model->getData('restaurant');
		$data['tripplans'] = $this->home_model->getData('trip_plan');
		$data['menu'] = 'home';
		$data['message'] = '';
		$data['error'] = false;
		if($this->input->post()){
			$this->form_validation->set_rules('email', 'Email', 'required|valid_email');
			if($this->form_validation->run() === TRUE){	
				$checkEmail = $this->details_model->checkEmail($this->input->post('email'));
				if($checkEmail){
					$subscribeSuccess = $this->details_model->subscribe($this->input->post('email'));
					if($subscribeSuccess) $data['message'] = 'Thanks! You have subscribed successfully.';
				}else{
					$data['error'] = true;
					$data['message'] = 'Email already exist.Please try other one.';
				}
				
			}else{
				$data['error'] = true;
				$data['message'] = 'Invalid email address.';
			}
		}
		//echo '<pre>';print_r($data['activities']);echo '</pre>';
		$this->load->view('/home', $data);
	}
}
?>
