<?php 
class Wheretostay_model extends CI_Model
{
    public function __construct() {
        parent::__construct();
    } 
    public function record_count($id) {// for counting rows
			
		$this->db->where('city',$id);
		$this->db->from('hotel');
		$cnt = $this->db->count_all_results();
        return $cnt;
    } 
    public function get($limit,$start,$id) { // setting limit row and starting row 
		$this->db->limit($limit,$start);		
		$query = $this->db->get_where("hotel",array('city'=>$id));	
		//echo '<pre>';print_r($this->db->last_query());echo '</pre>';
		if ($query->num_rows() > 0){
            foreach ($query->result_array() as $row){
                $data[] = $row;
            }
			
            return $data;
        }		
        return false;
   }
   public function gethotel($id) { // setting limit row and starting row 
		//$this->db->limit($limit,$start);		
		$query = $this->db->get_where("hotel",array('id'=>$id));		
		if ($query->num_rows() > 0){
            foreach ($query->result_array() as $row){
                $data[] = $row;
            }
            return $data;
        }		
        return false;
   }
   public function gettag($limit,$start,$cityid,$tagid='') { // setting limit row and starting row 
		$data = '';
		//$this->db->limit($limit,$start);
        $query = $this->db->get_where("hotel",array('city'=>$cityid));	
		//echo '<pre>';print_r($this->db->last_query());echo '</pre>';
		if ($query->num_rows() > 0) {
			$result  = $query->result_array();	
					
			foreach ($result as $row) {
				$tags = explode(',',$row['tag']);
				foreach($tags as $tag){				
					if($tag == $tagid){
						$data[] = $row;
					}
				}	
			}
			//echo $limit.', '.$start;
			$data1 = array_slice($data, $start, $limit, true);
			//echo '<pre>';print_r($data1);echo '</pre>';
			return $data1;
        }		
        return false;
   }
   
    public function record_tagcount($id,$tagid) {// for counting rows
		 $data ='';
   		 $query = $this->db->get_where("hotel",array('city'=>$id));
		 if ($query->num_rows() > 0) {
			$result  = $query->result_array();
			foreach ($result as $row) {
				$tags = explode(',',$row['tag']);
				$cnt = '';				
				foreach($tags as $tag){				
					if($tag == $tagid){
						$data[] = $row;					
					}
				}
				$cnt = count($data);
			}
			return $cnt;
        }		
    } 
}
?>