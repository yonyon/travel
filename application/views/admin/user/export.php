<?php	
header("Content-type: application/csv");
header("Content-Disposition: attachment; filename=".$fileName.'.csv');
$output= '';
$output .= 'id,name,country,email,updated_date,created_date';
$output .="\r\n";
if(is_array($user) && sizeof($user) > 0){
	foreach($user as $key=>$value){
		$created_date = redatetime($value['created_date']);
		$updated_date = redatetime($value['updated_date']);		
		$output .= $value['id'].','.$value['name'].','.$value['country'].','.$value['email'].','.$updated_date.','.$created_date;
		$output .="\r\n";
	}
}
echo $output;
exit;
?>
