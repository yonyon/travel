<?php
/**	
 * @file
 *  Program		: adminlogin_model.php 
 * 	Author		: ETK
 * 	Date		: 21/05/2014
 * 	Abstract	: adminlogin_model
*/
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Login extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model('admin/login_model');
		$this->load->library(array('form_validation'));	
	}
	public function index()
	{
		$this->form();
	}
	function form(){
		if($this->session->userdata('admin_logged_in')){
			redirect(base_url().'admin/activity');
		}else{
			$data = '';
			$success = false;
			if($this->input->post('submit')){
				$username = $this->input->post('username');
				$password = $this->input->post('password');
				$admin = $this->check_database();
				if ($admin === TRUE){
					$success = true;
					redirect(base_url().'admin/activity');
				}else{
					$data['fName']['value'] = $username;
					$data['fPass']['value'] = $password;
					$data['error']			= $admin;
				}
			}
			# login page
			if(!$success) $this->load->view('admin/login/index',$data);
		}		
	}
	function out(){
		$this->session->unset_userdata('admin_logged_in');
		$this->session->sess_destroy();
		$this->form();
	} 
	function check_database(){
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		if(!empty($username) && !empty($password)){
			$result = $this->login_model->login();
		if($result)
			{
				$sess_array = array();
				foreach($result as $row)
				{
					$travel_sess_array = array(
						'admin_id' => $row->id,
						'admin_name' => $row->username,
						'admin_name' => $row->firstname.' '.$row->lastname,
						'admin_level' => $row->type
					);
					$this->session->set_userdata('admin_logged_in', $travel_sess_array);
				}
				return TRUE;
			}else{
				# invalid user id and password
				$error = 'Incorrect username or password.';
				return $error;
			}
		}else{
			# invalid user id and password
			$error = 'Incorrect username or password.';
			return $error;
		}
	}
}
?>