<?php
/**	
 * @file
 *  Program		: hotel.php 
 * 	Author		: CMS
 * 	Date		: 17/7/2015
 * 	Abstract	: hotel
*/
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Hotel extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model('admin/hotel_model');
		$this->load->library(array('form_validation'));	
	}
	function index()
    {
		if(!$this->session->userdata('admin_logged_in')){
			redirect(base_url().'admin/login/');
		}else{
		$data['title'] = 'Hotel List';
        $this->load->view('admin/template/header',$data);
		$this->load->view('admin/hotel/hotellist');
		$this->load->view('admin/template/footer');
		}
    }
	function getdatatable()
    {
		if(!$this->session->userdata('admin_logged_in')){
			redirect(base_url().'admin/login/');
		}else{
			$aColumns = array('id', 'city','name','image_url','logo_url','price_range','latitude','created_date');
			// DB table to use
			$sTable = 'hotel';
			
			$iDisplayStart = $this->input->get_post('iDisplayStart', true);
			$iDisplayLength = $this->input->get_post('iDisplayLength', true);
			$iSortCol_0 = $this->input->get_post('iSortCol_0', true);
			$iSortingCols = $this->input->get_post('iSortingCols', true);
			$sSearch = $this->input->get_post('sSearch', true);
			$sEcho = $this->input->get_post('sEcho', true);
		
			// Paging
			if(isset($iDisplayStart) && $iDisplayLength != '-1')
			{
				$this->db->limit($this->db->escape_str($iDisplayLength), $this->db->escape_str($iDisplayStart));
			}
			// Ordering
			if(isset($iSortCol_0))
			{
				for($i=0; $i<intval($iSortingCols); $i++)
				{
					$iSortCol = $this->input->get_post('iSortCol_'.$i, true);
					$bSortable = $this->input->get_post('bSortable_'.intval($iSortCol), true);
					$sSortDir = $this->input->get_post('sSortDir_'.$i, true);
		
					if($bSortable == 'true')
					{
						$this->db->order_by($aColumns[intval($this->db->escape_str($iSortCol))], $this->db->escape_str($sSortDir));
					}
				}
			}
		   //Filtering
			if(isset($sSearch) && !empty($sSearch))
			{
				for($i=0; $i<count($aColumns); $i++)
				{
					$bSearchable = $this->input->get_post('bSearchable_'.$i, true);
					
					// Individual column filtering
					if(isset($bSearchable) && $bSearchable == 'true')
					{
						$this->db->or_like($aColumns[$i], $this->db->escape_like_str($sSearch));
					}
				}
			}        
			// Select Data
			$this->db->select('SQL_CALC_FOUND_ROWS '.str_replace(' , ', ' ', implode(', ', $aColumns)), false);
			$rResult = $this->db->get($sTable);
		
			// Data set length after filtering
			$this->db->select('FOUND_ROWS() AS found_rows');
			$iFilteredTotal = $this->db->get()->row()->found_rows;
		
			// Total data set length
			$iTotal = $this->db->count_all($sTable);
		
			// Output
			$output = array(
				'sEcho' => intval($sEcho),
				'iTotalRecords' => $iTotal,
				'iTotalDisplayRecords' => $iFilteredTotal,
				'aaData' => array()
			);     
			foreach ($rResult->result_array()  as $r) {
				$strhref1 = "<a href='" . $r['image_url'] . "' target='_blank'>";
				$strImage = $strhref1 . "<img src='" . $r['image_url'] . "' width='20' height='20' border='0'/> </a> ";
				
				$strhref2 = "<a href='" . $r['logo_url'] . "' target='_blank'>";
				$strLogo = $strhref2 . "<img src='" . $r['logo_url'] . "' width='20' height='20' border='0'/> </a> ";	
						
				$cityid = $r['city'];
				$city = $this->db->get_where('city',array('id'=>$cityid));
				$result = $city->result_array();
				if($result){
					$cityname = $result[0]['name'];
				}else{
					$cityname = $r['city'];
				}			
		
				$data = array();
				$id = $r['id'];
				$baseurl =  base_url();	
				$delUrl =  $baseurl.'admin/hotel/delete/';
				$idurl = $id.'-'.$delUrl;
				$rr['delete'] = "<a id='" . $idurl. "' class='del' onclick='del(this.id)'> Delete </a>";	
				$data[] =$r['id'];
				$data[] =$cityname;
				$data[] =$r['name'];
				$data[] = $strImage;
				$data[] = $strLogo;
				$data[] =$r['price_range'];
				$data[] =$r['latitude'];
				$data[] =$r['latitude'];
				$data[] =$r['created_date'];			
				$data[] =  anchor('admin/hotel/add/' . $r['id'], 'Edit');
				$data[] =  $rr['delete'];
				$output['aaData'][] = $data;
			}
			echo json_encode($output);
		}
    }
	// to add/ edit hotel data
	function add($id=''){ 
		if(!$this->session->userdata('admin_logged_in')){
			redirect(base_url().'admin/login/');
		}else{	
			if($this->input->post('cancel'))
			{
				redirect(base_url().'admin/hotel/');
			}
			$data = $this->hotel_model->form();
			$cities = gettable('city');
			foreach ($cities as $city){
				$data['allCity'][$city['id']] = $city['name'];
			}
			$tags = gettable('tag');
			foreach ($tags as $tag){
				$data['allTag'][$tag['id']] = $tag['keyword'];
			}
			$data['selectedCity'] = '';
			$data['selectedTag'] = '';
			$data['title'] = 'Hotel';
			$data['id'] = $id;
			$data['error'] = '';
			$data['fileName'] = '';
			$data['logoName'] = '';
			if($id > 0){
				$hotel = gettable('hotel','id',$id);
				if($hotel){
					$created = explode('-', $hotel[0]['created_date']);
					$updated = explode('-', $hotel[0]['updated_date']);
					$data['fName']['value'] = $hotel[0]['name'];
					$data['fAddress']['value'] = $hotel[0]['address'];
					$data['fDesc']['value'] = $hotel[0]['description'];
					$data['fPhone']['value'] = $hotel[0]['phone'];
					$data['fFax']['value'] = $hotel[0]['fax'];
					$data['fEmail']['value'] = $hotel[0]['email'];
					$data['fWebsite']['value'] = $hotel[0]['website'];
					$data['fPrice']['value'] = $hotel[0]['price_range'];
					$data['fpromotion']['value'] = $hotel[0]['promotion'];
					$data['fHowtoget']['value'] = $hotel[0]['how_to_get_there'];
					$data['fLongitude']['value'] = $hotel[0]['longitude'];
					$data['fLatitude']['value'] = $hotel[0]['latitude'];
					$data['fCdate']['value'] = $created[2].'/'.$created[1].'/'.$created[0];
					$data['fUdate']['value'] = $updated[2].'/'.$updated[1].'/'.$updated[0];
					$data['selectedTagId'] =$hotel[0]['tag'];
					$data['selectedTag'] = getcolname('tag','keyword',$data['selectedTagId']);	
					if($hotel[0]['image_url'] !=''){
						$filename = explode('upload/',$hotel[0]['image_url']);
						$data['fileName'] = $filename[1];
					}else{
						$data['fileName'] = '';
					}
					
				}else{
					redirect(base_url().'admin/hotel/');
				}
			}
			$this->form_validation->set_rules('name','Name','trim|required|xss_clean');
			$this->form_validation->set_rules('address','Address','trim|required|xss_clean');
			$this->form_validation->set_rules('longitude','Longitude','trim|required|xss_clean');
			$this->form_validation->set_rules('latitude','Latitude','trim|required|xss_clean');
			$this->form_validation->set_rules('desc','Description','trim|required|xss_clean');
			$this->form_validation->set_rules('cDate','created Date','trim|required|xss_clean');
			$this->form_validation->set_rules('uDate','Updated Date','trim|required|xss_clean');
			
			if($this->input->post('submit')){
				if($this->form_validation->run()===TRUE){
					if($id == 0 || ($id > 0 && $_FILES['upload']['name'] != '')|| ($id > 0 && $_FILES['logo']['name'] != '')){
						
						$config['upload_path'] = './images/hotel/upload';
						$config['allowed_types'] = 'gif|jpg|png';
						$config['max_size']	= '1024';
						$config['max_width']  = '1024';
						$config['max_height']  = '768';
						$this->load->library('upload');
						$this->upload->initialize($config);
						$upload = $this->upload->do_upload('upload');
						if(!$upload){					
							//$data['error'] = $this->upload->display_errors();	
							$result = $this->hotel_model->save($id,'','');
							if($result)
							{
								redirect(base_url().'admin/hotel/');
							}							
						}else{	
							$fullpath1 = $this->upload->data('full_path');	
							$fullpath1 = explode('travel/',$fullpath1);
							$fullpath1 = base_url().$fullpath1[1];				
							$configlogo['upload_path'] = './images/hotel/upload/logo/';
							$configlogo['allowed_types'] = 'gif|jpg|png';
							$configlogo['max_size']	= '800';
							$configlogo['max_width']  = '800';
							$configlogo['max_height']  = '800';
							$this->load->library('upload');
							$this->upload->initialize($configlogo);
							$logoupload = $this->upload->do_upload('logo');				
							if(!$logoupload){	
								if($fullpath1 != ''){
									$result = $this->hotel_model->save($id,$fullpath1,'');
									if($result)
									{
										redirect(base_url().'admin/hotel/');
									}
								}else{
									$result = $this->hotel_model->save($id,'','');
									if($result)
									{
										redirect(base_url().'admin/hotel/');
									}
								}
								//$data['error'] = $this->upload->display_errors();	
								
																
							}else{	
								$fullpath2 = $this->upload->data('full_path');				
								$fullpath2 = explode('travel/',$fullpath2);
								$fullpath2 = base_url().$fullpath2[1];
								$result = $this->hotel_model->save($id,$fullpath1,$fullpath2);
								if($result)
								{
									redirect(base_url().'admin/hotel/');
								}	
							}				
														
						}					
						
					}else{
						$fullpath1 = $hotel[0]['image_url'];
						if(isset($hotel[0]['image_url'])){
							$fullpath1 = $hotel[0]['image_url'];	
						}else{
							$fullpath1 = '';
						}
						if(isset($hotel[0]['logo_url'])){
							$fullpath2 = $hotel[0]['logo_url'];
						}else{
							$fullpath2 = '';
						}
						$result = $this->hotel_model->save($id,$fullpath1,$fullpath2);
						if($result)
						{
							redirect(base_url().'admin/hotel/');
						}	
					}
				}
				$data['fName']['value'] 	= $this->input->post('name');
				$data['fAddress']['value'] 	= $this->input->post('address');
				$data['fPhone']['value'] = $this->input->post('phone');
				$data['fFax']['value'] =$this->input->post('fax');
				$data['fEmail']['value'] = $this->input->post('email');
				$data['fWebsite']['value'] = $this->input->post('website');
				$data['fLongitude']['value'] 	= $this->input->post('longitude');
				$data['fLatitude']['value'] 	= $this->input->post('latitude');
				$data['fDesc']['value'] 	= $this->input->post('desc');
				$data['fCdate']['value'] 	= $this->input->post('cDate');
				$data['fUdate']['value'] 	= $this->input->post('uDate');	
				$data['selectedTag'] 	= $this->input->post('tags');
			}
			$this->load->view('admin/template/header', $data); 
			$this->load->view('admin/hotel/index', $data); 
			$this->load->view('admin/template/footer');
		}
	}
	// export data as csv file
	function export(){
		if(!$this->session->userdata('admin_logged_in')){
			redirect(base_url().'admin/login/');
		}else{
			$startDate = date('Y-m-d 00:00:01');
			$start = strtotime($startDate);
			$endDate = date('Y-m-d 23:59:59');
			$end = strtotime($endDate);
			
			$hotel = gettable('hotel');
			$data['fileName'] = 'hotel'.date('dmy');
			$data['hotel'] = array();
			$data['hotel'] = $hotel;
			$this->load->view('admin/hotel/export', $data);
		}
		
	}
	// to import table
	function import(){
		if(!$this->session->userdata('admin_logged_in')){
			redirect(base_url().'admin/login/');
		}else{
			if($this->input->post('cancel'))
			{
				redirect(base_url().'admin/hotel/');
			}
			$data['error'] = '';
			if($this->input->post('submit')){		
				$config['upload_path'] = './images/hotel/upload/table';
				$config['allowed_types'] = 'csv';
				$config['max_size']	= '1024';
				$config['max_width']  = '1024';
				$config['max_height']  = '768';
				$this->load->library('upload', $config);
				$upload = $this->upload->do_upload('upload');
				if(!$upload){
					$data['error'] = $this->upload->display_errors();
				}else{
					$fileName = $this->upload->data('file_name');
					$this->load->library('csvreader');	
					$fullpath = $this->upload->data('full_path');
					$data['csvData'] = $this->csvreader->parse_file($fullpath);
					foreach($data['csvData'] as $cd)
					{
						
						if (isset($cd['cityId'],$cd['tagId'],$cd['name'],$cd['description'],$cd['address'],$cd['image_url'],$cd['longitude'],$cd['latitude'],$cd['phone'],$cd['fax'],$cd['email'],$cd['website'],$cd['logo_url'])){
							$id = $cd['id'];
							$results_array = array(
												  'city' => $cd['cityId'],
												  'tag' => $cd['tagId'],
												  'name' => $cd['name'],
												  'description' => $cd['description'],
												  'address' => $cd['address'],
												  'image_url' => $cd['image_url'],
												  'longitude' => $cd['longitude'],
												  'latitude' => $cd['latitude'],
												  'phone' => $cd['phone'],
												  'email' => $cd['email'],
												  'fax' => $cd['fax'],											  
												  'website' => $cd['website'],											  
												  'logo_url' => $cd['logo_url'],
												  'created_date' => date('Y-m-d'),
												  'updated_date' => date('Y-m-d')
												   );  
						  $query = $this->db->get_where('hotel',array('id'=>$id));
						  if($query->num_rows() > 0){
							  $this->db->where('id',$id);
							  $result = $this->db->update('hotel',$results_array);
						  }else{				  
							  $result = $this->db->insert('hotel', $results_array);
						  }	
					  }else{
							$data['error'] = "Please check the column name of your csv file";
							$result = false;
					  }				  			  
					}	
					if($result){
						 redirect(base_url().'admin/hotel/');
					}
				}	
			}
			$this->load->view('admin/template/header',$data); 
			$this->load->view('admin/hotel/import');
			$this->load->view('admin/template/footer');
		}
		
	}
	function delete(){
		if(!$this->session->userdata('admin_logged_in')){
			redirect(base_url().'admin/login/');
		}else{
			$id = $_REQUEST['id'];
			$result = $this->hotel_model->delete($id);
			echo json_encode(array('status'=>$result));	
		}
	}
	function sendMail(){
		$this->load->library('email');
		$this->email->from('eithandarkyaw@appvantage.sg', 'Ei Thandar Kyaw');
		$this->email->to('ezl.etk@gmail.com'); 
		$this->email->subject('Email Test for subscribed');
		$this->email->message('Testing the email for subscribed.');
		$data = '';
		//$send = $this->email->send();
		if ( ! $this->email->send())
		{
			echo 'error';
		}else{
			
			redirect(base_url().'admin/hotel/');
		}
		
	}
}
?>