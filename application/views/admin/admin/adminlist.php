<script type="text/javascript" charset="utf-8">
	$(document).ready(function() {
		var oTable;
		oTable = $('#DataTables').dataTable( {			
			"bJQueryUI": true,
			"sPaginationType": "full_numbers",
			"bProcessing": true,
			"bServerSide": true,
			"sAjaxSource": '<?php echo base_url(); ?>/admin/admin/getdatatable',
			"aoColumns": [
			null, null, null,null,null,null,null,
			{ "sClass": "center", "bSortable": false },
			{ "sClass": "center", "bSortable": false }
			],
			"aaSorting": [[0, 'desc']],
			"iDisplayLength":10,			
		});
	});
</script>
<style>
	.paging_full_numbers {
		width: 280px !important;
	}
</style>
<div id="toolbar" style=" margin-left:5px;"><a class="btn-add" href="<?php echo base_url(); ?>admin/admin/add" style="text-decoration:none;">Add Admin</a></div>

<div style="width:900px; text-align:left; margin-left:5px;">
<table cellspacing="0" cellpadding="0" border="0" class="display" id="DataTables" bgcolor="#ffffff">
  <thead>
    <tr>
    	<th> ID </th>
        <th> First Name </th>
        <th> Last Name </th>        
        <th> Email</th>
        <th> Username</th>
        <th> Type</th>
        <th> Register Time</th>
        <th width="50"> </th>
        <th width="50"> </th>
    </tr>
  </thead>
  <tbody>
 </tbody>
</table>
</div>
