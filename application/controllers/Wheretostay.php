<?php
/**	
 * @file
 *  Program		: attraction.php 
 * 	Author		: CMS
 * 	Date		: 7/8/2015
 * 	Abstract	: Attraction
*/
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Wheretostay extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model("wheretostay_model");
		$this->load->model('details_model');
		$this->load->library(array('form_validation'));	
		$this->load->library("pagination");		 
	}
	function index()
    {
		/*$this->load->view('wheretostay');
		$this->load->view('template/footer');*/
		$this->show(1);
    }
	function pagination($id){
		$config["base_url"] = base_url() . "wheretostay/detail/".$id."/";
		$config["total_rows"] = $this->wheretostay_model->record_count($id);
        $config["per_page"] = 1;
        $config["uri_segment"] = 4;	
		$config['first_link'] = 'First';
		$config['last_link'] = ' Last';
		$config['next_link'] = '>>';
		$config['prev_link'] = '<<';
		$config['full_tag_open'] = "<ul class='pagination'>";
		$config['full_tag_close'] ="</ul>";
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
		$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
		$config['next_tag_open'] = "<li>";
		$config['next_tagl_close'] = "</li>";
		$config['prev_tag_open'] = "<li>";
		$config['prev_tagl_close'] = "</li>";
		$config['first_tag_open'] = "<li>";
		$config['first_tagl_close'] = "</li>";
		$config['last_tag_open'] = "<li>";
		$config['last_tagl_close'] = "</li>";
		$this->pagination->initialize($config);
		$pageid = $this->uri->segment(4);		
		if($pageid>0){
			$page = $pageid; //for page number
		}else{
			$page = 0; //for page number
		}
		$data["result_hotles"] = $this->wheretostay_model->get($config["per_page"],$page,$id);
        $data["links"] = $this->pagination->create_links();	
		$data['id']=$id;
	}
	function show($id='')
    {
		$config["base_url"] = base_url() . "wheretostay/show/".$id."/";	
		$config["total_rows"] = $this->wheretostay_model->record_count($id);
        $config["per_page"] = 8;
        $config["uri_segment"] = 4;	
		$config['first_link'] = 'First';
		$config['last_link'] = ' Last';
		$config['next_link'] = '>>';
		$config['prev_link'] = '<<';
		$config['full_tag_open'] = "<ul class='pagination'>";
		$config['full_tag_close'] ="</ul>";
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
		$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
		$config['next_tag_open'] = "<li>";
		$config['next_tagl_close'] = "</li>";
		$config['prev_tag_open'] = "<li>";
		$config['prev_tagl_close'] = "</li>";
		$config['first_tag_open'] = "<li>";
		$config['first_tagl_close'] = "</li>";
		$config['last_tag_open'] = "<li>";
		$config['last_tagl_close'] = "</li>";
	
		$this->pagination->initialize($config);
		$pageid = $this->uri->segment(4);
		
		if($pageid>0){
			$page = $pageid; //for page number
		}else{
			$page = 0; //for page number
		}
		
		$data["results"] = $this->wheretostay_model->get($config["per_page"],$page,$id);
		//echo '<pre>';print_r($data["results"]);echo '</pre>';
        $data["links"] = $this->pagination->create_links();		
		$data['id']=$id;
		$data['detail']='';
		$data['type'] = 'city';
		$data['detailurl']= 'Details/show/hotel/';
		$data['tagurl'] = 'wheretostay/showpage/';
		$data['menu'] = 'city';
		$data['message'] = '';
		$data['error'] = false;
		if($this->input->post()){
			$this->form_validation->set_rules('email', 'Email', 'required|valid_email');
			if($this->form_validation->run() === TRUE){	
				$checkEmail = $this->details_model->checkEmail($this->input->post('email'));
				if($checkEmail){
					$subscribeSuccess = $this->details_model->subscribe($this->input->post('email'));
					if($subscribeSuccess) $data['message'] = 'Thanks! You have subscribed successfully.';
				}else{
					$data['error'] = true;
					$data['message'] = 'Email already exist.Please try other one.';
				}
				
			}else{
				$data['error'] = true;
				$data['message'] = 'Invalid email address.';
			}
		}
		$this->load->view('template/header', $data);
		$this->load->view('wheretostay', $data);
		$this->load->view('template/footer');
		//$this->load->view('wheretostay',$data);
		//$this->load->view('template/footer');
	}
	function showpage($tagid='',$cityid=''){
		$config["base_url"] = base_url() . "wheretostay/showpage/".$tagid."/".$cityid."/"; // base_url for pagination
		$config["total_rows"] = $this->wheretostay_model->record_tagcount($cityid,$tagid);
        $config["per_page"] = 8;
        $config["uri_segment"] = 5;	
		$config['first_link'] = 'First';
		$config['last_link'] = ' Last';
		$config['next_link'] = '>>';
		$config['prev_link'] = '<<';
		$config['full_tag_open'] = "<ul class='pagination'>";
		$config['full_tag_close'] ="</ul>";
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
		$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
		$config['next_tag_open'] = "<li>";
		$config['next_tagl_close'] = "</li>";
		$config['prev_tag_open'] = "<li>";
		$config['prev_tagl_close'] = "</li>";
		$config['first_tag_open'] = "<li>";
		$config['first_tagl_close'] = "</li>";
		$config['last_tag_open'] = "<li>";
		$config['last_tagl_close'] = "</li>";
		$this->pagination->initialize($config);
		$pageid = $this->uri->segment(5);
		if($pageid>0){
			$page = $pageid; //for page number
		}else{
			$page = 0; //for page number
		}
		$data["results"] = $this->wheretostay_model->gettag($config["per_page"],$page,$cityid,$tagid);
        $data["links"] = $this->pagination->create_links();		
		$data['id']=$cityid;
		$data['detail']='';
		$data['type'] = 'city';
		$data['detailurl'] = 'Details/show/hotel/';
		$data['tagurl'] = 'wheretostay/showpage/';
		$data['menu'] = 'city';
		$data['message'] = '';
		$data['error'] = false;
		if($this->input->post()){
			$this->form_validation->set_rules('email', 'Email', 'required|valid_email');
			if($this->form_validation->run() === TRUE){	
				$checkEmail = $this->details_model->checkEmail($this->input->post('email'));
				if($checkEmail){
					$subscribeSuccess = $this->details_model->subscribe($this->input->post('email'));
					if($subscribeSuccess) $data['message'] = 'Thanks! You have subscribed successfully.';
				}else{
					$data['error'] = true;
					$data['message'] = 'Email already exist.Please try other one.';
				}
				
			}else{
				$data['error'] = true;
				$data['message'] = 'Invalid email address.';
			}
		}
		$this->load->view('template/header', $data);
		$this->load->view('wheretostay', $data);
		$this->load->view('template/footer');
		//$this->load->view('template/footer');
	}
	function detail($cityid,$tableid)
    {
		$data["result_hotles"] = $this->wheretostay_model->gethotel($tableid);
		$data['id']=$cityid;
		$data['detail']='detail';
		$this->load->view('wheretostay',$data);
		$this->load->view('template/footer');
    }
}
?>