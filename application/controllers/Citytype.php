<?php
/**	
 * @file
 *  Program		: attraction.php 
 * 	Author		: CMS
 * 	Date		: 17/7/2015
 * 	Abstract	: Attraction
*/
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Citytype extends CI_Controller {
	public function __construct(){
		parent::__construct();
		//$this->load->model('admin/attraction_model');
		$this->load->library(array('form_validation'));	
	}
	function index()
    {
		//$this->load->view('template/menu');
		$this->load->view('yangon');
		$this->load->view('template/footer');
    }
	function show($id='')
    {
		$data['id'] = $id;
		$this->load->view('citytype',$data);
		$this->load->view('template/footer');
    }
}
?>