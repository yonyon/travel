<?php	
header("Content-type: application/csv");
header("Content-Disposition: attachment; filename=".$fileName.'.csv');
$output= '';
$output .= 'id,tagId,tag_keyword,categoryId,category,description,image_url,updated_date,created_date';
$output .="\r\n";
if(is_array($info) && sizeof($info) > 0){
	foreach($info as $key=>$value){		
		$created_date = redatetime($value['created_date']);
		$updated_date = redatetime($value['updated_date']);
		
		$tagid = $value['tag'];
		$tag = $this->db->get_where('tag',array('id'=>$tagid));
		$tag_result = $tag->result_array();
		
		if($tag_result){
			$keyword = $tag_result[0]['keyword'];
		}else{
			$keyword = $tagid;
		}
		$categoryid = $value['category'];
		$category = $this->db->get_where('category',array('id'=>$categoryid));
		$category_result = $category->result_array();
		
		if($category_result){
			$category = $category_result[0]['name'] ." - ".$category_result[0]['name'];
		}else{
			$category = $categoryid;
		}
		$desc = strip_tags($value['description']);
		$desc = str_replace(",","-",$desc);
		$output .= $value['id'].','.$tagid.','.$keyword.','.$categoryid.','.$category.','.$desc.','.$value['image_url'].','.$updated_date.','.$created_date;
		$output .="\r\n";
	}
}
echo $output;
exit;
?>
