<div class="title"><?php echo ($id > 0) ? 'Edit '.$title : 'New '.$title; ?></div>
<?php 
$attributes = array('class'=>'eventform');
echo form_open_multipart(base_url().'admin/userrequest/add/'.$id, $attributes);
if($error != '') $error = $error;
?>
<script type="text/javascript">
	$(document).ready(function(){
		$(".txtFromDate").datepicker({
			minDate: 0,
			maxDate: "+1530D",
			numberOfMonths: 1,
			onSelect: function(selected) {
			  $(".txtToDate").datepicker("option","minDate", selected)
			}
		});
		$(".txtToDate").datepicker({ 
			minDate: 0,
			maxDate:"+1530D",
			numberOfMonths: 1,
			onSelect: function(selected) {
			   $(".txtFromDate").datepicker("option","maxDate", selected)
			}
		});
	});
	tinymce.init({
		selector: "textarea",
		forced_root_block: false,
    });
</script>
<table style="width:710px !important;">
<tr><td class="star_red" colspan="3"><?php echo $error; ?></td><td></td></tr>
    <?php
    if($id > 0){
	?>
    <tr>
    	<td>ID : </td>
        <td><?php echo $id; ?></td>
    </tr>
    <?php
	}
	?>
    <tr>
    	<td style="padding-right:80px !important;"><?php echo $type; ?></td>
        <td><?php echo form_dropdown('type', $allType, $selectedType); ?></td>
    </tr>
      <tr>
    	<td><?php echo $name; ?></td>
        <td ><?php echo form_input($fName); 
			echo form_error('name', '<div class="error">', '</div>');
			?>
        </td>
    </tr>
	<tr>
    	<td><?php echo $city; ?></td>
        <td><?php echo form_dropdown('city', $allCity, $selectedCity); ?></td>
    </tr>
    <tr>
    	<td valign="top"><?php echo $address; ?></td>
        <td><?php echo form_input($fAddress);
		 echo form_error('address', '<div class="error">', '</div>');
		?></td>
    </tr>
     <tr>  
    	<td valign="top"><?php echo $image; ?></td>
        <td><?php echo form_upload($fImage). ' '.$fileName; ?></td>
    </tr>
     <tr>
    	<td valign="top"><?php echo $logo; ?></td>
        <td><?php echo form_upload($fLogo). ' '.$logoName; ?></td>
    </tr>
    <tr>
    	<td><?php echo $price; ?></td>
        <td ><?php echo form_input($fPrice); 
			echo form_error('price', '<div class="error">', '</div>');
			?>
        </td>
    </tr>
    <tr>
    	<td><?php echo $phone; ?></td>
        <td ><?php echo form_input($fPhone); 
			echo form_error('phone', '<div class="error">', '</div>');
			?>
        </td>
    </tr>
    <tr>
    	<td><?php echo $fax; ?></td>
        <td ><?php echo form_input($fFax); 
			echo form_error('fax', '<div class="error">', '</div>');
			?>
        </td>
    </tr>
    <tr>
    	<td><?php echo $email; ?></td>
        <td ><?php echo form_input($fEmail); 
			echo form_error('email', '<div class="error">', '</div>');
			?>
        </td>
    </tr>
    <tr>
    	<td><?php echo $website; ?></td>
        <td ><?php echo form_input($fWebsite); 
			echo form_error('website', '<div class="error">', '</div>');
			?>
        </td>
    </tr>
    <tr>    
    	<td><?php echo $cDate; ?></td>
        <td><?php echo form_input($fCdate);
		echo form_error('cDate', '<div class="error">', '</div>');
		?></td>
    </tr>
    
    <tr>
        <td colspan="2">
        <?php
			$submit = array('name' => 'submit', 'content'=> 'Save', 'id'=> 'submit','type'=> 'submit', 'value' => 'true', 'onclick'=>'', 'class'=>'btn btn-primary');
			echo form_button($submit);
			$cancel = array('name' => 'cancel', 'content'=> 'Cancel', 'id'=> 'submit','type'=> 'submit', 'value' => 'true', 'class'=>'btn');
			echo form_button($cancel);
		?>
        </td>
    </tr>
</table>
<?php echo form_close(); ?>