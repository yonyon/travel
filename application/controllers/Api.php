<?php
/**	
 * @file
 *  Program		: api.php 
 * 	Author		: ETK
 * 	Date		: 23/05/2014
 * 	Abstract	: update beacon for API
*/
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Api extends CI_Controller {
	public function __construct(){
		parent::__construct();
		# load model s
		$this->load->model('api_model');
		$this->load->helper(array('html', 'url', 'file'));
		$this->load->library(array('form_validation'));
	}
	
	function getcity(){ // getting the city data
		extract($this->input->post());
		$this->form_validation->set_rules('token','Token','trim|required|xss_clean');
		if($this->form_validation->run()===TRUE){
			$tokenCheck = $this->api_model->checkToken();
			$dateCheck = $this->api_model->checkDate();
			if($tokenCheck){
				if($dateCheck){
					$result = $this->api_model->getcity();
					if($result){
						$data['result'] = $result;
						$data['success'] = 'success';
					}else{
						$data['result'] = '';
						$data['no_id'] = 'no_id';
					}
				}else{
					$data['result'] = '';
					$data['invaliddate'] = 'invalid';
				}					
			}else{
				$data['result'] = '';
				$data['invalidApi'] = 'invalid';
			}
		}else{
			$data['result'] = '';
			$data['required_field_token'] = 'required_field';
		}
		# log file
		$curUrl = 'URL : '.base_url(uri_string());
		$data1 = '';
		$curTime = date('Y-m-d H:m:s');
		$post = $this->input->post();
		$postValue = json_encode($post, JSON_NUMERIC_CHECK);
		$data1 .= ' '.$curTime.' '.$curUrl.' POST : '.$postValue;
		if(!write_file('./log_file.txt', $data1."\n", "a+")){
			echo 'File not write.';
		}else{
			$this->load->view('api/travel', $data); 
		}
	}
	
	function register(){ // recording the user data
		extract($this->input->post());
		$tokenCheck = $this->api_model->checkToken();
		$this->form_validation->set_rules('token','Token','trim|required|xss_clean');
		$this->form_validation->set_rules('name','Name','trim|required|xss_clean');
		$this->form_validation->set_rules('email','Email','trim|required|xss_clean');
		$this->form_validation->set_rules('password','Password','trim|required|xss_clean');
		$this->form_validation->set_rules('conpassword','Confirm Password','trim|required|xss_clean');
		if($this->form_validation->run()===TRUE){
			$this->form_validation->set_rules('email','Email','trim|required|xss_clean|valid_email');
			if($this->form_validation->run()===TRUE){
				$this->form_validation->set_rules('password','password','trim|required|xss_clean');
				$this->form_validation->set_rules('conpassword','confirm password','trim|required|xss_clean|matches[password]');
				if($this->form_validation->run()===TRUE){
					if($tokenCheck){							
							$result = $this->api_model->register();
							if(is_array($result)){
								$data['result'] = $result;
								$data['success'] = 'success';
							}elseif($result  == 'email_err'){
								$data['result'] = '';
								$data['email_err'] = 'email_err';
							}else{
								$data['result'] = '';
								$data['no_id'] = 'no_id';
							}						
					}else{
						$data['result'] = '';
						$data['invalidApi'] = 'invalid';
					}
				}else{
					$data['result'] = '';
					$data['pass_err'] = 'pass_err';
				}
			}else{
				$data['result'] = '';
				$data['email_err'] = 'email_err';
			}
		}else{
			$data['result'] = '';
			$data['required_field'] = 'required_field';
		}
		$curUrl = 'URL : '.base_url(uri_string());
		$data1 = '';
		$curTime = date('Y-m-d H:m:s');
		$post = $this->input->post();
		$postValue = json_encode($post, JSON_NUMERIC_CHECK);
		$data1 .= ' '.$curTime.' '.$curUrl.' POST : '.$postValue;
		if(!write_file('./log_file.txt', $data1."\n", "a+")){
			echo 'File not write.';
		}else{
			$this->load->view('api/travel', $data); 
		}
	}
	
	function login(){ // checking username and password
		extract($this->input->post());
		$this->form_validation->set_rules('token','Token','trim|required|xss_clean');
		$this->form_validation->set_rules('email','Email','trim|required|xss_clean');
		$this->form_validation->set_rules('password','Password','trim|required|xss_clean');
		if($this->form_validation->run()===TRUE){
			$this->form_validation->set_rules('email', 'Email','valid_email');
			if($this->form_validation->run()===TRUE){
				$tokenCheck = $this->api_model->checkToken();
				if($tokenCheck){								
					$result = $this->api_model->login();
					if(is_array($result)){
						$data['result'] = $result;
						$data['success'] = 'success';
					}elseif($result == 'wrong password'){
						$data['wrong_pass'] = 'wrong_pass';
					}elseif($result == 'email not exitst'){
						$data['wrong_email'] = 'wrong_email';
					}else{
						$data['result'] = '';
						$data['no_id'] = 'no_id';
					}			
				}else{
					$data['result'] = '';
					$data['invalidApi'] = 'invalid';
				}				
		}else{
				$data['result'] = '';
				$data['email_err'] = 'email_err';
		    }				
		}else{
			$data['result'] = '';
			$data['required_field'] = 'required_field';
		}
		$curUrl = 'URL : '.base_url(uri_string());
		$data1 = '';
		$curTime = date('Y-m-d H:m:s');
		$post = $this->input->post();
		$postValue = json_encode($post, JSON_NUMERIC_CHECK);
		$data1 .= ' '.$curTime.' '.$curUrl.' POST : '.$postValue;
		if(!write_file('./log_file.txt', $data1."\n", "a+")){
			echo 'File not write.';
		}else{
			$this->load->view('api/travel', $data); 
		}
	}
		
	function postfavourite(){ // checking username and password
		extract($this->input->post());
		$this->form_validation->set_rules('token','Token','trim|required|xss_clean');
		$this->form_validation->set_rules('user_id','user id','trim|required|xss_clean');
		$this->form_validation->set_rules('new_install','New Install','trim|required|xss_clean');
		if($this->form_validation->run()===TRUE){
			$tokenCheck = $this->api_model->checkToken();				
				if($tokenCheck){								
					$result = $this->api_model->postfavourite();			
					if($result){
						$data['result'] = $result;
						$data['success'] = 'success';
					}else if ($result == 'already favourite'){
						$data['alreadyfavourite'] = 'already favourite';
					}else{
						$data['result'] = '';
						$data['no_id'] = 'no_id';
					}			
				}else{
					$data['result'] = '';
					$data['invalidApi'] = 'invalid';
				}
		}else{
			$data['result'] = '';
			$data['required_field'] = 'required_field';
		}
		$curUrl = 'URL : '.base_url(uri_string());
		$data1 = '';
		$curTime = date('Y-m-d H:m:s');
		$post = $this->input->post();		
		$postValue = json_encode($post, JSON_NUMERIC_CHECK);
		$data1 .= ' '.$curTime.' '.$curUrl.' POST : '.$postValue;
		if(!write_file('./log_file.txt', $data1."\n", "a+")){
			echo 'File not write.';
		}else{
			$this->load->view('api/travel', $data); 
		}
	}
	
	function postrating(){ // checking username and password
		extract($this->input->post());
		$this->form_validation->set_rules('token','Token','trim|required|xss_clean');
		$this->form_validation->set_rules('rating','Rating','trim|required|xss_clean');
		$this->form_validation->set_rules('rating_id','Rating id','trim|required|xss_clean');
		$this->form_validation->set_rules('api','Api','trim|required|xss_clean');
		if($this->form_validation->run()===TRUE){
			$tokenCheck = $this->api_model->checkToken();			
				if($tokenCheck){								
					$result = $this->api_model->postrating();
					if($result){
						$data['result'] = $result;
						$data['success'] = 'success';
					}else{
						$data['result'] = '';
						$data['no_id'] = 'no_id';
					}			
				}else{
					$data['result'] = '';
					$data['invalidApi'] = 'invalid';
				}			
		}else{
			$data['result'] = '';
			$data['required_field'] = 'required_field';
		}
		$curUrl = 'URL : '.base_url(uri_string());
		$data1 = '';
		$curTime = date('Y-m-d H:m:s');
		$post = $this->input->post();
		$postValue = json_encode($post, JSON_NUMERIC_CHECK);
		$data1 .= ' '.$curTime.' '.$curUrl.' POST : '.$postValue;
		if(!write_file('./log_file.txt', $data1."\n", "a+")){
			echo 'File not write.';
		}else{
			$this->load->view('api/travel', $data); 
		}
	}
	
	function facebook(){ // recording the user data
		extract($this->input->post());
		$tokenCheck = $this->api_model->checkToken();
		$this->form_validation->set_rules('token','Token','trim|required|xss_clean');
		$this->form_validation->set_rules('fb_id','fb_id','trim|required|xss_clean');
		$this->form_validation->set_rules('email','Email','trim|required|xss_clean');
		if($this->form_validation->run()===TRUE){
			$this->form_validation->set_rules('email', 'Email','valid_email');
			if($this->form_validation->run()===TRUE){
					if($tokenCheck){							
							$result = $this->api_model->facebook();
							if($result){
								$data['result'] = $result;
								$data['success'] = 'success';
							}else{
								$data['result'] = '';
								$data['no_id'] = 'no_id';
							}						
					}else{
						$data['result'] = '';
						$data['invalidApi'] = 'invalid';
					}
			}else{
				$data['result'] = '';
				$data['email_err'] = 'email_err';
		    }				
		}else{
			$data['result'] = '';
			$data['required_field'] = 'required_field';
		}
		$curUrl = 'URL : '.base_url(uri_string());
		$data1 = '';
		$curTime = date('Y-m-d H:m:s');
		$post = $this->input->post();
		$postValue = json_encode($post, JSON_NUMERIC_CHECK);
		$data1 .= ' '.$curTime.' '.$curUrl.' POST : '.$postValue;
		if(!write_file('./log_file.txt', $data1."\n", "a+")){
			echo 'File not write.';
		}else{
			$this->load->view('api/travel', $data); 
		}
	}
}
?>