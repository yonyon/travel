<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {
	public function __construct(){
		parent::__construct();
		//$this->load->model('admin/adminlogin_model');
		$this->load->helper(array('html', 'url'));
		$this->load->library(array('form_validation'));	
	}
	public function index()
	{
		$this->load->view('template/header');
	}
}
