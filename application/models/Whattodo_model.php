<?php 
class Whattodo_model extends CI_Model
{
    public function __construct() {
        parent::__construct();
    } 
    public function record_count($id) { // for counting rows
		$this->db->where('city', $id);
		$this->db->from('activities');
		$cnt = $this->db->count_all_results();
        return $cnt;
    } 
    public function get($limit,$start,$id) { // setting limit row and starting row
        $this->db->limit($limit,$start);
        $query = $this->db->get_where("activities",array('city'=>$id));		
		if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $row) {
                $data[] = $row;
            }			
            return $data;
        }		
        return false;
   }   
   public function gettag($limit,$start,$cityid,$tagid='') { // setting limit row and starting row		
        //$this->db->limit($limit,$start);
        $query = $this->db->get_where("activities",array('city'=>$cityid));
		if ($query->num_rows() > 0) {
			$result  = $query->result_array();
			foreach ($result as $row) {
				$tags = explode(',',$row['tag']);
				foreach($tags as $tag){				
					if($tag == $tagid){
						$data[] = $row;
					}
				}	
			}
			if(isset($data)){
				$data1 = array_slice($data, $start, $limit, true);
				return $data1;
			}else{
				return false;
			}           
        }		
        return false;
   }   
    public function record_tagcount($id,$tagid) {// for counting rows
   		 $query = $this->db->get_where("activities",array('city'=>$id));
		 if ($query->num_rows() > 0) {
			 $data ='';
			$result  = $query->result_array();
			foreach ($result as $row) {
				$tags = explode(',',$row['tag']);
				$cnt = '';
				foreach($tags as $tag){				
					if($tag == $tagid){
						$data[] = $row;						
					}
				}
				$cnt = count($data);	
			}
			return $cnt;
        }		
    } 
	public function getactivity($id) { // setting limit row and starting row 	
		$query = $this->db->get_where("activities",array('id'=>$id));		
		if ($query->num_rows() > 0){
            foreach ($query->result_array() as $row){
                $data[] = $row;
            }
            return $data;
        }		
        return false;
   }
}
?>