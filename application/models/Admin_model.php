<?php
/**	
 * @file
 *  Program		: admin_model.php 
 * 	Author		: ETK
 * 	Date		: 21/05/2014
 * 	Abstract	: admin user
*/
class admin_model extends CI_Model{
	function admin_model(){
		parent::__construct();
	}
	function login(){
		$name = $this->input->post('username');
		$pass = $this->input->post('password');
		
		$this->db->select('salt');
		$this->db->from('admin');
		$this->db->where('username', $name);
		$this->db->limit(1);
		$query = $this->db->get();
				
		if($query->num_rows() == 1){
			$user = $query->result_array();
			$salt = $user[0]['salt'];
			$password = base64_encode(sha1($pass .$salt, true) . $salt);
			$this->db->select('id, username, password, firstname, aUser_lastName, aUser_type');
			$this->db->from('admin');
			$this->db->where('username', $name);
			$this->db->where('aUser_password', $password);
			$this->db->limit(1);
			$query = $this->db->get();
			//echo '<pre>';print_r($this->db->queries);echo '</pre>';exit;
			if($query->num_rows() == 1){				
				return $query->result();
			}else{
				return false;
			}
		}else{
			return false;
		}
	}
}
?>