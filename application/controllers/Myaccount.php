<?php
/**	
 * @file
 *  Program		: attraction.php 
 * 	Author		: CMS
 * 	Date		: 17/7/2015
 * 	Abstract	: Attraction
*/
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Myaccount extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model('Myaccount_model');
		$this->load->library(array('form_validation'));	
	}
	function index()
    {
		$this->load->view('yangon');
		$this->load->view('template/footer');
    }
	function show($name='')
    {
		if(!$this->session->userdata('user_logged_in')){
			redirect(base_url().'login');
		}else{	
			//$data['userinfo'] = $this->session->userdata('user_logged_in');
			$data['userinfo'] = $this->Myaccount_model->getProfile();
			$data['menu'] = 'my account';
			$this->load->view('template/header', $data);	
			if($name == 'requestform'){
				$data['fName'] 	= '';
				$data['fAddress'] 	= '';
				$data['fPrice'] 	= '';
				$data['fPhone'] 	= '';
				$data['fFax']       = '';
				$data['fEmail'] 	= '';
				$data['fWebsite'] 	= '';
				$cities = $this->Myaccount_model->getcity();
				foreach ($cities as $city){
					$data['allCity'][$city['id']] = $city['name'];
				}	
				$this->load->view($name,$data);
			}else if($name == 'changepassword'){
				$this->load->view($name);
			}else if($name == 'profile'){
				$this->load->view($name);
			}else{
				$this->load->view('myaccount');
			}
		}		
		$this->load->view('template/footer');
    }
	function profile()
    {
		if(!$this->session->userdata('user_logged_in')){
			redirect(base_url().'login');
		}else{
			$data['userinfo'] = $this->session->userdata('user_logged_in');
			//echo '<pre>';print_r($data['userinfo']);echo '</pre>';
			$data['menu'] = 'my account';
			$result = $this->Myaccount_model->profile();
			$data['userinfo'] = $this->Myaccount_model->getProfile();
			if($result){
			$data ['msg'] = 'your profile has been changed';
			}else{
				$data ['error'] = 'you can not change your information. Please contact us';
			}
			
			$this->load->view('template/header', $data);	
			$this->load->view('profile',$data);			
			$this->load->view('template/footer');
		}
    }
	
	function changepassword(){
		if(!$this->session->userdata('user_logged_in')){
			redirect(base_url().'login');
		}else{
			$data['menu'] = 'my account';
			$this->load->view('template/header', $data);
			$this->form_validation->set_rules('curpassword','current password','trim|required|xss_clean');
			$this->form_validation->set_rules('password','password','trim|required|xss_clean');
			$this->form_validation->set_rules('conpassword','confirm password','trim|required|xss_clean|matches[password]');
			if($this->form_validation->run()===TRUE){
				$result = $this->Myaccount_model->changepassword();
				
				if($result === TRUE)
				{
					$data['msg'] = "your password has been changed";
					$this->load->view('changepassword',$data);
				}else if($result == 'incorrect')
				{
					$data['error'] = "your old password is incorrect";
					$this->load->view('changepassword',$data);
				}else{
					$data['error'] = "you can not change your password";
					$this->load->view('changepassword',$data);
				}
			}else{
				$this->load->view('changepassword');
			}
			$this->load->view('template/footer');
		}
	}
	
	function requestadd(){
		$this->form_validation->set_rules('name','Name','trim|required|xss_clean');
		$this->form_validation->set_rules('address','Address','trim|required|xss_clean');
		$this->form_validation->set_rules('email','Email','trim|required|xss_clean|valid_email|is_unique[user_request.email]');
		$this->form_validation->set_rules('phone','Phone','trim|required|xss_clean');
		$this->form_validation->set_rules('fax','Fax','trim|required|xss_clean|integer');
		$this->form_validation->set_rules('website','Website','trim|required|xss_clean');
		$this->form_validation->set_rules('price','Price range','trim|required|xss_clean|integer');
		
		$cities = $this->Myaccount_model->getcity();
		foreach ($cities as $city){
			$data['allCity'][$city['id']] = $city['name'];
		}		
		if($this->form_validation->run()===TRUE){
			$cities = $this->Myaccount_model->getcity();
			foreach ($cities as $city){
				$data['allCity'][$city['id']] = $city['name'];
			}
			$config['upload_path'] = './images/userrequest/upload';
			$config['allowed_types'] = 'gif|jpg|png';
			$config['max_size']	= '1024';
			$config['max_width']  = '1024';
			$config['max_height']  = '768';
			$this->load->library('upload');
			$this->upload->initialize($config);
			$upload = $this->upload->do_upload('upload');
			if(!$upload){					
				$data['error'] = $this->upload->display_errors();
				$uploaderror =$this->upload->display_errors();
				$data['fName'] 	    = $this->input->post('name');
				$data['fAddress'] 	= $this->input->post('address');
				$data['fPrice'] 	= $this->input->post('price');
				$data['fPhone'] 	= $this->input->post('phone');
				$data['fFax']       = $this->input->post('fax');
				$data['fEmail'] 	= $this->input->post('email');
				$data['fWebsite'] 	= $this->input->post('website');											
				$this->session->set_flashdata('data', $data);
				redirect(site_url('myaccount/show/requestform'));
			}else{
				$fileName1 = $this->upload->data('file_name');	
				$fullpath1 = $this->upload->data('full_path');					
				$configlogo['upload_path'] = './images/userrequest/upload/logo/';
				$configlogo['allowed_types'] = 'gif|jpg|png';
				$configlogo['max_size']	= '800';
				$configlogo['max_width']  = '800';
				$configlogo['max_height']  = '800';
				$this->load->library('upload');
				$this->upload->initialize($configlogo);
				$logoupload = $this->upload->do_upload('logo');				
				if(!$logoupload){	
					$data['error'] = $this->upload->display_errors();
					$uploaderror =$this->upload->display_errors();
					$data['fName'] 	    = $this->input->post('name');
					$data['fAddress'] 	= $this->input->post('address');
					$data['fPrice'] 	= $this->input->post('price');
					$data['fPhone'] 	= $this->input->post('phone');
					$data['fFax']       = $this->input->post('fax');
					$data['fEmail'] 	= $this->input->post('email');
					$data['fWebsite'] 	= $this->input->post('website');											
					$this->session->set_flashdata('data', $data);
					redirect(site_url('myaccount/show/requestform'));
				}else{
					$fileName2 = $this->upload->data('file_name');	
					$fullpath2 = $this->upload->data('full_path');			
					$fullpath1 = explode('travel/',$fullpath1);
					$fullpath1 = base_url().$fullpath1[1];				
					$fullpath2 = explode('travel/',$fullpath2);
					$fullpath2 = base_url().$fullpath2[1];
					$result = $this->Myaccount_model->requestadd($fileName1, $fileName2,$fullpath1,$fullpath2);
					if($result)
					{
						$data['msg'] 	= 'your request information has been record. Thank you!';						
						$this->session->set_flashdata('data', $data);
						redirect(site_url('myaccount/show/requestform')); 	
					}else{
						$data['error']	= 'your request information can not be record.Please try again';						
						$this->session->set_flashdata('data', $data);
						redirect(site_url('myaccount/show/requestform'));
					}	
				}											
			}
		}else{
				$data['fName'] 	= $this->input->post('name');
				$data['fAddress'] 	= $this->input->post('address');
				$data['fPrice'] 	= $this->input->post('price');
				$data['fPhone'] 	= $this->input->post('phone');
				$data['fFax']       = $this->input->post('fax');
				$data['fEmail'] 	= $this->input->post('email');
				$data['fWebsite'] 	= $this->input->post('website');
			}
			$data['menu'] = 'my account';
			$this->load->view('template/header', $data);
			$this->load->view('requestform', $data); 
			$this->load->view('template/footer');
	}
}
?>