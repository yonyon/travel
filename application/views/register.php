<?php
//include 'description.php';
?>
<style>
div.container div#registerPage {
	padding:8em 0 7em;
}
</style>
<!--<div class="container">-->
    <div class="banner-bottom" align="center" id="registerPage">
        <div class="well span5 center login-box res">               
            <div class="alert alert-info">
				<?php if(isset($login))
                { 
					$login_url = base_url().'login';
					echo 'your acount has been registered.you can login <a href="' .$login_url. '" style="font-style:italic">here</a>';						
                }else{ ?>Please fill these fields.<?php } ?>
            </div>
            <?php 
			$attributes = array('class'=>'form-horizontal','method'=>'post');
			echo form_open_multipart(base_url().'register/save', $attributes);
            ?>
            <table class="tbl-register" style="text-align:left">
                <tr>
                    <td>Name</td>
                    <td>
                    <?php
						$username = array('class'=>'input-large','name'=>'name','id'=>'name','autofocus'=>'autofocus');
						echo form_input($username, $fName); 
						echo form_error('name', '<div class="error">', '</div>');
                    ?>
                    </td>
                </tr>    
                <tr>
                    <td>Email</td>
                    <td>
                    <?php 
						$email = array('class'=>'input-large','name'=>'email','id'=>'email','autofocus'=>'autofocus');
						echo form_input($email,$fEmail);
						echo form_error('email', '<div class="error">', '</div>'); 
                    ?>
                	</td>
                </tr>    
                <tr>
                    <td> Password</td>
                    <td>
                    <?php 
						$password = array('class'=>'input-large','name'=>'password','id'=>'password','autofocus'=>'autofocus');
						echo form_password($password); 
						echo form_error('password', '<div class="error">', '</div>');
                    ?>
                    </td>
                </tr>    
                <tr>
                    <td> Cofirm password</td>
                    <td>
                    <?php 
						$conpassword = array('class'=>'input-large','name'=>'conpassword','id'=>'conpassword','autofocus'=>'autofocus');
						echo form_password($conpassword); 
						echo form_error('conpassword', '<div class="error">', '</div>');
                    ?>                    
                    </td>
                </tr>   
                <tr>
                    <td> Country</td>
                    <td>
                    <?php 
						$country = array('class'=>'input-large','name'=>'country','id'=>'country');
						$options = array(
						'Myanmar'=>'Myanmar',
						'Singapore'=>'Singapore',
						'China'=>'China',
						'America'=>'America',
						'French'=>'French',
						'Europe'=>'Europe',
						'Korea'=>'Korea',
						'Thailand'=>'Thailand');
						echo form_dropdown($country,$options,$fCountry); 
                    ?>
                    </td>
                </tr>   
                <tr>                                	
                    <td colspan="2">
                        <button type="submit" class="btn btn-primary btnres" name="submit" value="true">Sign up</button>                        
                    </td>
                </tr>   
            </table>
            <?php echo form_close(); ?>
            	Already have an account? Login <a href="<?php echo base_url() ?>login">here</a>            
            
        </div><!--/span-->
    <!--</div>--><!--/fluid-row-->
    
