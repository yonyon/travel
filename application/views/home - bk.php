<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE html>
<html>
<head>
<title>VantageTrip</title>

<link href="css/style1.css" rel="stylesheet" type="text/css" media="all" />
<link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
<link href="<?php echo base_url();?>css/bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url();?>css/navbar.css" rel="stylesheet">
<!--<link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Roboto+Slab:300,700,100,400' rel='stylesheet' type='text/css'>-->
<!-- js -->
<script src="js/jquery.min.js"></script>
<script src="<?php echo base_url();?>js/bootstrap.min.js"></script>
<!-- //js -->
<!-- for-mobile-apps -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Eco Travel Responsive web template, Bootstrap Web Templates, Flat Web Templates, Andriod Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyErricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
		function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- //for-mobile-apps -->
<!-- start-smoth-scrolling -->
<script type="text/javascript" src="js/move-top.js"></script>
<script type="text/javascript" src="js/easing.js"></script>
<script type="text/javascript">
	jQuery(document).ready(function($) {
		$(".scroll").click(function(event){		
			event.preventDefault();
			$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
		});
	});
</script>
<!-- start-smoth-scrolling -->
</head>
	
<body>
<!-- banner -->
	<div class="banner-with-text">
		<div class="container">
<!-- header -->	
		<div class="header">
			<div class="header-top">
				<input type="text" placeholder="Search" required=" ">
			</div>
			<div class="clearfix"> </div>
			<div class="header-bottom">
				<div class="header-bottom-left">
					<a href="index.html">Vantage<br>Trip</span></a>
				</div>
				<div class="header-bottom-right">
					<span class="menu">MENU</span>
					<ul class="nav1">
						<li class="active"><a href="<?php echo base_url(); ?>home">Home</a></li>
                        <li class="dropdown" class="upp"><a href="#" class="dropdown-toggle" data-toggle="dropdown">Cities<b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                <?php 
									$query = $this->db->get('city');			
									$results = $query->result_array();
									$rsize = sizeof($results);
									$i = 1;
									$addClass = '';
									foreach($results as $result)
									{
										if($i == $rsize) $addClass = 'class="last"';
										$linkpage=preg_replace('/\s+/', '', $result['name']);
                            			$linkpage = 'Details/show/city';
										echo '<li '.$addClass.'>';
										$linkpage = base_url().$linkpage;
										echo '<a href="' .$linkpage .'/'.$result['id']. '">' .$result['name'] . '</a>';
										echo '</li>';
										$i++;
									}
								?>
                            </ul>
                        </li>
						<li><a href="<?php echo base_url(); ?>essinfo/show/1">Essential Information</a></li>
                        <li><a href="<?php echo base_url(); ?>history/show/1">History</a></li>
                        <li><a href="<?php echo base_url(); ?>culture/show/1">Culture</a></li>
                        <li><a href="<?php echo base_url(); ?>tripplan/show/1">Trip plan</a></li>
                        <li><a href="<?php echo base_url(); ?>tip/show/1">Tips</a></li>
                      	<?php  if ($this->session->userdata('user_logged_in')){ ?>
                        <li><a href="<?php echo base_url(); ?>myaccount/show">My account</a></li>
                         <li><a href="<?php echo base_url(); ?>login/out">logout</a></li>
            			<?php }else{?>
                        <li><a href="<?php echo base_url(); ?>login">Login</a></li>
                        <li><a href="<?php echo base_url(); ?>register">Register</a></li>
						<li><a href="<?php echo base_url(); ?>contact">Contact</a></li>
                        <?php }?>
					</ul>
					<!-- script for menu -->
						<script> 
							$( "span.menu" ).click(function() {
							$( "ul.nav1" ).slideToggle( 300, function() {
							 // Animation complete.
							});
							});
						</script>
					<!-- //script for menu -->
				</div>
				<div class="clearfix"> </div>
			</div>
		</div>	
<!-- //header -->
<!-- Slider-starts-Here -->
				<script src="js/responsiveslides.min.js"></script>
				 <script>
				    // You can also use "$(window).load(function() {"
				    $(function () {
				      // Slideshow 4
				      $("#slider3").responsiveSlides({
				        auto: true,
				        pager: true,
				        nav: false,
				        speed: 500,
				        namespace: "callbacks",
				        before: function () {
				          $('.events').append("<li>before event fired.</li>");
				        },
				        after: function () {
				          $('.events').append("<li>after event fired.</li>");
				        }
				      });
				
				    });
				  </script>
			<!--//End-slider-script -->
			<div  id="top" class="callbacks_container wow fadeInUp" data-wow-delay="0.5s">
				<ul class="rslides" id="slider3">
				<?php
				$className = 'banner';
				$i = 0;
				foreach($cities as $city){
					extract($city);
					//$desc = substr($description , 0,100)."....";
				?>
					<li>
					<div class="<?php echo $className; ?>">
						<div class="jumbotron banner-info">
							<h1><?php echo $name; ?></h1>
							<div id="description">
							<p><?php echo $description; ?></p>
							</div>
							<p><a class="btn btn-primary btn-lg" href="<?php echo base_url(); ?>Details/show/city/<?php echo $id; ?>" role="button">READ MORE</a></p>
						</div>	
					</div>
					</li>
				<?php
				}
				?>
				</ul>
			</div>
	</div>
	</div>
<!-- //banner -->
<!-- banner-bottom -->
	<div class="banner-bdy">
		<div class="container">
	<div class="banner-bottom" id="events">
			<div class="banner-bottom-grids">
				<?php
				foreach($tripplans as $plan){
					extract($plan);
				?>
					<div class="col-md-4 banner-bottom-grid">
						<img src="<?php echo $image_url; ?>" alt=" "/>
						<div class="more">
							<a href="<?php echo base_url(); ?>Details/show/tripplan/<?php echo $id; ?>">More</a>
						</div>
					</div>
				<?php
				}
				?>
				<div class="clearfix"> </div>
			</div>
			<div class="features" id="essentialInfo">
				<div class="col-md-9 features-left">
					<h3>Essential Information</h3>
					<?php
					if(sizeof($essInfo) > 0){
						$detailsUrl = '';
						//echo '<pre>';print_r($essInfo);echo '</pre>';
						foreach($essInfo as $one){
							extract($one);
							$detailsUrl = base_url().'Details/show/information/'.$id;
						?>
							<div class="features-left-grids">
								<div class="col-md-4 features-left-grid">
									<a href="<?php echo $detailsUrl; ?>"><img src="<?php echo $image_url; ?>" alt=" " /></a>
								</div>
								<div class="col-md-8 jumbotron features-left-grid1">
									<h4><a href="<?php echo $detailsUrl; ?>"><?php echo $name; ?></a></h4>
									<p><?php echo substr($description , 0,500)."...."; ?></p>
									<p><a class="btn read btn-primary btn-lg" href="<?php echo $detailsUrl; ?>" role="button">Read More</a></p>
								</div>
								<div class="clearfix"> </div>
							</div>
						<?php
						}
					}
					?>
					
				</div>
				<div class="col-md-3 features-right">
					<div class="features-rgt">
						<h3>Upcoming Events</h3>
						<div class="features-rgt-grid">
							<div class="features-rgt-grid-left">
								<h4><a href="single.html">Culpa Qui Officia</a></h4>
								<p>cupiditate non provident</p>
								<a href="single.html">More Info</a>
							</div>
							<div class="features-rgt-grid-right">
								<p>15th May</p>
							</div>
							<div class="clearfix"> </div>
						</div>
						<div class="features-rgt-grid">
							<div class="features-rgt-grid-left">
								<h4><a href="single.html">Culpa Qui Officia</a></h4>
								<p>cupiditate non provident</p>
								<a href="single.html">More Info</a>
							</div>
							<div class="features-rgt-grid-right">
								<p>15th May</p>
							</div>
							<div class="clearfix"> </div>
						</div>
						<div class="features-rgt-grid">
							<div class="features-rgt-grid-left">
								<h4><a href="single.html">Culpa Qui Officia</a></h4>
								<p>cupiditate non provident</p>
								<a href="single.html">More Info</a>
							</div>
							<div class="features-rgt-grid-right">
								<p>15th May</p>
							</div>
							<div class="clearfix"> </div>
						</div>
						<div class="all-events">
							<a href="single.html">All Events</a>
						</div>
					</div>
				</div>
				<div class="clearfix"> </div>
			</div>
	</div>
<!-- //banner-bottom -->
<!-- slider -->
	<div class="sliderfig">
		<ul id="flexiselDemo1">	
			<li>
				<div class="sliderfig-grids">
					<div class="sliderfig-grid">
						<img src="<?php echo $activities[0]['image_url']; ?>" alt=" " />
						<div class="slider-text">
							<a href="<?php echo base_url(); ?>Details/show/activities/<?php echo $activities[0]['id']; ?>"><p><?php echo $activities[0]['name']; ?></p></a>
						</div>
					</div>
				</div>
			</li>	
			<li>
				<div class="sliderfig-grids">
					<div class="sliderfig-grid">
						<img src="<?php echo $attractions[0]['image_url']; ?>" alt=" " />
						<div class="slider-text">
							<a href="<?php echo base_url(); ?>Details/show/attraction/<?php echo $attractions[0]['id']; ?>"><p><?php echo $attractions[0]['name']; ?></p></a>
						</div>
					</div>
				</div>
			</li>	
			<li>
				<div class="sliderfig-grids">
					<div class="sliderfig-grid">
						<img src="<?php echo $hotels[0]['image_url']; ?>" alt=" " />
						<div class="slider-text">
							<a href="<?php echo base_url(); ?>Details/show/hotel/<?php echo $hotels[0]['id']; ?>"><p><?php echo $hotels[0]['name']; ?></p></a>
						</div>
					</div>
				</div>
			</li>	
			<li>
				<div class="sliderfig-grids">
					<div class="sliderfig-grid">
						<img src="<?php echo $restaurants[0]['image_url']; ?>" alt=" " />
						<div class="slider-text">
							<a href="<?php echo base_url(); ?>Details/show/restaurant/<?php echo $restaurants[0]['id']; ?>"><p><?php echo $restaurants[0]['name']; ?></p></a>
						</div>
					</div>
				</div>
			</li>	
			</ul>
			<script type="text/javascript">
							$(window).load(function() {
								$("#flexiselDemo1").flexisel({
									visibleItems: 4,
									animationSpeed: 1000,
									autoPlay: true,
									autoPlaySpeed: 3000,    		
									pauseOnHover: true,
									enableResponsiveBreakpoints: true,
									responsiveBreakpoints: { 
										portrait: { 
											changePoint:480,
											visibleItems: 2
										}, 
										landscape: { 
											changePoint:640,
											visibleItems:3
										},
										tablet: { 
											changePoint:768,
											visibleItems: 3
										}
									}
								});
								
							});
					</script>
					<script type="text/javascript" src="js/jquery.flexisel.js"></script>
	</div>
	</div>
	</div>
<!-- //slider -->
<!-- footer -->
	<div class="footer-top">
	<div class="container">
		<div class="footer-top-grids">
			<div class="col-md-4 footer-top-grid">
				<h3>About VantageTrip</h3>
				<p>"At vero eos et accusamus et iusto odio dignissimos ducimus 
				qui blanditiis praesentium voluptatum deleniti atque corrupti quos 
				dolores et quas molestias excepturi sint occaecati cupiditate non 
				provident, similique sunt in culpa qui officia deserunt mollitia animi, 
				id est laborum et dolorum fuga. </p>
				<div class="read1">
					<a href="single.html">Read More</a>
				</div>
			</div>
			<div class="col-md-4 footer-top-grid">
				<h3>Connect With Us</h3>
				<div class="twi-txt">
					<div class="twi">
						<a href="#" class="twitter"></a>
					</div>
					<div class="twi-text">
						<p><a href="#">Follow Us On Twitter</a></p>
					</div>
					<div class="clearfix"> </div>
				</div>
				<div class="twi-txt1">
					<div class="twi">
						<a href="#" class="flickr"> </a>
					</div>
					<div class="twi-text">
						<p><a href="#">Check Us Out On Flickr</a></p>
					</div>
					<div class="clearfix"> </div>
				</div>
				<div class="twi-txt1">
					<div class="twi">
						<a href="#" class="facebook"> </a>
					</div>
					<div class="twi-text">
						<p><a href="#">Become a Fan On Facebook</a></p>
					</div>
					<div class="clearfix"> </div>
				</div>
			</div>
			<div class="col-md-4 footer-top-grid">
				<h3>Extra Features</h3>
				<ul class="last">
					<li><a href="#">Temporibus autem quibusdam</a></li>
					<li><a href="#">Et aut officiis debitis aut</a></li>
					<li><a href="#">Necessitatibus saepe eveniet</a></li>
					<li><a href="#">Ut et voluptates repudiandae</a></li>
					<li><a href="#">Molestiae non recusandae earum</a></li> 
					<li><a href="#">Rerum hic tenetur a sapiente delectus</a></li>
				</ul>
			</div>
			<div class="clearfix"> </div>
		</div>
	</div>
		<div class="footer">
			<p>Copyright © 2015 Eco Travel. All Rights Reserved | Design by<a href="http://w3layouts.com/"> W3layouts</a></p>
		</div>
	</div>
<!-- //footer -->	
<!-- here stars scrolling icon -->
	<script type="text/javascript">
		$(document).ready(function() {
			/*
				var defaults = {
				containerID: 'toTop', // fading element id
				containerHoverID: 'toTopHover', // fading element hover id
				scrollSpeed: 1200,
				easingType: 'linear' 
				};
			*/
								
			$().UItoTop({ easingType: 'easeOutQuart' });
								
			});
	</script>
<!-- //here ends scrolling icon -->
</body>
</html>