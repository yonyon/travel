<style>
div.container div#loginPage {
	padding:0em 2em 0em;
}
.list-group a{
	text-align:left;
}
a.selected{
	color:#000000;
}
div.banner-bottom div.header-desc-yangon{
	margin-top:2%;
}
</style>
<?php
	//include 'description.php';
	$flash = $this->session->flashdata('data');
	if(isset($flash['fName'])){
		$fName	    = $flash['fName'] ;
		$fAddress 	= $flash['fAddress'];
		$fPrice	= $flash['fPrice'];
		$fPhone 	= $flash['fPhone'];
		$fFax       = $flash['fFax'] ;
		$fEmail 	= $flash['fEmail'];
		$fWebsite 	= $flash['fWebsite'];
		$allCity 	= $flash['allCity'];
	}
?>
<!--<div class="container">-->
<div class="banner-bottom" align="center" id="loginPage">
    <div class="sidebar-offcanvas" id="sidebar" role="navigation">
        <div class="list-group">
            <a href="<?php echo base_url(); ?>myaccount/show/profile" class="list-group-item" >profile</a>
            <a href="<?php echo base_url(); ?>myaccount/show/changepassword" class="list-group-item ">change password</a>
            <a href="<?php echo base_url(); ?>myaccount/show/requestform" class="list-group-item selected">request listing</a>    
            <a href="<?php echo base_url(); ?>login/out" class="list-group-item">logout</a>    
        </div>        
    </div>
    <div class="header-desc-yangon" style="width:60%;" id="div-href">                 
        <div class="well changepass" id="div-request-form">               
            <div class="alert alert-info profiletitle">
                 <?php if(isset($flash['error'])){
                      echo '<span class=error >'.$flash['error'].'</span>' ; 
                      }else if(isset($flash['msg'])){
                      echo '<span class=text-success>'.$flash['msg'].'</span>' ; 
                      }elseif(isset($error)){
                      echo '<span class= error >'.$error.'</span>'; 
                      } else{ ?>please fill these fields.<?php } ?>
            </div>
				<?php 
                    $attributes = array('class'=>'form-horizontal','method'=>'post');
                    echo form_open_multipart(base_url().'myaccount/requestadd', $attributes);
                ?>
                <table class="tbl-register" style="text-align:left">  
                    <tr>
                        <td>Name</td>
                        <td>
							<?php
								$name = array('class'=>'input-large','name'=>'name','id'=>'name','autofocus'=>'autofocus');
								echo form_input($name,$fName);
								echo form_error('name', '<div class="error">', '</div>'); 
                            ?>
                        </td>
                    </tr>        
                    <tr>
                        <td> Type</td>
                        <td>
							<?php 
								$type = array('class'=>'input-large','name'=>'type','id'=>'type');
								$options = array(
								'hotel'=>'hotel',
								'restaurant'=>'restaurant');
								echo form_dropdown($type,$options); 
                            ?>
                        </td>
                    </tr>
                    <tr>
                        <td> City</td>
                        <td>
							<?php 
								$city = array('class'=>'input-large','name'=>'city','id'=>'city');	                            
								echo form_dropdown($city,$allCity); 
                            ?>
                        </td>
                    </tr>
                    <tr>
                        <td>Address</td>
                        <td>
							<?php 
								$address = array('class'=>'input-large','name'=>'address','id'=>'address','autofocus'=>'autofocus');
								echo form_input($address,$fAddress);
								echo form_error('address', '<div class="error">', '</div>'); 
                            ?>
                        </td>
                    </tr>    
                    <tr>
                        <td>Image</td>
                        <td>
							<?php 
								$image = array('class'=>'input-large','name'=>'upload','id'=>'upload','autofocus'=>'autofocus');
								echo form_upload($image);
								echo form_error('image', '<div class="error">', '</div>'); 
                            ?>
                        </td>
                    </tr>   
                    <tr>
                        <td>Logo</td>
                        <td>
							<?php 
								$logo = array('class'=>'input-large','name'=>'logo','id'=>'logo','autofocus'=>'autofocus');
								echo form_upload($logo);
								echo form_error('logo', '<div class="error">', '</div>'); 
                            ?>
                        </td>
                    </tr>   
                    <tr>
                        <td>Price range</td>
                        <td>
							<?php 
								$price = array('class'=>'input-large','name'=>'price','id'=>'price','autofocus'=>'autofocus');
								echo form_input($price,$fPrice);
								echo form_error('price', '<div class="error">', '</div>'); 
                            ?>
                        </td>
                    </tr>   
                    <tr>
                        <td>Phone</td>
                        <td>
							<?php 
								$phone = array('class'=>'input-large','name'=>'phone','id'=>'phone','autofocus'=>'autofocus');
								echo form_input($phone,$fPhone);
								echo form_error('phone', '<div class="error">', '</div>'); 
                            ?>
                        </td>
                    </tr>   
                    <tr>
                        <td>Fax</td>
                        <td>
							<?php 
								$fax = array('class'=>'input-large','name'=>'fax','id'=>'fax','autofocus'=>'autofocus');
								echo form_input($fax,$fFax);
								echo form_error('fax', '<div class="error">', '</div>'); 
                            ?>
                        </td>
                    </tr>   
                    <tr>
                        <td>Email</td>
                        <td>
							<?php 
								$email = array('class'=>'input-large','name'=>'email','id'=>'email','autofocus'=>'autofocus');
								echo form_input($email,$fEmail);
								echo form_error('email', '<div class="error">', '</div>'); 
                            ?>
                        </td>
                    </tr>   
                    <tr>
                    <td>Website</td>
                        <td>
							<?php 
								$website = array('class'=>'input-large','name'=>'website','id'=>'website','autofocus'=>'autofocus');
								echo form_input($website,$fWebsite);
								echo form_error('website', '<div class="error">', '</div>'); 
                            ?>
                        </td>
                    </tr>     
                    <tr>
                    <td colspan="2">
                    <button type="submit" class="btn btn-primary btnres" name="submit" value="true">Add</button>
                    </td>
                    </tr>  
                </table>
        <?php echo form_close(); ?>
        </div><!--/span-->      
    		
    </div>
	<div class="clear"></div> 
</div>
