
<?php 
	$attributes = array('class'=>'form-horizontal','method'=>'post');
	echo form_open_multipart(base_url().'test/upload', $attributes);
?>
    <table class="tbl-register" style="text-align:left"> 
        <tr>
            <td>Image</td>
            <td>
            <?php 
            $image = array('class'=>'input-large','name'=>'upload','id'=>'upload','autofocus'=>'autofocus');
            echo form_upload($image);
            echo form_error('image', '<div class="error">', '</div>'); 
            ?>
            </td>
        </tr>   
        <tr>
            <td>Logo</td>
            <td>
            <?php 
            $logo = array('class'=>'input-large','name'=>'logo','id'=>'logo','autofocus'=>'autofocus');
            echo form_upload($logo);
            echo form_error('logo', '<div class="error">', '</div>'); 
            ?>
            </td>
        </tr> 
            <td colspan="2">
            <button type="submit" class="btn btn-primary btnres" name="submit" value="true">Add</button>
            </td>
        </tr>  
    </table>
<?php echo form_close(); ?>
     