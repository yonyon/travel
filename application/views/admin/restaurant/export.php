<?php	
header("Content-type: application/csv");
header("Content-Disposition: attachment; filename=".$fileName.'.csv');
$output= '';
$output .= 'id,cityId,city_name,tagId,tag_keyword,classification,name,rating,description,promotion,address,price_range,phone,fax,email,website,image_url,logo_url,longitude,latitude,updated_date,created_date';
$output .="\r\n";
if(is_array($restaurant) && sizeof($restaurant) > 0){
	foreach($restaurant as $key=>$value){
		$created_date = redatetime($value['created_date']);
		$updated_date = redatetime($value['updated_date']);
		$cityid = $value['city'];
		$city = $this->db->get_where('city',array('id'=>$cityid));
		$result = $city->result_array();
		if($result){
			$cityname = $result[0]['name'];
		}else{
			$cityname = $cityid;
		}		
		
		$tagid = $value['tag'];	
		$tagsplitid = str_replace(",","-",$tagid);	
		$tag = getcolname('tag','keyword',$tagid);
		if($tag){
			$keyword = str_replace(",","-",$tag);
		}else{
			$keyword = $tagsplitid;
		}
		
		$desc = strip_tags($value['description']);
		$desc = str_replace(",","-",$desc);
		$address = str_replace(",","-",$value['address']);
			
		$output .= $value['id'].','.$cityid.','.$cityname.','.$tagsplitid.','.$keyword.','.$value['classification'].','.$value['name'].','.$value['rating'].','.$desc.','.$value['promotion'].','.$address.','.$value['price_range'].','.$value['phone'].','.$value['fax'].','.$value['email'].','.$value['website'].','.$value['image_url'].','.$value['logo_url'].','.$value['longitude'].','.$value['latitude'].','.$updated_date.','.$created_date;
		$output .="\r\n";
	}
}
echo $output;
exit;
?>
