<?php
/**	
 * @file
 *  Program		: Home_model.php 
 * 	Author		: ETK
 * 	Date		: 01/03/2016
 * 	Abstract	: home model
*/
class Home_model extends CI_Model{
	function home_model(){
		parent::__construct();
	}
	function getData($table){
		if($table == 'trip_plan') {
			$this->db->order_by('id','desc');
			$this->db->limit(3,0);
		}else if($table == 'activities' || $table == 'attraction' || $table == 'hotel' || $table == 'restaurant'){
			$this->db->order_by('id','desc');
			$this->db->limit(1,0);
		}else if($table == 'information'){
			$this->db->order_by('id','desc');
			$this->db->limit(2,0);
		}
		$query = $this->db->get($table);
		$result = $query->result_array();
		return $result;
	}
}
?>