<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Forgot_password extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model('Forgot_password_model');
		$this->load->helper(array('html', 'url', 'file'));
		$this->load->library(array('form_validation'));
		$this->load->helper('string');
	}
	public function index()
	{
		$data['menu'] = 'forgot password';
		$this->load->view('template/header', $data);
		$this->load->view('forgot_password');
		$this->load->view('template/footer');
		//$this->load->view('template/footer');
	}
	public function email()
	{
		$data['menu'] = 'forgot password';
		$email = $this->input->post('email');
		$this->form_validation->set_rules('email', 'Email', 'required|valid_email');
		if($this->form_validation->run()===TRUE){
			$random =  random_string('alnum', 30);	
			$result = $this->Forgot_password_model->checkemail($email,$random);
			if($result == TRUE)
			{
				$this->session->set_userdata('user_logged_in_email', $email);			
				$this->load->library('email');		
				$this->email->from('chanmyaesoe@appvantage.sg','Travel Myanmar');
				$this->email->to($email);
				$this->email->subject('Sending new password link');
				$msg = 'Please click the link below to change password<br/><br/>';
				$msg .= base_url().'forgot_password/getnew/' . $random;
				$this->email->message($msg);	
				if($this->email->send()){
					$data['error'] = 'you will receive a link to get new password in your email';
					$this->load->view('template/header', $data);
					$this->load->view('forgot_password',$data);
					$this->load->view('template/footer');
				}else{	
					$data['error'] = $this->email->print_debugger();
					$this->load->view('template/header', $data);
				    $this->load->view('forgot_password',$data);
				    $this->load->view('template/footer');
				}
			}
			else
			{
			   $data['error'] = 'your email does not exist in travelmyanmar.com';
			   $this->load->view('template/header', $data);
			   $this->load->view('forgot_password',$data);
			   $this->load->view('template/footer');
			}
		}else{
		    $this->load->view('template/header', $data);
			$this->load->view('forgot_password');
			$this->load->view('template/footer');
		}		
	}
	function getnew($random){
		$data['menu'] = 'forgot password';
		$email = $this->session->userdata('user_logged_in_email') ;  
		$result = $this->Forgot_password_model->checkrandom($email,$random);
		if($result == TRUE)
		{
			$data['menu'] = 'reset password';
			$this->load->view('template/header', $data);
			$this->load->view('reset_password');
			$this->load->view('template/footer');
		}else{
			$data ['error'] = "Your activation key is incorrect or expired.Please try again";
			$this->load->view('template/header', $data);
			$this->load->view('forgot_password',$data);
			$this->load->view('template/footer');
		}
	}
	function changepassword(){
		$data['menu'] = 'reset password';
		$email = $this->session->userdata('user_logged_in_email');
		$this->form_validation->set_rules('password','password','trim|required|xss_clean');
		$this->form_validation->set_rules('conpassword','confirm password','trim|required|xss_clean|matches[password]');
		if($this->form_validation->run()===TRUE){
			$result = $this->Forgot_password_model->changepassword($email);
			if($result == TRUE)
			{
				$data['msg'] = "your password has been changed";
				$this->load->view('template/header', $data);
				$this->load->view('reset_password',$data);
				$this->load->view('template/footer');
			}else{
				$data['error'] = "you can not change your password";
				$this->load->view('template/header', $data);
				$this->load->view('reset_password',$data);
				$this->load->view('template/footer');
			}
		}else{
			$this->load->view('template/header', $data);
			$this->load->view('reset_password');
			$this->load->view('template/footer');
		}
	}
}
