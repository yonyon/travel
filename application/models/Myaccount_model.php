<?php
/**	
 * @file
 *  Program		: user_model.php 
 * 	Author		: CMS
 * 	Date		: 20/07/2015
 * 	Abstract	: user
*/
class Myaccount_model extends CI_Model{
	function Myaccount_model(){
		parent::__construct();
		$this->load->library('phpass');
	}	
	function profile(){ // to save user data
		$useinfo  = $this->session->userdata('user_logged_in');
		$user_id = $useinfo['user_id'];
		$name 		= $this->input->post('name');
		$email 		= $this->input->post('email');
		$country 	= $this->input->post('country');
		$data = array(
				'name'=>$name, 
				'email'=>$email,
				'country'=>$country
			);
		//echo $user_id;
		//echo '<pre>';print_r($data);echo '</pre>';exit;
		$this->db->where('id',$user_id);
		$result = $this->db->update('user',$data);		
		if($result){
			return $result;
		}else{
			return false;
		}
	}
	function getProfile(){
		$useinfo  = $this->session->userdata('user_logged_in');
		$user_id = $useinfo['user_id'];
		$this->db->where('id',$user_id);
		$query = $this->db->get('user');
		$user = $query->result_array();
		//echo '<pre>';print_r($result);echo '</pre>';
		$result['user_id'] = $user[0]['id'];
		$result['user_name'] = $user[0]['name'];
		$result['user_email'] = $user[0]['email'];
		$result['user_country'] = $user[0]['country'];
		return $result;
	}
	function changepassword(){
		$useinfo  = $this->session->userdata('user_logged_in');
		$user_id = $useinfo['user_id'];
		$user_email = $useinfo['user_email'];
		$curpassword = $this->input->post('curpassword');
		//$curpassword = $this->phpass->hash($curpassword);		
		$query = $this->db->get_where('user',array('id'=>$user_id));
		$result = $query->result_array();
		$userpassword = $result[0]['password'];
		if($this->phpass->check($curpassword,$userpassword)){
			$password = $this->input->post('password');		
			$password = $this->phpass->hash($password);
			$this->db->where('id',$user_id);
			$data = array('password'=>$password);
			$result = $this->db->update('user',$data);
			if($result){
				return TRUE;
			}else{
				return FALSE;
			}			
		}else{
			return 'incorrect' ; 
		}	
	}
	
	function requestadd($name1,$name2,$fullpath1,$fullpath2){
		$name		= $this->input->post('name');
		$type		= $this->input->post('type');
		$address 		= $this->input->post('address');
		$price 	= $this->input->post('price');
		$phone 	= $this->input->post('phone');
		$fax 	= $this->input->post('fax');
		$email 		= $this->input->post('email');
		$website 		= $this->input->post('website');
		$city 		= $this->input->post('city');
		$data = array(
					'name'=>$name, 
					'type'=>$type,
					'address'=>$address, 
					'price'=>$price,
					'image'=>$name1,
					'image_url'=>$fullpath1,
					'logo'=>$name2,
					'logo_url'=>$fullpath2,
					'phone'=>$phone, 
					'fax'=>$fax,
					'email'=>$email,
					'website'=>$website,
					'status'=>0,
					'city'=>$city,
					'created_date' => date('Y-m-d')
				);
		$result = $this->db->insert('user_request', $data);
		if($result){
			return $result;
		}else{
		 return false;	
		}
	}
	function getcity(){ // get city  data
		$query = $this->db->get('city');		
		return $query->result_array();
	}
}
?>