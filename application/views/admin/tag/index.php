
<div class="title"><?php echo ($id > 0) ? 'Edit '.$title : 'New '.$title; ?></div>
<?php 
$attributes = array('class'=>'eventform');
echo form_open_multipart(base_url().'admin/tag/add/'.$id, $attributes);
if($error != '') $error = $error;
?>
<script type="text/javascript">
	$(document).ready(function(){
		$(".txtFromDate").datepicker({
			minDate: 0,
			maxDate: "+1530D",
			numberOfMonths: 1,
			onSelect: function(selected) {
			  $(".txtToDate").datepicker("option","minDate", selected)
			}
		});
		$(".txtToDate").datepicker({ 
			minDate: 0,
			maxDate:"+1530D",
			numberOfMonths: 1,
			onSelect: function(selected) {
			   $(".txtFromDate").datepicker("option","maxDate", selected)
			}
		});
	});
</script>
<table>
	<tr><td class="star_red" colspan="3"><?php echo $error; ?></td><td></td></tr>
    <?php
    if($id > 0){
       // var_dump($fName);
	?>
    <tr>
    	<td>ID : </td>
        <td><?php echo $id; ?></td>
    </tr>
    <?php
	}
	?>
      <tr>
    	<td><?php echo $keyword; ?></td>
        <td><?php echo form_textarea($fKeyword); 
		echo form_error('keyword', '<div class="error">', '</div>');
			?></td>
    </tr>
    <tr>    
    	<td><?php echo $cDate; ?></td>
        <td><?php echo form_input($fCdate);
		echo form_error('cDate', '<div class="error">', '</div>');
			?></td>
    </tr>
    <tr>
    	<td><?php echo $uDate; ?></td>
        <td><?php echo form_input($fUdate);
			echo form_error('uDate', '<div class="error">', '</div>');
		?></td>
    </tr>
    <tr>
        <td colspan="2">
			<?php
            $submit = array('name' => 'submit', 'content'=> 'Save', 'id'=> 'submit','type'=> 'submit', 'value' => 'true', 'onclick'=>'', 'class'=>'btn btn-primary');
            echo form_button($submit);
            $cancel = array('name' => 'cancel', 'content'=> 'Cancel', 'id'=> 'submit','type'=> 'submit', 'value' => 'true', 'class'=>'btn');
            echo form_button($cancel);
            ?>
        </td>
    </tr>
</table>
<?php echo form_close(); ?>