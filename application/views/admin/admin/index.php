
<div class="title"><?php echo ($id > 0) ? 'Edit '.$title : 'New '.$title; ?></div>
<?php 
$attributes = array('class'=>'eventform');
echo form_open(base_url().'admin/admin/add/'.$id, $attributes);
if($error != '') $error = $error;
?>
<script type="text/javascript">
	$(document).ready(function(){
		$(".txtFromDate").datepicker({
			minDate: 0,
			maxDate: "+1530D",
			numberOfMonths: 1,
			onSelect: function(selected) {
			  $(".txtToDate").datepicker("option","minDate", selected)
			}
		});
		$(".txtToDate").datepicker({ 
			minDate: 0,
			maxDate:"+1530D",
			numberOfMonths: 1,
			onSelect: function(selected) {
			   $(".txtFromDate").datepicker("option","maxDate", selected)
			}
		});
	});
</script>
<table>
	<tr><td class="star_red" colspan="3"><?php echo $error; ?></td><td></td></tr>
    <?php
    if($id > 0){
	?>
    <tr>
    	<td>ID : </td>
        <td><?php echo $id; ?></td>
    </tr>
    <?php
	}
	?>
      <tr>
    	<td><?php echo $fname; ?></td>
        <td><?php echo form_input($fName);
				  echo form_error('name', '<div class="error">', '</div>');
		 ?></td>
    </tr>
	<tr>
    	<td><?php echo $lname; ?></td>
        <td><?php echo form_input($fLname); 
				  echo form_error('lName', '<div class="error">', '</div>');
		?></td>
    </tr>   
    <tr>
    	<td valign="top"><?php echo $email; ?></td>
        <td><?php echo form_input($fEmail); 
				  echo form_error('email', '<div class="error">', '</div>');
		?></td>
    </tr>
    <tr>
    	<td valign="top"><?php echo $userName; ?></td>
        <td><?php echo form_input($fUserName); 
				  echo form_error('userName', '<div class="error">', '</div>');
		?></td>
    </tr>
    <tr>    
    	<td><?php echo $pass; ?></td>
        <td><?php echo form_password($fPass);
				  echo form_error('pass', '<div class="error">', '</div>');
		?></td>
    </tr>
     <tr>
    	<td valign="top"><?php echo $conPass; ?></td>
        <td><?php echo form_password($fConPass); 
				  echo form_error('conPass', '<div class="error">', '</div>');
		?></td>
    </tr>
    <tr>    
    	<td><?php echo $type; ?></td>
        <td><?php echo form_dropdown('type', $allType, $selectedType);?></td>
    </tr>
    
    <tr>
        <td colspan="2">
        <?php
		$submit = array('name' => 'submit', 'content'=> 'Save', 'id'=> 'submit','type'=> 'submit', 'value' => 'true', 'onclick'=>'', 'class'=>'btn btn-primary');
		echo form_button($submit);
		$cancel = array('name' => 'cancel', 'content'=> 'Cancel', 'id'=> 'submit','type'=> 'submit', 'value' => 'true', 'class'=>'btn');
		echo form_button($cancel);
		?>
        </td>
    </tr>
</table>
<?php echo form_close(); ?>