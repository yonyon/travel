<?php
/**	
 * @file
 *  Program		: api_model.php 
 * 	Author		: ETK
 * 	Date		: 23/05/2014
 * 	Abstract	: api
*/
class Test_model extends CI_Model{
	function test_model(){
		parent::__construct();
	}
	function checkToken($token=false){
		$check = false;
		if($token == false) $token = $this->input->post('token');
		$query = $this->db->get_where('user_token',array('user_token'=>$token));
		if($query->num_rows() > 0){
			$check = true;
		}
		return $check;
	}
	function getRestaurant(){
		$city = array();
		$allarray =  array();
		$tag  = array();
		$query = $this->db->get('city');		
		$result = $query->result_array();
		$cityid = '';
		$res_result= array();
		$city_result = array();
		foreach($result as $key=>$value){			
			$id = $result[$key]['id'];	
			$res_query = $this->db->get_where('restaurant',array('city'=>$id));
			$res_result []= $res_query->result_array();		
			//$result	=	array_merge($result,$res_result);			
					
		}		
		return $result;
	}
	function getcity(){
		$query = $this->db->get('city');		
		$result = $query->result_array();
		return $result;
	}
	function getattraction(){
		$query = $this->db->get('attractions');		
		$result = $query->result_array();
		return $result;
	} 
	function getactivities(){
		$query = $this->db->get('activities');		
		$result = $query->result_array();
		return $result;
	}  
	function gethotel(){
		$query = $this->db->get('hotel');		
		$result = $query->result_array();
		return $result;
	} 
	function getphrasebook(){
		$query = $this->db->get('phrase_book');		
		$result = $query->result_array();
		return $result;
	}
	
	function save($id, $encrypt_password){ // to save admin data
		var_dump($this->input->post()); echo "<br/><br/>";
		if($encrypt_password)
		{
			$salt = $encrypt_password['salt'];
			$password = $encrypt_password['encrypted'];
		}
		$fname 		= $this->input->post('name');
		$lname 		= $this->input->post('lName');
		$email 	    = $this->input->post('email');
		$username	= $this->input->post('userName');
		$type 		= $this->input->post('type');
		$data = array(
					'firstname'=>$fname, 
					'lastname'=>$lname,
					'email'=>$email, 
					'salt'=>$salt, 
					'username'=>$username, 
					'password'=>$password,  
					'type'=>$type, 
				);
		
		if(!empty($id) && $id > 0){
			$this->db->where('id',$id);
			$result = $this->db->update('admin',$data);	
		}else{
			$result = $this->db->insert('admin', $data);	
		}
		if($result){
			return $result;
		}else{
			return false;
		}
	}
	
}
?>