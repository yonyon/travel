<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Register extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model('Register_model');
		$this->load->helper(array('html', 'url', 'file'));
		$this->load->library(array('form_validation'));
	}
	public function index()
	{
		$data['fName'] 	    = '';
		$data['fEmail']     = '';
		$data['fCountry']	= '';	
		$data['menu'] 		= 'register';
		/*$this->load->view('register',$data);
		$this->load->view('template/footer');*/
		$this->load->view('template/header', $data);
		$this->load->view('register', $data);
		$this->load->view('template/footer');
	}
	public function save()
	{
		
		$this->form_validation->set_rules('name','Name','trim|required|xss_clean');
		$this->form_validation->set_rules('email', 'Email', 'required|xss_clean|valid_email|is_unique[user.email]');
		$this->form_validation->set_rules('password','password','trim|required|xss_clean');
		$this->form_validation->set_rules('conpassword','confirm password','trim|required|xss_clean|matches[password]');
		if($this->form_validation->run()===TRUE){	
			extract($this->input->post());
			$email = $this->input->post('email');
			$password = $this->input->post('password');
			$conpassword = $this->input->post('conpassword');										
			$result = $this->Register_model->save();
			if($result){
				$data['fName'] 	= '';
				$data['fEmail'] 	= '';
				$data['fCountry']	= '';
				$data['login'] = 'login';
				$data['menu'] 		= 'register';
				/*$this->load->view('register',$data);
				$this->load->view('template/footer');*/
				$this->load->view('template/header', $data);
				$this->load->view('register', $data);
				$this->load->view('template/footer');
			}
		}else{
			$data['fName'] 	= $this->input->post('name');
			$data['fEmail'] 	= $this->input->post('email');
			$data['fCountry']	= $this->input->post('country');
			$data['menu'] 		= 'register';	
			/*$this->load->view('register',$data);
			$this->load->view('template/footer');*/
			$this->load->view('template/header', $data);
			$this->load->view('register', $data);
			$this->load->view('template/footer');
		}
	}
}
