<script type="text/javascript" charset="utf-8">
	$(document).ready(function() {
		var oTable;
		oTable = $('#DataTables').dataTable( {			
			"bJQueryUI": true,
			"sPaginationType": "full_numbers",
			"bProcessing": true,
			"bServerSide": true,
			"sAjaxSource": '<?php echo base_url(); ?>/admin/phrasebook/getdatatable',
			"aoColumns": [
			null, null, null,null,null,null,
			{ "sClass": "center", "bSortable": false },
			{ "sClass": "center", "bSortable": false }
			],
			"aaSorting": [[0, 'desc']],
			"iDisplayLength":10,			
		});
	});
</script>
<style>
	.paging_full_numbers {
		width: 280px !important;
	}
</style>

<div id="toolbar"style="float:left; margin-left:30px; margin-left:5px;">
	<a class="btn-add" href="<?php echo base_url(); ?>admin/phrasebook/add" style="text-decoration:none;">Add phrase</a>
</div>
<div id="toolbar" style="float:left; margin-left:30px;">
    <a href="<?php echo base_url(); ?>admin/phrasebook/export">Export Excel</a>
</div>
<div id="toolbar" style="float:left; margin-left:30px;" >
    <a href="<?php echo base_url(); ?>admin/phrasebook/import">Import Excel</a>
</div>
<div style="width:900px; text-align:left; margin-left:5px;">
<table cellspacing="0" cellpadding="0" border="0" class="display" id="DataTables" bgcolor="#ffffff">
  <thead>
    <tr>
    	<th> ID </th>
        <th> MM </th>
        <th> En </th>        
        <th> Phonetic</th>
        <th width="50"> audio </th>
        <th> Created Date</th>
        <th width="50"> </th>
        <th width="50"> </th>
    </tr>
  </thead>
  <tbody>
 </tbody>
</table>
</div>
