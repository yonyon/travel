<?php
/**	
 * @file
 *  Program		: hotel_model.php 
 * 	Author		: CMS
 * 	Date		: 17/07/2015
 * 	Abstract	: hotel
*/
class Hotel_model extends CI_Model{
	function hotel_model(){
		parent::__construct();
	}
	//create form
	function form(){
		$data['city']	='City <span class="star_red">*</span> :';
		$data['image']  ='Image <span class="star_red">*</span> :';
		$data['logo']  ='Logo <span class="star_red">*</span> :';
		$data['tag']	='Tag <span class="star_red">*</span> :';
		$data['name']		='Name <span class="star_red">*</span> :';
		$data['desc']	='Description <span class="star_red">*</span> :';	
		$data['address']	='Address <span class="star_red">*</span> :';	
		$data['promotion']	='Promotion';
		$data['phone'] ='Phone <span class="star_red">*</span> :';		
		$data['fax'] ='Fax <span class="star_red">*</span> :';
		$data['email'] ='Email <span class="star_red">*</span> :';
		$data['website'] ='Website <span class="star_red">*</span> :';	
		$data['price']	='Price <span class="star_red">*</span> :';	
		$data['longitude']	='Longitude <span class="star_red">*</span> :';
		$data['latitude']	='Latitude <span class="star_red">*</span> :';
		$data['howtoget'] ='How to get there <span class="star_red">*</span> :';		
		$data['cDate']     ='created Date <span class="star_red">*</span> :';
		$data['uDate']     ='updated Date <span class="star_red">*</span> :';		
		
		$data['fName']		= array('name'=>'name', 'id'=>'name', 'value'=>'', 'rows'=>'8');
		$data['fDesc'] = array('name'=>'desc', 'id'=>'desc', 'value'=>'', 'rows'=>'8');
		$data['fpromotion']= array('name'=>'promotion', 'id'=>'promotion', 'value'=>'', 'rows'=>'8');
		$data['fAddress']	= array('name'=>'address', 'id'=>'address', 'value'=>'', 'rows'=>'5');
		$data['fImage']	= array('name'=>'upload', 'id'=>'upload', 'value'=>'');
		$data['fLogo']	= array('name'=>'logo', 'id'=>'logo', 'value'=>'');
		$data['fPhone']	= array('name'=>'phone', 'id'=>'phone', 'value'=>'', 'rows'=>'8');
		$data['fFax']	= array('name'=>'fax', 'id'=>'fax', 'value'=>'', 'rows'=>'8');
		$data['fEmail']	= array('name'=>'email', 'id'=>'email', 'value'=>'', 'rows'=>'8');
		$data['fWebsite']	= array('name'=>'website', 'id'=>'website', 'value'=>'', 'rows'=>'8');
		$data['fPrice']	= array('name'=>'price', 'id'=>'price', 'value'=>'', 'rows'=>'5');
		$data['fCdate']	= array('name'=>'cDate', 'id'=>'cDate', 'placeholder'=>'dd/mm/yyyy', 'value'=>'', 'class'=>'txtFromDate c_in');
		$data['fUdate']	= array('name'=>'uDate', 'id'=>'uDate', 'placeholder'=>'dd/mm/yyyy', 'value'=>'', 'class'=>'txtToDate c_out');
		$data['fHowtoget'] = array('name'=>'howtoget', 'id'=>'howtoget',  'value'=>'', 'class'=>'txtFromDate c_in');
		$data['fLongitude']= array('name'=>'longitude', 'id'=>'longitude', 'value'=>'', 'rows'=>'5');
		$data['fLatitude']	= array('name'=>'latitude', 'id'=>'latitude', 'value'=>'', 'rows'=>'5');
		return $data;
	}
	// to save hotel data
	function save($id,$fullpath1,$fullpath2){
		
		$finaltagid = '';
		$tagid = '';
		$tags = $this->input->post('tags');
		$insertresult = false;
		foreach($tags as $key){
			$query = $this->db->get_where('tag',array('keyword'=>$key));
			if($query->num_rows() > 0){
				$result = $query->result_array();
				$tagid = $result[0]['id'];
			}else{
				$data = array('keyword'=>$key,'created_date'=>date('Y-m-d'),'updated_date'=>date('Y-m-d'));
				$queryresult = $this->db->insert('tag', $data);
				$insertresult = true;
			}
			if($insertresult){
				$tagid = $this->db->insert_id();
				$insertresult = false;
			}
			 $finaltagid .= $tagid.",";
		}
		
		$name 		= $this->input->post('name');
		$desc 		= $this->input->post('desc');
		$city 		= $this->input->post('city');
		$tag 		= $finaltagid;
		$howtoget 	= $this->input->post('howtoget');
		$address 	= $this->input->post('address');
		$promotion	= $this->input->post('promotion');
		$phone 		= $this->input->post('phone');
		$fax 		= $this->input->post('fax');
		$email 		= $this->input->post('email');
		$website 	= $this->input->post('website');
		$longitude 	= $this->input->post('longitude');
		$latitude 	= $this->input->post('latitude');
		$cDate 		= $this->input->post('cDate');
		$uDate 		= $this->input->post('uDate');
		$price 		= $this->input->post('price');
		if(!empty($cDate)){
			$created = explode('/', $cDate);	
			$cDate = $created[2].'-'.$created[1].'-'.$created[0];
		}
		if(!empty($uDate)){
			$updated = explode('/', $uDate);	
			$uDate = $updated[2].'-'.$updated[1].'-'.$updated[0];
		}
		$hotel = array(
					'name'=>$name, 
					'city'=>$city, 
					'tag'=>$tag, 
					'description'=>$desc,
					'how_to_get_there'=>$howtoget,
					'address'=>$address,
					'promotion'=>$promotion,			
					'image_url'=>$fullpath1,			
					'logo_url'=>$fullpath2,
					'price_range'=>$price,  
					'phone'=>$phone,
					'fax'=>$fax,
					'email'=>$email,
					'website'=>$website,
					'longitude'=>$longitude,  
					'latitude'=>$latitude, 
					'created_date'=>$cDate,
					'updated_date'=>$uDate
				);
		
		if(!empty($id) && $id > 0){
			$query1= $this->db->get_where('hotel',array('id'=>$id));
			$result = $query1->result_array();			
			// if($fullpath1 != $result[0]['image_url']){
			// 	$img = explode('upload/',$result[0]['image_url']);
			// 	$file = $_SERVER['DOCUMENT_ROOT'] . '/travel/images/hotel/upload/' . $img[1];
			// 	if (file_exists($file)) {
			// 		unlink($file);
			// 	}
			// }
			// if($fullpath2 != $result[0]['logo_url']){
			// 	$logo = explode('logo/',$result[0]['logo_url']);
			// 	$logofile = $_SERVER['DOCUMENT_ROOT'] . '/travel/images/hotel/upload/logo/' . $logo[1];
			// 	if (file_exists($logofile)) {
			// 		unlink($logofile);
			// 	}
			// }
			$this->db->where('id',$id);
			$result = $this->db->update('hotel',$hotel);	
		}else{
			$result = $this->db->insert('hotel', $hotel);	
		}
		if($result){
			return $result;
		}else{
			return false;
		}
	}	
	// delete function
	function delete($id){
		$query1= $this->db->get_where('hotel',array('id'=>$id));
		$result = $query1->result_array();
		if($result){
			$img = explode('upload/',$result[0]['image_url']);
			$logo = explode('logo/',$result[0]['logo_url']);
			$file_logo = $_SERVER['DOCUMENT_ROOT'] . '/travel/images/hotel/upload/logo/' . $logo[1];
			$file = $_SERVER['DOCUMENT_ROOT'] . '/travel/images/hotel/upload/' . $img[1];
			if (file_exists($file)) {
				unlink($file);
			}
			if(file_exists($file_logo)){
				unlink($file_logo);
			}
		}
		$this->db->where('id',$id);
		$query = $this->db->delete('hotel');
		if($query){
			return TRUE;
		}else{
			return FALSE;
		}
	}
}
?>