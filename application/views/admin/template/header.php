<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link type="image/x-icon" href="<?php echo base_url();?>images/logo.jpg" rel="icon">
<title><?php echo $title?></title>
<link rel="stylesheet" href="<?php echo base_url();?>css/dataTables.css">
<link rel="stylesheet" href="<?php echo base_url();?>css/admin/jquery-ui.min.css">
<link rel="stylesheet" href="<?php echo base_url();?>css/admin/common.css">
<link rel="stylesheet" href="<?php echo base_url();?>css/bootstrap-cerulean.css"> 
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/style.css" media="screen" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/navi.css" media="screen" />
<link href="<?php echo base_url();?>css/jquery.tagit.css" rel="stylesheet" type="text/css">
<script src="<?php echo base_url();?>js/jquery.js"></script>
<script src="<?php echo base_url();?>js/jquery-ui.min.js"></script>
<script src="<?php echo base_url();?>js/admin/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>js/tinymce/tinymce.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/ui/jquery.ui.datepicker.js"></script>
<script src="<?php echo base_url();?>js/ajax_function.js"></script>
<script src="<?php echo base_url(); ?>js/tag-it.js" type="text/javascript" charset="utf-8"></script>
<script>
$(document).ready(function () {
    $(".nav li").click(function () {
        var id = $(this).attr("id");
        $('#' + id).addClass("active");
        localStorage.setItem("selectedolditem", id);
    });
    var selectedolditem = localStorage.getItem('selectedolditem');
    if (selectedolditem != null) {
        $('#' + selectedolditem).addClass("active");
    }
});
</script>
</head>
<body>
<div class="wrap">
    <div id="header">
        <div id="top">
            <div class="left">
            </div>
        </div>
        <div id="nav" class="nav">
            <ul>
                <li class="upp" id="list"><a href="#" class="sib_up">List</a>
                   <ul>
                   		<li>&#8250; <a href="<?php echo base_url(); ?>admin/activity/">Activities</a></li>
                        <li>&#8250; <a href="<?php echo base_url(); ?>admin/attraction/">Attraction</a></li>
                        <li>&#8250; <a href="<?php echo base_url(); ?>admin/hotel/">Hotels</a></li>
                        <li>&#8250; <a href="<?php echo base_url(); ?>admin/restaurant/">Restaurants</a></li>
                    </ul>
                </li>
                <li class="upp" id="city"><a href="<?php echo base_url(); ?>admin/city/" class="sib_up">City</a> </li>
                <li class="upp" id="phrase_book"><a href="<?php echo base_url(); ?>admin/phrasebook/" class="sib_up">Phrase Book</a> </li>
                <li class="upp" id="tripplan"><a href="<?php echo base_url(); ?>admin/tripplan/" class="sib_up">Trip Plan</a> </li>
                
                <li class="upp" id="tagkeyword"><a href="<?php echo base_url(); ?>admin/tag/" class="sib_up">Tag keyword</a> </li>
                <li class="upp" id="info"><a href="#" class="sib_up">Essential Information</a>
                	 <ul>
                   		<li>&#8250; <a href="<?php echo base_url(); ?>admin/info/">Item</a></li>
                        <li>&#8250; <a href="<?php echo base_url(); ?>admin/category/">category</a></li>
                    </ul>
                </li>
                <li class="upp" id="user"><a href="#" class="sib_up">User</a>
                	 <ul>
                   		<li class="upp"><a href="<?php echo base_url(); ?>admin/user/">user info</a>  </li>
                        <li class="upp"><a href="<?php echo base_url(); ?>admin/userrequest/">user request</a></li>
                    </ul>
                </li>
                <li class="upp" id="others"><a href="#" class="sib_up">Others</a>
                	 <ul>
                   		<li>&#8250; <a href="<?php echo base_url(); ?>admin/history/">History</a></li>
                        <li>&#8250; <a href="<?php echo base_url(); ?>admin/culture/">Culture</a></li>
                         <li>&#8250; <a href="<?php echo base_url(); ?>admin/tip/">Tips</a></li>
                    </ul>
                </li>                
                <li class="upp" id="admin"><a href="<?php echo base_url(); ?>admin/admin/" class="sib_up">Admin Panel</a></li>
                <li class="upp"><a href="<?php echo base_url(); ?>admin/login/out">logout</a> </li>                
            </ul>
        </div>
        <div class="clear"></div>
    </div>
    <div id="main">
    	<div class="full_w">
