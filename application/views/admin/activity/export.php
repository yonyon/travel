<?php	
header("Content-type: application/csv");
header("Content-Disposition: attachment; filename=".$fileName.'.csv');
$output= '';
$output .= 'id,cityId,city_name,tagId,tag_keyword,name,rating,description,address,longitude,latitude,updated_date,created_date';
$output .="\r\n";
if(is_array($activity) && sizeof($activity) > 0){
	foreach($activity as $key=>$value){
		
		$created = explode('-',$value['created_date']);	
		$updated = explode('-', $value['updated_date']);
		$created_date = $created[2].'/'.$created[1].'/'.$created[0];
		$updated_date = $updated[2].'/'.$updated[1].'/'.$updated[0];
		
		$cityid = $value['city'];
		$city = $this->db->get_where('city',array('id'=>$cityid));
		$result = $city->result_array();
		if($result){
			$cityname = $result[0]['name'];
		}else{
			$cityname = $r['city'];
		}
		
		$tagid = $value['tag'];	
		$tagsplitid = str_replace(",","-",$tagid);	
		$tag = getcolname('tag','keyword',$tagid);
		if($tag){
			$keyword = str_replace(",","-",$tag);
		}else{
			$keyword = $tagsplitid;
		}
		$desc = strip_tags($value['description']);
		$desc = str_replace(",","-",$desc);
		$address = str_replace(",","-",$value['address']);
		$output .= $value['id'].','.$cityid.','.$cityname.','.$tagsplitid.','.$keyword.','.$value['name'].','.$value['rating'].','.$desc.','.$address.','.$value['longitude'].','.$value['latitude'].','.$updated_date.','.$created_date;
		$output .="\r\n";
	}
}
echo $output;
exit;
?>
