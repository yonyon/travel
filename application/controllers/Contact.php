<?php
/**	
 * @file
 *  Program		: contact.php 
 * 	Author		: ETK
 * 	Date		: 07/03/2016
 * 	Abstract	: contact
*/
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Contact extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model("contact_model");
		$this->load->library(array('form_validation'));		 
	}
	function index()
    {
		$data = '';
		$data['msg'] = '';
		if($this->input->post()){
			 $config = Array(       
				'mailtype'  => 'html'
			);
			$this->load->library('email', $config);
			$name = $this->input->post('name');
			$email = $this->input->post('email');
			$phone = $this->input->post('phone');
			$message = $this->input->post('message');
			$message = 'phone : '.$phone.'<br>message : '.$message;
			$this->email->from($email, $name);
			$this->email->to('eithandarkyaw@appvantage.sg'); 
			$this->email->subject('VantageTrip : Contact Message');
			$this->email->message($message);
			if ( ! $this->email->send()){
				//echo 'error';
				$data['msg'] = "Your message can not send.";
			}else{
				$data['msg'] = "Your message has been sent.";
			}
		
		}
		$data['menu'] = 'contact';
		$this->load->view('template/header', $data);
		$this->load->view('contact', $data);
		$this->load->view('template/footer');
    }
}
?>