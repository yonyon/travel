<?php
/**	
 * @file
 *  Program		: event.php 
 * 	Author		: ETK
 * 	Date		: 20/05/2014
 * 	Abstract	: event form, event list
*/
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Event extends MY_Controller {
	public function __construct(){
		parent::__construct();
		# load model 
		$this->load->model('event_model');
		$this->load->helper(array('html', 'url'));
		$this->load->library(array('form_validation'));	
	}
	public function index()
	{
		$this->setup();
	}
	function setup($id=''){ 
		if(!$this->session->userdata('beacon_logged_in')){
			redirect(base_url().'admin/login/');
		}else{
			
			if($this->input->post('cancel'))
			{
				redirect(base_url().'admin/event/eventList/');
			}
			
			$data = $this->event_model->form();
			
			$data['title'] = 'Event';
			$data['id'] = $id;
			$data['error'] = '';
			$adminUser = $this->getSessionUser();
			foreach ($adminUser as $key=>$value)
			{
				$data[$key] = $value;
			}
			if($id > 0){
				$event = $this->event_model->getEventById($id);
				$start = explode('-', $event[0]['event_start_date']);
				$end = explode('-', $event[0]['event_end_date']);
				$data['fCode']['value'] = $event[0]['event_code'];
				$data['fName']['value'] = $event[0]['event_name'];
				$data['fDesc']['value'] = $event[0]['event_description'];
				$data['fAddress']['value'] = $event[0]['event_address'];
				$data['fContactNo']['value'] = $event[0]['event_contact_number'];
				$data['fWebsite']['value'] = $event[0]['event_website'];
				$data['fSdate']['value'] = $start[2].'/'.$start[1].'/'.$start[0];
				$data['fEdate']['value'] = $end[2].'/'.$end[1].'/'.$end[0];
			}
			$this->form_validation->set_rules('code','Event Code','trim|required|xss_clean');
			$this->form_validation->set_rules('name','Event Name','trim|required|xss_clean');
			$this->form_validation->set_rules('address','Address','trim|required|xss_clean');
			$this->form_validation->set_rules('contactNo','Contact No.','trim|required|xss_clean');
			$this->form_validation->set_rules('sDate','Start Date','trim|required|xss_clean');
			$this->form_validation->set_rules('eDate','End Date','trim|required|xss_clean');
			
			if($this->input->post('submit')){
				if($this->form_validation->run()===TRUE){
					$code = $this->input->post('code');
					$code1 = strstr($code, ' ');
					if($code1 == ''){
						$result = $this->event_model->saveEvent($id, $this->input->post());
						if($result)
						{
							redirect(base_url().'admin/event/eventList/');
						}
					}else{
						$data['error'] = 'N0t allow space in "Event Code".';
					}
					
				}
				$data['fCode']['value'] 	= $this->input->post('code');
				$data['fName']['value'] 	= $this->input->post('name');
				$data['fDesc']['value'] 	= $this->input->post('desc');
				$data['fAddress']['value'] 	= $this->input->post('address');
				$data['fContactNo']['value']= $this->input->post('contactNo');
				$data['fWebsite']['value'] 	= $this->input->post('website');
				$data['fSdate']['value'] 	= $this->input->post('sDate');
				$data['fEdate']['value'] 	= $this->input->post('eDate');
			}
			$this->load->view('admin/template/header', $data); 
			$this->load->view('admin/event/index', $data); 
			$this->load->view('admin/template/footer');
		}
	}
	function eventList(){
		if(!$this->session->userdata('beacon_logged_in')){
			redirect(base_url().'admin/login/');
		}else{
			$data['title'] = 'Event List';
			$adminUser = $this->getSessionUser();
			foreach ($adminUser as $key=>$value)
			{
				$data[$key] = $value;
			}
			$this->load->view('admin/template/header', $data);
			$this->load->view('admin/event/eventList');
			$this->load->view('admin/template/footer');
		}
	}
	function ajaxList($page, $type=''){
		if(!$this->session->userdata('beacon_logged_in')){
			redirect(base_url().'admin/login/');
		}else{
			$post = $this->input->post();
			extract($post);
			$event['search'] = false;
			$config = array();
			$config["total_rows"]= $event['total_rows'] = $this->event_model->eventCount();
			$config["per_page"] = $this->itemPerPage;
			$config['full_tag_open'] = '<div class="dataTables_paginate paging_bootstrap pagination"><ul>';
			$config['full_tag_close'] = '</ul></div>';
			$config['first_tag_open'] = '<li>';
			$config['first_tag_close'] = '</li>';
			$config['last_tag_open'] = '<li>';
			$config['last_tag_close'] = '</li>';
			$config['prev_tag_open'] = '<li class="prev disabled">';
			$config['prev_tag_close'] = '</li>';
			$config['next_tag_open'] = '<li class="next">';
			$config['next_tag_close'] = '</li>';
			$config['num_tag_open'] = '<li>';
			$config['num_tag_close'] = '</li>';
			$config['cur_tag_open'] = '<li class="active"><a href="#">';
			$config['cur_tag_close'] = '</a></li>';
			$config['return_fun'] = 'ajaxList';
			$this->load->library("ajaxpagination", $config);
			$event['event'] = $this->event_model->getEvent($config["per_page"], $page, $type);
			$event["links"] = $this->ajaxpagination->create_links();
			$event['page'] = $page;
			if($eventName != ''){ $event['search'] = true; }
			//echo '<pre>';print_r($event);echo '</pre>';
			$this->load->view('admin/event/ajaxList', $event);
		}
	}
	function delete($id){
		if(!$this->session->userdata('beacon_logged_in')){
			redirect(base_url().'admin/login/');
		}else{
			if(!empty($id)){
				$result = $this->event_model->deleteEvent($id);
				if($result == TRUE):
					return TRUE;
				else:
					return FALSE;
				endif;	
			}
		}
	}
	function details($id){
		if(!$this->session->userdata('beacon_logged_in')){
			redirect(base_url().'admin/login/');
		}else{
			if($id > 0){
				$data['event'] = $this->event_model->getEventById($id);
				$data['title'] = 'Event Details';
				$data['id'] = $id;
				$adminUser = $this->getSessionUser();
				foreach ($adminUser as $key=>$value)
				{
					$data[$key] = $value;
				}
				//var_dump($data);
				$this->load->view('admin/template/header', $data); 
				$this->load->view('admin/event/details', $data); 
				$this->load->view('admin/template/footer');	
			}
		}
	}
}
?>