<?php
/**	
 * @file
 *  Program		: user_request_model.php 
 * 	Author		: CMS
 * 	Date		: 20/07/2015
 * 	Abstract	: user_request
*/
class Userrequest_model extends CI_Model{
	function Userrequest_model(){
		parent::__construct();
	}	
	function form(){ // to create form		
		$data['city']    ='city <span class="star_red">*</span> :';
		$data['image']  ='Image <span class="star_red">*</span> :';
		$data['logo']  ='Logo <span class="star_red">*</span> :';
		$data['price']  ='Price <span class="star_red">*</span> :';
		$data['phone']  ='Phone <span class="star_red">*</span> :';
		$data['fax']  ='Fax <span class="star_red">*</span> :';
		$data['email']  ='Email <span class="star_red">*</span> :';
		$data['website']  ='Website <span class="star_red">*</span> :';
		$data['name']		='Name <span class="star_red">*</span>:';
		$data['type']		='Type <span class="star_red">*</span>:';
		$data['address']	='address <span class="star_red">*</span> :';	
		$data['cDate']     ='created Date <span class="star_red">*</span> :';
		$data['selectedCity'] 		= '';		
		$data['fImage']	= array('name'=>'upload', 'id'=>'upload', 'value'=>'');
		$data['fLogo']	= array('name'=>'logo', 'id'=>'logo', 'value'=>'');
		$data['fName']		= array('name'=>'name', 'id'=>'name', 'value'=>'', 'rows'=>'8');
		$data['selectedType'] = '';
		$data['fPrice']		= array('name'=>'price', 'id'=>'price', 'value'=>'', 'rows'=>'8');
		$data['fPhone']		= array('name'=>'phone', 'id'=>'phone', 'value'=>'', 'rows'=>'8');
		$data['fFax']		= array('name'=>'fax', 'id'=>'fax', 'value'=>'', 'rows'=>'8');
		$data['fEmail']		= array('name'=>'email', 'id'=>'email', 'value'=>'', 'rows'=>'8');
		$data['fWebsite']	= array('name'=>'website', 'id'=>'website', 'value'=>'', 'rows'=>'8');		
		$data['fAddress']	= array('name'=>'address', 'id'=>'address', 'value'=>'', 'rows'=>'5');
		$data['fCdate']	= array('name'=>'cDate', 'id'=>'cDate', 'placeholder'=>'dd/mm/yyyy', 'value'=>'', 'class'=>'txtFromDate c_in');
		return $data;
	}
	//$id,$name1,$name2,$fullpath1,$fullpath2
	function save($id,$name1,$name2,$fullpath1,$fullpath2){ // to save user_request data
		$name 		= $this->input->post('name');
		$city		= $this->input->post('city');
		$address 	= $this->input->post('address');		
		$price		= $this->input->post('price');
		$phone 		= $this->input->post('phone');
		$fax 		= $this->input->post('fax');
		$email 		= $this->input->post('email');
		$website 	= $this->input->post('website');
		$type 	= $this->input->post('type');
		$cDate 		= $this->input->post('cDate');
		if(!empty($cDate)){
			$created = explode('/', $cDate);	
			$cDate = $created[2].'-'.$created[1].'-'.$created[0];
		}
		$data = array(
					'name'=>$name, 
					'city'=>$city,
					'address'=>$address,					
					'price'=>$price,
					'phone'=>$phone,
					'fax'=>$fax,
					'email'=>$email,
					'website'=>$website,
					'image'=>$name1,  
					'image_url'=>$fullpath1, 
					'logo'=>$name2, 
					'logo_url'=>$fullpath2, 
					'type'=>$type, 
					'created_date'=>$cDate
				);		
		$this->db->where('id',$id);
		$result = $this->db->update('user_request',$data);	
		if($result){
			return $result;
		}else{
			return false;
		}
	}

	function delete($id){
		$this->db->where('id',$id);
		$query = $this->db->delete('user_request');
		if($query){
			return TRUE;
		}else{
			return FALSE;
		}
	}
	function publish($id){
		$query = $this->db->get_where('user_request',array('id'=>$id));
		$result = $query->result_array();
		
		if($result[0]['status'] == 0){
			
			$image = $result[0]['image']; 
			$newimage = explode('.',$image);				
			$newimage = $newimage[0].'_'.rand().'.'.$newimage[1];	// rename image name
			$image_url = $result[0]['image_url'];
			
			$logo = $result[0]['logo'];
			$newlogo = explode('.',$logo);	
			$newlogo = $newlogo[0].'_'.rand().'.'.$newlogo[1];	// rename logo name
			$logo_url = $result[0]['logo_url'];		
			
			$dir = explode('travel/',$image_url); // for copy image 
			$dir = FCPATH . $dir[1];
			$dirlogo = explode('travel/',$logo_url);
			$dirlogo = FCPATH . $dirlogo[1];
			
			if($result[0]['type'] == 'hotel'){
				$newurl = base_url().'images/hotel/upload/'.$newimage;
				$logourl = base_url().'images/hotel/upload/logo/'.$newlogo;
			}else if($result[0]['type'] == 'restaurant'){
				$newurl = base_url().'images/restaurant/upload/'.$newimage;
				$logourl = base_url().'images/restaurant/upload/logo/'.$newlogo;
			}
			$data = array(
						'name'=>$result[0]['name'], 
						'city'=>$result[0]['city'],
						'address'=>$result[0]['address'],					
						'image'=>$newimage,
						'image_url'=>$newurl,
						'logo'=>$newlogo,
						'logo_url'=>$logourl,
						'price'=>$result[0]['price'],  
						'phone'=>$result[0]['phone'], 
						'fax'=>$result[0]['fax'], 
						'email'=>$result[0]['email'], 
						'website'=>$result[0]['website'],
						'created_date' => date('Y-m-d'),
						'updated_date' => date('Y-m-d')
					);
			if($result[0]['type'] == 'hotel'){			
				$dirNew = FCPATH . 'images/hotel/upload/';
				$dirNewlogo = FCPATH . 'images/hotel/upload/logo/';
				$result = $this->db->insert('hotel', $data);
							
			}else if($result[0]['type'] == 'restaurant'){		
				$dirNew = FCPATH . 'images/restaurant/upload/';	
				$dirNewlogo = FCPATH . 'images/restaurant/upload/logo/';		
				$result = $this->db->insert('restaurant', $data);
			}
			if($result){
				$publishdata = array('status'=>1);
				$this->db->where('id',$id);
				$result = $this->db->update('user_request',$publishdata);
				$copyimages = $this->copyimages($dir,$dirNew,$newimage);			
				$copyimages = $this->copyimages($dirlogo,$dirNewlogo,$newlogo);
				return 'unpublish';
			}else{
				return false;
			}
		}elseif($result[0]['status'] == 1){
			return 'publish';
		}
	}
	
	function copyimages($dir,$dirNew,$name){
		if (is_dir($dirNew)) {			
			if(copy("$dir","$dirNew/$name")){
				return true;
			}else{
				return false;
			}	
		}else{
			return false;
		}		
	}
}
?>