<?php //include 'template/menu.php';?>
<!DOCTYPE html>
<html lang="en">
<head>
	<!--
		Charisma v1.0.0

		Copyright 2012 Muhammad Usman
		Licensed under the Apache License v2.0
		http://www.apache.org/licenses/LICENSE-2.0

		http://usman.it
		http://twitter.com/halalit_usman
	-->
	<meta charset="utf-8">
	<title>Travel  Login</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="Charisma, a fully featured, responsive, HTML5, Bootstrap admin template.">
	<meta name="author" content="Muhammad Usman">

	<!-- The styles -->
	<link id="bs-css" href="<?php echo base_url();?>css/bootstrap-cerulean.css" rel="stylesheet">
	<style type="text/css">
	  body {
		padding-bottom: 40px;
	  }
	  .sidebar-nav {
		padding: 9px 0;
	  }
	  span.star_red{
		 color: #FF0000; 
	  }
	</style>
   
	<link href="<?php echo base_url();?>css/charisma-app.css" rel="stylesheet">
    <link href="<?php echo base_url();?>css/charisma-app.css" rel="stylesheet">
</head>

<body>
<div class="banner-with-text">
<div class="header-yangon">
</div>
</div>
<div class="container">
<div class="banner-bottom" id="event">	
    <div class="span12 center login-header">
        <h2>Welcome to Travel Myanmar</h2>
    </div><!--/span-->    
    
    <div class="row-fluid">
        <div class="well span5 center login-box">
            <div class="alert alert-info">
                <?php if(isset($error)) echo '<span class="star_red">'.$error.'</span>'; else{ ?>Please login with your Email and Password.<?php } ?>
            </div>
            <?php 
                $attributes = array('class'=>'form-horizontal','method'=>'post');
                echo form_open_multipart(base_url().'login/save', $attributes);
                ?>  
                <fieldset>
                    <div class="input-prepend" title="Username" data-rel="tooltip">
                        <span class="add-on"><i class="icon-user"></i></span>
                        <?php 
                                $email = array('class'=>'input-large span10','name'=>'email','id'=>'email','autofocus'=>'autofocus');
                                echo form_input($email); 
                            ?>
                    </div>
                    <div class="clearfix"></div>

                    <div class="input-prepend" title="Password" data-rel="tooltip">
                        <span class="add-on"><i class="icon-lock"></i></span>
                         <?php 
                                $password = array('class'=>'input-large span10','name'=>'password','id'=>'password','autofocus'=>'autofocus');
                                echo form_password($password); 
                            ?>
                    </div>
                    <div class="clearfix"></div>

                    <!--<div class="input-prepend">
                    <label class="remember" for="remember"><input type="checkbox" id="remember" />Remember me</label>
                    </div>-->
                    <div class="clearfix"></div>

                    <p class="center span5">
                    <button type="submit" class="btn btn-primary" name="submit" value="true">Login</button>
                    </p>
                </fieldset>
            <?php echo form_close(); ?>
            <div class="input-prepend" title="Password" data-rel="tooltip">
            <a href="<?php echo base_url() ?>forgot_password">Forgot password?</a><br/>
            Don't have account? Register <a href="<?php echo base_url() ?>register">here</a>
                        
            </div>
        </div><!--/span-->
    </div><!--/row-->		
</div>