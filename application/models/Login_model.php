<?php
/**	
 * @file
 *  Program		: user_model.php 
 * 	Author		: CMS
 * 	Date		: 23/07/2015
 * 	Abstract	: user
*/
class Login_model extends CI_Model{
	function Login_model(){
		parent::__construct();
		$this->load->library('phpass');
	}
	function getById($id){
		$query = $this->db->get_where('user',array('id'=>$id));
		if($query->num_rows() > 0):
			return $query->result_array();
		endif;
	}
	function login($id=''){
		$email 	= $this->input->post('email');
		$password = $this->input->post('password');	
		$query = $this->db->get_where('user',array('email'=>$email));
		$result = $query->result();
		if($query->num_rows() > 0){
			$hashed = $result[0]->password;			
			if ($this->phpass->check($password, $hashed)){
				return  $result;
			}else{
			 echo "this";	
			}
		}
	}
}
?>