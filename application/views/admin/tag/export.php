<?php	
header("Content-type: application/csv");
header("Content-Disposition: attachment; filename=".$fileName.'.csv');
$output= '';
$output .= 'id,keyword,updated_date,created_date';
$output .="\r\n";
if(is_array($tag) && sizeof($tag) > 0){
	foreach($tag as $key=>$value){	
		$created_date = redatetime($value['created_date']);
		$updated_date = redatetime($value['updated_date']);
		$output .= $value['id'].','.$value['keyword'].','.$updated_date.','.$created_date;
		$output .="\r\n";
	}
}
echo $output;
exit;
?>
