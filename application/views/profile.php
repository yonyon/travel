<style>
div.container div#loginPage {
	padding:0em 2em 0em;
}
.list-group a{
	text-align:left;
}
a.selected{
	color:#000000;
}
div.banner-bottom div.header-desc-yangon{
	margin-top:2%;
}
</style>
<?php
	//include 'description.php';
	
	
?>
<!--<div class="container">-->
<div class="banner-bottom" align="center" id="loginPage">
    <div class="sidebar-offcanvas" id="sidebar" role="navigation">
        <div class="list-group">
            <a href="<?php echo base_url(); ?>myaccount/show/profile" class="list-group-item selected" >profile</a>
            <a href="<?php echo base_url(); ?>myaccount/show/changepassword" class="list-group-item ">change password</a>
            <a href="<?php echo base_url(); ?>myaccount/show/requestform" class="list-group-item">request listing</a> 
            <a href="<?php echo base_url(); ?>login/out" class="list-group-item">logout</a>      
        </div>        
    </div>
    <div class="header-desc-yangon" style="width:60%;" id="div-href">         
        
         <div class="well changepass" id="div-change-profile">               
        <div class="alert alert-info profiletitle">
        	 <?php if(isset($error)){
				  echo '<span class=text-danger>'.$error.'</span>' ; 
				  }if(isset($msg)){
				  echo '<span class=text-success>'.$msg.'</span>' ; 
				  } else{ ?>Profile setting.<?php } ?>
        </div>
        <?php 
			$attributes = array('class'=>'form-horizontal','method'=>'post');
            echo form_open_multipart(base_url().'myaccount/profile', $attributes);
        ?>
        <table class="tbl-register" style="text-align:left">       
            <tr>
                <td>User name</td>
                <td>
					 <?php
						$username = array('class'=>'input-large','name'=>'name','id'=>'name','value'=>$userinfo['user_name'],'autofocus'=>'autofocus');
						echo form_input($username); 
						echo form_error('name', '<div class="error">', '</div>');
                    ?>
                </td>
            </tr>    
            <tr>
                <td>Email</td>
                <td>
					<?php 
						$email = array('class'=>'input-large','name'=>'email','id'=>'email','value'=>$userinfo['user_email'],'autofocus'=>'autofocus');
						echo form_input($email);
						echo form_error('email', '<div class="error">', '</div>'); 
                    ?>
                </td>
            </tr>    
           <tr>
                <td> Country</td>
                <td>
                <?php 
                    $country = array('class'=>'input-large','name'=>'country','id'=>'country');
                    $options = array(
                    'Myanmar'=>'Myanmar',
                    'Singapore'=>'Singapore',
                    'China'=>'China',
                    'America'=>'America',
                    'French'=>'French',
                    'Europe'=>'Europe',
                    'Korea'=>'Korea',
                    'Thailand'=>'Thailand');
					$selectedcountry = $userinfo['user_country'];
                    echo form_dropdown($country,$options,$selectedcountry); 
                ?>
                </td>
                </tr>   
            <tr>
                <td colspan="2">
                    <button type="submit" class="btn btn-primary btnres" name="submit" value="true">Continue</button>
                </td>
            </tr>  
        </table>
        <?php echo form_close(); ?>
        </div><!--/span-->  
        
    		
    </div>
	<div class="clear"></div>     
</div>
