<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('datetime'))
{
    function datetime($ludate){
		if(!empty($ludate)){
			if (strpos($ludate,'/') !== false) {
				$ludate = explode('/',$ludate);	
				$ludate = $ludate[2].'-'.$ludate[1].'-'.$ludate[0];				
				if(preg_match('/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/', $ludate)){
					$date = $ludate;
				}else{
					$date = '';
				}
			}else{
				$date = '';
			}
		return $date;				
		}
	}
}

if ( ! function_exists('redatetime'))
{
    function redatetime($date){
		if(!empty($date)){
		$date = explode('-',$date);	
		$date = $date[2].'/'.$date[1].'/'.$date[0];
		return $date;				
		}
	}
}


?>