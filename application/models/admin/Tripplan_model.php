<?php
/**	
 * @file
 *  Program		: tripplan_model.php 
 * 	Author		: CMS
 * 	Date		: 22/07/2015
 * 	Abstract	: tripplan
*/
class Tripplan_model extends CI_Model{
	function tripplan_model(){
		parent::__construct();
	}
	//create form
	function form(){		
		$data['city']    ='City <span class="star_red">*</span> :';	
		$data['image']  ='Image <span class="star_red">*</span> :';
		$data['desc']    ='Description <span class="star_red">*</span> :';	
		$data['name']    ='name <span class="star_red">*</span> :';			
		$data['cDate']     ='created Date <span class="star_red">*</span> :';
		$data['uDate']     ='updated Date <span class="star_red">*</span> :';	
		$data['fDesc']		= array('name'=>'desc', 'id'=>'desc', 'value'=>'', 'rows'=>'8');
		$data['fName']		= array('name'=>'name', 'id'=>'name', 'value'=>'', 'rows'=>'8');	
		$data['fImage']	= array('name'=>'upload', 'id'=>'upload', 'value'=>'');	
		$data['fCdate']	= array('name'=>'cDate', 'id'=>'cDate', 'placeholder'=>'dd/mm/yyyy', 'value'=>'', 'class'=>'txtFromDate c_in');
		$data['fUdate']	= array('name'=>'uDate', 'id'=>'uDate', 'placeholder'=>'dd/mm/yyyy', 'value'=>'', 'class'=>'txtToDate c_out');		
		return $data;
	}
	// to save tripplan data
	function save($id,$fullpath){
		$city 	= $this->input->post('city');
		$desc 	= $this->input->post('desc');
		$name 	= $this->input->post('name');
		$cDate 		= $this->input->post('cDate');
		$uDate 		= $this->input->post('uDate');
		if(!empty($cDate)){
			$created = explode('/', $cDate);	
			$cDate = $created[2].'-'.$created[1].'-'.$created[0];
		}
		if(!empty($uDate)){
			$updated = explode('/', $uDate);	
			$uDate = $updated[2].'-'.$updated[1].'-'.$updated[0];
		}
		$data = array(
					'name'=>$name,
					'description'=>$desc,
					'city'=>$city,
					'image_url'=>$fullpath, 
					'created_date'=>$cDate,
					'updated_date'=>$uDate
				);
		if(!empty($id) && $id > 0){
			
			
			$query1= $this->db->get_where('trip_plan',array('id'=>$id));
			$result = $query1->result_array();			
			if($fullpath != $result[0]['image_url']){
				$img = explode('upload/',$result[0]['image_url']);
				$file = $_SERVER['DOCUMENT_ROOT'] . '/travel/images/tripplan/upload/' . $img[1];
				if (file_exists($file)) {
					unlink($file);
				}
			}
			$this->db->where('id',$id);
			$result = $this->db->update('trip_plan',$data);
		}else{
			$result = $this->db->insert('trip_plan', $data);	
		}
		if($result){
			return $result;
		}else{
			return false;
		}
	}
	// delete function
	function delete($id){
		$query_tripplan = $this->db->get_where('trip_plan',array('id'=>$id));
		$result = $query_tripplan->result_array();
		if($result){
			$img = explode('upload/',$result[0]['image_url']);
			$file = $_SERVER['DOCUMENT_ROOT'] .'/travel/images/tripplan/upload/' . $img[1];
			if (file_exists($file)) {
				unlink($file);
			}
		}
		$this->db->where('id',$id);
		$query = $this->db->delete('trip_plan');
		if($query){
			return TRUE;
		}else{
			return FALSE;
		}
	}
}
?>