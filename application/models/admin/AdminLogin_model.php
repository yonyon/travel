<?php
/**	
 * @file
 *  Program		: adminLogin_model.php 
 * 	Author		: CMS
 * 	Date		: 23/7/2015
 * 	Abstract	: admin user
*/
class Adminlogin_model extends CI_Model{
	function adminlogin_model(){
		parent::__construct();
		$this->load->library('phpass');
	}
	function login(){
		$name = $this->input->post('username');
		$password = $this->input->post('password');	
		$query = $this->db->get_where('admin',array('username'=>$name));
		$result = $query->result();
		
		if($query->num_rows() > 0){
			$hashed = $result[0]->password;			
			if ($this->phpass->check($password, $hashed)){
				return  $result;
			}else{
				return  false;
			}
		}else{
			return false;
		}
	}
}
?>