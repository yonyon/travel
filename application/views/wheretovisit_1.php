<?php include 'template/menu.php';?>
<div class="banner-with-text">
<div class="header-yangon">
</div>
</div>
<div class="container">
<?php 
$query = $this->db->get_where('city',array('id'=>$id));
$result =  $query->result_array();
$img = $result[0]['image_url'];		
$desc = $result[0]['description'];
?>
<div class="banner-bottom" id="event">
    <div class="sidebar-offcanvas" id="sidebar" role="navigation">
        <div class="list-group">
            <a href="<?php echo base_url(); ?>wheretostay/show/<?php echo $id;?>" class="list-group-item">WHERE TO STAY</a>
            <a href="<?php echo base_url(); ?>wheretovisit/show/<?php echo $id;?>" class="list-group-item ">WHERE TO VISIT</a>
            <a href="<?php echo base_url(); ?>wheretoeat/show/<?php echo $id;?>" class="list-group-item">WHERE TO EAT</a>
            <a href="<?php echo base_url(); ?>whattodo/show/<?php echo $id;?>" class="list-group-item">WHAT TO DO</a>
        </div>
    </div>
	<?php
	if( $detail == 'detail' ){
		if($result_attractions != false){// check data is exist or not
			foreach($result_attractions as $result_attraction)
			{                     
				$keyword = getcolname('tag','keyword',$result_attraction['tag']); 
				$desc = $result_attraction['description'];
				$url = $result_attraction['image_url'];
				echo ' <div class="col-md-0 banner-bottom-grid">';
				echo '<div class="tag_detail">
						<div style="text-align: justify;">
							<img width="300px;" style="margin-left: 10px; float: left;" alt="bagan-panoramicview" src="' . $url . '">						
						</div><div style="margin-top:170px;">';
						$keyword = explode(',',$result_attraction['tag']);						
						foreach ($keyword as $value){
							$query_tag = $this->db->get_where('tag',array('id'=>$value));
							$result_tag =  $query_tag->result_array();
							if($result_tag){							
								echo '<span class="keyword"><a href='.base_url().'wheretovisit/showpage/'.$value.'/'.$id.' style="text-decoration:none;">'.$result_tag[0]['keyword'].' </a></span>';
							}
						}
				echo '</div></div>';
				echo '<div class="desc_detail"><span style="font-size: 18pt; color: #ec534d;">' . ucwords(strtolower($result_attraction['name'])) . '</span>
					  <p style=" text-align:justify">' . $desc . '</p>';
				?>
                <!--table class="tbl-register" style="text-align:left; margin-bottom:12px;" >   
                    <tr>
                        <td style="padding-right:35px;"><span class="glyphicon glyphicon-home"></span>Address</td>
                        <td> <?php echo $result_attraction['address'];?> </td>
                    </tr>
                    <tr>
                        <td><span class="glyphicon glyphicon-envelope"></span>How to get there</td>
                        <td><?php echo $result_attraction['how_to_get_there'];?>  </td>
                    </tr>
                </table>-->
                
               <div id="googleMap" style="width:500px;height:380px;margin-bottom:15px;"></div></div>
				<?php				
				echo '</div>';	 //end of col-md-0 banner-bottom-grid;						
			}
			    //echo '<div class="div-pagination">'.$links.'</div>'; // showing pagination links
		}
	}else{
		if($result_attractions != false){ // check data is exist or not
			foreach($result_attractions as $result_attraction)
			{  
				$keyword = getcolname('tag','keyword',$result_attraction['tag']);   
				$attractionid = $result_attraction['id'];     	            
				$desc = substr($result_attraction['description'] , 0, 295)."....";
				$url = $result_attraction['image_url'];
				$detailurl =  base_url(). 'wheretovisit/detail/'.$id."/".$attractionid."/";
				echo ' <div class="col-md-0 banner-bottom-grid">';
				echo '<p style="text-align: justify;">
						<img width="300px;" style="margin-left: 10px; float: left;" alt="bagan-panoramicview" src="' . $url . '">
						<span style="font-size: 18pt; color: #ec534d;">' . ucwords(strtolower($result_attraction['name'])) .'</span>
					</p>';
				echo '<p style="text-align: justify;">' . $desc . '</p>';
				echo '<p style="text-align: justify;"></p>
					<p style="text-align: justify;"></p>
					<p style="text-align: justify;"></p>';	
				echo '<p>
						<a href="' .$detailurl. '" class="desmore">More </a>';
						$keyword = explode(',',$result_attraction['tag']);					
						foreach ($keyword as $value){
						$query_tag = $this->db->get_where('tag',array('id'=>$value));
						$result_tag =  $query_tag->result_array();
						if($result_tag){
							echo '<span style="border:1px solid black;border-radius:3px;padding:3px;margin-right:6px;" class="keyword"><a href='.base_url().'wheretovisit/showpage/'.$value.'/'.$id.' style="text-decoration:none;">'.$result_tag[0]['keyword'].' </a></span>';
						}
						}
				echo '</p></div>';								
			}
			echo '<div class="div-pagination"><ul class="pagination"><li>'.$links.'</li></ul></div></div>'; // showing pagination links
		}
	}
    ?>
</div><!-- end of banner-bottom-->
<style>
.header-yangon {
background: url(<?php echo $img; ?>);
  position:relative;
  height:230px;
  background-repeat:no-repeat;
  background-size:cover;
}
</style>
<script>
var myCenter=new google.maps.LatLng(<?php echo $result_attraction['latitude']; ?>,<?php echo $result_attraction['longitude']; ?>);

function initialize()
{
var mapProp = {
  center:myCenter,
  zoom:15,
  mapTypeId:google.maps.MapTypeId.ROADMAP
  };

var map=new google.maps.Map(document.getElementById("googleMap"),mapProp);

var marker=new google.maps.Marker({
  position:myCenter,
  });

marker.setMap(map);
}
google.maps.event.addDomListener(window, 'load', initialize);
</script>