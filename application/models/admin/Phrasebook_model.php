<?php
/**	
 * @file
 *  Program		: phrasebook_model.php 
 * 	Author		: CMS
 * 	Date		: 20/07/2015
 * 	Abstract	: phrasebook
*/
class Phrasebook_model extends CI_Model{
	function phrasebook_model(){
		parent::__construct();
	}
	//create form
	function form(){
		$data['name_mm']  ='Name MM <span class="star_red">*</span> :';
		$data['name_en']	='Name En <span class="star_red">*</span> :';
		$data['phonetic']   ='Phonetic <span class="star_red">*</span>:';
		$data['audio']   ='Audio <span class="star_red">*</span>:';
		$data['desc']   ='Description <span class="star_red">*</span>:';
		$data['cDate'] ='created Date <span class="star_red">*</span> :';
		$data['uDate'] ='updated Date <span class="star_red">*</span> :';	
		$data['fName_mm']		= array('name'=>'namemm', 'id'=>'namemm', 'value'=>'', 'rows'=>'8');
		$data['fName_en']= array('name'=>'nameen', 'id'=>'nameen', 'value'=>'', 'rows'=>'8');
		$data['fAudio']	= array('name'=>'upload', 'id'=>'upload', 'value'=>'');
		$data['fDesc']	= array('name'=>'desc', 'id'=>'desc', 'value'=>'');
		$data['fPhontic']= array('name'=>'phonetic', 'id'=>'phonetic', 'value'=>'');
		$data['fCdate']	= array('name'=>'cDate', 'id'=>'cDate', 'placeholder'=>'dd/mm/yyyy', 'value'=>'', 'class'=>'txtFromDate c_in');
		$data['fUdate']	= array('name'=>'uDate', 'id'=>'uDate', 'placeholder'=>'dd/mm/yyyy', 'value'=>'', 'class'=>'txtToDate c_out');
		return $data;
	}
	// save phrasebook data
	function save($id,$filename,$realpath){
		$name_mm 		= $this->input->post('namemm');
		$name_en 		= $this->input->post('nameen');
		$phonetic 		= $this->input->post('phonetic');
		$desc 		= $this->input->post('desc');
		$cDate 		= $this->input->post('cDate');
		$uDate 		= $this->input->post('uDate');
		if(!empty($cDate)){
			$created = explode('/', $cDate);	
			$cDate = $created[2].'-'.$created[1].'-'.$created[0];
		}
		if(!empty($uDate)){
			$updated = explode('/', $uDate);	
			$uDate = $updated[2].'-'.$updated[1].'-'.$updated[0];
		}
		$data = array(
					'name_mm'=>$name_mm, 
					'name'=>$name_en, 
					'description'=>$desc,
					'filename'=>$filename,
					'phonetic'=>$phonetic,
					'audio'=>$realpath,
					'created_date'=>$cDate,
					'updated_date'=>$uDate
				);
		
		if(!empty($id) && $id > 0){
			$query1= $this->db->get_where('phrase_book',array('id'=>$id));
			$result = $query1->result_array();			
			if($realpath != $result[0]['audio']){
				$img = explode('upload/',$result[0]['audio']);
				$file = $_SERVER['DOCUMENT_ROOT'] . '/travel/images/phrasebook/upload/' . $img[1];
				if (file_exists($file)) {
					unlink($file);
				}
			}
			$this->db->where('id',$id);
			$result = $this->db->update('phrase_book',$data);	
		}else{
			$result = $this->db->insert('phrase_book', $data);	
		}
		if($result){
			return $result;
		}else{
			return false;
		}
	}
	// delete function
	function delete($id){
		$query1= $this->db->get_where('phrase_book',array('id'=>$id));
		$result = $query1->result_array();
		if($result){
			$audio = explode('upload/',$result[0]['audio']);
			$file = $_SERVER['DOCUMENT_ROOT'] .'/travel/images/phrasebook/upload/' . $audio[1];
			if (file_exists($file)) {
			unlink($file);
			}
		}
		$this->db->where('id',$id);
		$query = $this->db->delete('phrase_book');
		if($query){
			return TRUE;
		}else{
			return FALSE;
		}
	}
}
?>