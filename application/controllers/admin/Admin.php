<?php
/**	
 * @file
 *  Program		: admin.php 
 * 	Author		: CMS
 * 	Date		: 17/7/2015
 * 	Abstract	: Admin 
*/
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Admin extends CI_Controller {
	public function __construct(){
		parent::__construct();
		# load model
		$this->load->model('admin/admin_model');
		$this->load->library(array('form_validation'));	
		//$this->load->library('phpass');
	}
	function index()
    {
		if(!$this->session->userdata('admin_logged_in')){
			redirect(base_url().'admin/login/');
		}else{
			$data['title'] = 'Admin List';
			$this->load->view('admin/template/header',$data);
			$this->load->view('admin/admin/adminlist');
			$this->load->view('admin/template/footer');
		}
    }
	function getdatatable()
    {   
		if(!$this->session->userdata('admin_logged_in')){
			redirect(base_url().'admin/login/');
		}else{     
			$aColumns = array('id', 'firstname','lastname', 'email','username','type','registertime');        
			// DB table to use
			$sTable = 'admin';
				
			$iDisplayStart = $this->input->get_post('iDisplayStart', true);
			$iDisplayLength = $this->input->get_post('iDisplayLength', true);
			$iSortCol_0 = $this->input->get_post('iSortCol_0', true);
			$iSortingCols = $this->input->get_post('iSortingCols', true);
			$sSearch = $this->input->get_post('sSearch', true);
			$sEcho = $this->input->get_post('sEcho', true);
		
			// Paging
			if(isset($iDisplayStart) && $iDisplayLength != '-1')
			{
				$this->db->limit($this->db->escape_str($iDisplayLength), $this->db->escape_str($iDisplayStart));
			}        
			// Ordering
			if(isset($iSortCol_0))
			{
				for($i=0; $i<intval($iSortingCols); $i++)
				{
					$iSortCol = $this->input->get_post('iSortCol_'.$i, true);
					$bSortable = $this->input->get_post('bSortable_'.intval($iSortCol), true);
					$sSortDir = $this->input->get_post('sSortDir_'.$i, true);
		
					if($bSortable == 'true')
					{
						$this->db->order_by($aColumns[intval($this->db->escape_str($iSortCol))], $this->db->escape_str($sSortDir));
					}
				}
			}
			
		   //Filtering
			if(isset($sSearch) && !empty($sSearch))
			{
				for($i=0; $i<count($aColumns); $i++)
				{
					$bSearchable = $this->input->get_post('bSearchable_'.$i, true);
					
					// Individual column filtering
					if(isset($bSearchable) && $bSearchable == 'true')
					{
						$this->db->or_like($aColumns[$i], $this->db->escape_like_str($sSearch));
					}
				}
			}
			
			// Select Data
			$this->db->select('SQL_CALC_FOUND_ROWS '.str_replace(' , ', ' ', implode(', ', $aColumns)), false);
			$rResult = $this->db->get($sTable);
		
			// Data set length after filtering
			$this->db->select('FOUND_ROWS() AS found_rows');
			$iFilteredTotal = $this->db->get()->row()->found_rows;
		
			// Total data set length
			$iTotal = $this->db->count_all($sTable);
		
			// Output
			$output = array(
				'sEcho' => intval($sEcho),
				'iTotalRecords' => $iTotal,
				'iTotalDisplayRecords' => $iFilteredTotal,
				'aaData' => array()
			);     
			foreach ($rResult->result_array()  as $r) {
				$data = array();
				$id = $r['id'];
				$baseurl =  base_url();	
				$delUrl =  $baseurl.'admin/admin/delete/';
				$idurl = $id.'-'.$delUrl;
				$rr['delete'] = "<a id='" . $idurl. "' class='del' onclick='del(this.id)'> Delete </a>";	
				$data[] =$r['id'];
				$data[] =$r['firstname'];
				$data[] =$r['lastname'];
				$data[] =$r['email'];
				$data[] =$r['username'];
				$data[] =$r['type'];
				$data[] =$r['registertime'];			
				$data[] =  anchor('admin/admin/add/' . $r['id'], 'Edit');
				$data[] =  $rr['delete'];
				$output['aaData'][] = $data;
			}
			echo json_encode($output);
		}
    }
	// end server side processing
	// to add/edit admin data
	function add($id=''){
		if(!$this->session->userdata('admin_logged_in')){
			redirect(base_url().'admin/login/');
		}else{	 
			if($this->input->post('cancel'))
			{
				redirect(base_url().'admin/admin/');
			}
			$data = $this->admin_model->form();
			$data['allType']['administrator'] = 'Administrator';
			$data['allType']['editor'] = 'Editor';
			$data['selectedType'] = '';
			$data['title'] = 'Admin Panel';
			$data['id'] = $id;
			$data['error'] = '';
	
			if($id > 0){
				$admin = gettable('admin','id',$id);
				if($admin){
					$data['fName']['value'] 	= $admin[0]['firstname'];
					$data['fLname']['value'] 	= $admin[0]['lastname'];
					$data['fEmail']['value'] 	= $admin[0]['email'];
					$data['fUserName']['value'] = $admin[0]['username'];
					$data['selectedType'] 		= $admin[0]['type'];
				}else{
					redirect(base_url().'admin/admin/');
				}
			}
			$this->form_validation->set_rules('name','First Name','trim|required|xss_clean');
			$this->form_validation->set_rules('lName','Last Name','trim|required|xss_clean');
			$this->form_validation->set_rules('email','Email','trim|required|xss_clean|valid_email');
			$this->form_validation->set_rules('userName','User Name','trim|required|xss_clean');
			$this->form_validation->set_rules('pass','Password','trim|required|xss_clean');
			$this->form_validation->set_rules('conPass','Confirm Password','trim|required|xss_clean|matches[pass]');
			if($this->input->post('submit')){
				if($this->form_validation->run()===TRUE){
					$pass = $this->input->post('pass');
					$conPass = $this->input->post('conPass');					
					$result = $this->admin_model->save($id);
					if($result)
					{				
						redirect(base_url().'admin/admin/');
					}					
				}
				$data['fName']['value'] 	= $this->input->post('name');
				$data['fLname']['value'] 	= $this->input->post('lName');
				$data['fEmail']['value'] 	= $this->input->post('email');
				$data['fUserName']['value'] 	= $this->input->post('userName');
			}
			$this->load->view('admin/template/header', $data); 
			$this->load->view('admin/admin/index', $data); 
			$this->load->view('admin/template/footer');
		}
	}
	function delete(){
		if(!$this->session->userdata('admin_logged_in')){
			redirect(base_url().'admin/login/');
		}else{
			$id = $_REQUEST['id'];
			$result = $this->admin_model->delete($id);
			echo json_encode(array('status'=>$result));	
		}
	}

}
?>