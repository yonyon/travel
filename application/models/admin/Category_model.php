<?php
/**	
 * @file
 *  Program		: category_model.php 
 * 	Author		: CMS
 * 	Date		: 20/07/2015
 * 	Abstract	: category
*/
class Category_model extends CI_Model{
	function category_model(){
		parent::__construct();
	}

	// create form
	function form(){		
		$data['name']	='Name <span class="star_red">*</span>:';
		$data['code']	='Code <span class="star_red">*</span>:';
		$data['mapicon']='Icon <span class="star_red">*</span>:';
		$data['desc']	='Description <span class="star_red">*</span> :';
		$data['cDate']  ='created Date <span class="star_red">*</span> :';
		$data['uDate']  ='updated Date <span class="star_red">*</span> :';
		$data['fName']	= array('name'=>'name', 'id'=>'name', 'value'=>'', 'rows'=>'8');
		$data['fCode']  = array('name'=>'code', 'id'=>'code', 'value'=>'', 'rows'=>'8');
		$data['fDesc']  = array('name'=>'desc', 'id'=>'desc', 'value'=>'', 'rows'=>'8');
		$data['fCdate']	= array('name'=>'cDate', 'id'=>'cDate', 'placeholder'=>'dd/mm/yyyy', 'value'=>'', 'class'=>'txtFromDate c_in');
		$data['fUdate']	= array('name'=>'uDate', 'id'=>'uDate', 'placeholder'=>'dd/mm/yyyy', 'value'=>'', 'class'=>'txtToDate c_out');
		return $data;
	}
	
	// to save category data
	function save($id){
		$name 		= $this->input->post('name');
		$mapicon 	= $this->input->post('mapicon'); 
		$code 		= $this->input->post('code');
		$desc 		= $this->input->post('desc');
		$cDate 		= $this->input->post('cDate');
		$uDate 		= $this->input->post('uDate');
		if(!empty($cDate)){
			$created = explode('/', $cDate);	
			$cDate = $created[2].'-'.$created[1].'-'.$created[0];
		}
		if(!empty($uDate)){
			$updated = explode('/', $uDate);	
			$uDate = $updated[2].'-'.$updated[1].'-'.$updated[0];
		}
		$data = array(
					'name'=>$name, 
					'code'=>$code,
					'map_icon_id'=>$mapicon,
					'description'=>$desc,
					'created_date'=>$cDate,
					'updated_date'=>$uDate
				);
		if(!empty($id) && $id > 0){
			$this->db->where('id',$id);
			$result = $this->db->update('category',$data);	
		}else{
			$result = $this->db->insert('category', $data);	
		}
		if($result){
			return $result;
		}else{
			return false;
		}
	}
	// delete function
	function delete($id){
		$del = true;
		$info_query = $this->db->get_where('information',array('category'=>$id));
		if($info_query->num_rows() > 0){
			$del = false;
		}
		$tag_query = $this->db->get_where('tag',array('category'=>$id));
		if($tag_query->num_rows() > 0){
			$del = false;
		}
		if($del){
			$this->db->where('id',$id);
			$query = $this->db->delete('category');
			if($query){
				return TRUE;
			}else{
				return FALSE;
			}
		}else{
			return $del;
		}
	}
}
?>