
<!--<div class="container">-->
<style>
div.container div#loginPage {
	padding:0em 2em 0em;
}
.list-group a{
	text-align:left;
}
a.selected{
	color:#000000;
}
div.banner-bottom div.header-desc-yangon{
	margin-top:2%;
}
</style>
<div class="banner-bottom" align="center" id="loginPage">
    <div class="sidebar-offcanvas" id="sidebar" role="navigation">
        <div class="list-group">
            <a href="<?php echo base_url(); ?>myaccount/show/profile" class="list-group-item" >profile</a>
            <a href="<?php echo base_url(); ?>myaccount/show/changepassword" class="list-group-item selected">change password</a>
            <a href="<?php echo base_url(); ?>myaccount/show/requestform" class="list-group-item">request listing</a> 
            <a href="<?php echo base_url(); ?>login/out" class="list-group-item">logout</a> 
        </div>        
    </div>
    <div class="header-desc-yangon" style="width:60%;" id="div-href">         
      <div class="well changepass" id="div-change-password">               
        <div class="alert alert-info profiletitle">
        	 <?php if(isset($error)){
				  	echo '<span class = error>'.$error.'</span>' ; 
				  }else if(isset($msg)){
				  	echo '<span class=text-success>'.$msg.'</span>' ; 
				  }  else{ ?>change your password.<?php } ?>
        </div>
        <?php 
			$attributes = array('class'=>'form-horizontal','method'=>'post');
            echo form_open_multipart(base_url().'myaccount/changepassword', $attributes);
        ?>
        <table class="tbl-register" style="text-align:left"> 
         	<tr>
                <td>current Password</td>
                <td>
					<?php 
                    $curpassword = array('class'=>'input-large','name'=>'curpassword','id'=>'curpassword','autofocus'=>'autofocus');
                    echo form_password($curpassword); 
					echo form_error('curpassword', '<div class="error">', '</div>');
                    ?>
                </td>
            </tr>         
            <tr>
                <td>Password</td>
                <td>
					<?php 
                    $password = array('class'=>'input-large','name'=>'password','id'=>'password','autofocus'=>'autofocus');
                    echo form_password($password); 
					echo form_error('password', '<div class="error">', '</div>');
                    ?>
                </td>
            </tr>    
            <tr>
                <td>Confrim Password</td>
                <td>
					<?php 
                    $conpassword = array('class'=>'input-large','name'=>'conpassword','id'=>'conpassword','autofocus'=>'autofocus');
                    echo form_password($conpassword); 
					echo form_error('conpassword', '<div class="error">', '</div>');
                    ?>
                </td>
            </tr>    
           
            <tr>
                <td colspan="2">
                    <button type="submit" class="btn btn-primary btnres" name="submit" value="true">Continue</button>
                </td>
            </tr>  
        </table>
        <?php echo form_close(); ?>
        </div><!--/span-->  
        
    		
    </div>
	<div class="clear"></div>     
</div>
