<script type="text/javascript" charset="utf-8">
	$(document).ready(function() {
		var oTable;
		oTable = $('#DataTables').dataTable( {			
			"bJQueryUI": true,
			"sPaginationType": "full_numbers",
			"bProcessing": true,
			"bServerSide": true,
			"sAjaxSource": '<?php echo base_url(); ?>/admin/userrequest/getdatatable',
			"aoColumns": [
			null, null, null,null,null,null,null,
			{ "sClass": "center", "bSortable": false },
			{ "sClass": "center", "bSortable": false },
			{ "sClass": "center", "bSortable": false },
			{ "sClass": "center", "bSortable": false }
			],
			"aaSorting": [[0, 'desc']],
			"iDisplayLength":10,			
		});
	});
</script>
<style>
	.paging_full_numbers {
		width: 280px !important;
	}
</style>
<div>
    <?php
	$flash = $this->session->flashdata('data');
	if(isset($flash['msg'])){
		echo '<div style="color:green;font-size:13px;padding:10px;">' .$flash['msg']. '</div>';
	}else{
		echo '<div style="color:red;font-size:13px;padding:10px;">' .$flash['error']. '</div>';
	}	
?>
</div>
<div style="width:900px; text-align:left; margin-left:5px;">
<table cellspacing="0" cellpadding="0" border="0" class="display" id="DataTables" bgcolor="#ffffff">
  <thead>
    <tr>
    	<th> ID </th>
        <th> Type </th> 
        <th> City </th>        
        <th> name</th>
        <th> Image</th>
        <th> Logo</th>
        <th> Created Date</th>
        <th width="50"> </th>
        <th width="50"> </th>
        <th width="50"> </th>
        <th width="50"> </th>
    </tr>
  </thead>
  <tbody>
 </tbody>
</table>
</div>
