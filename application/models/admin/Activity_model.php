<?php
/**	
 * @file
 *  Program		: activity_model.php 
 * 	Author		: CMS
 * 	Date		: 24/7/2015
 * 	Abstract	: activity
*/
class Activity_model extends CI_Model{
	function activity_model(){
		parent::__construct();
	}

	function form(){	// to create form
		$data['city']    	='city <span class="star_red">*</span> :';
		$data['tag']	    = 'Tag <span class="star_red">*</span> :';	
		$data['name']		='Name <span class="star_red">*</span> :';
		$data['image']  ='Image <span class="star_red">*</span> :';
		$data['desc']		='Description <span class="star_red">*</span> :';
		$data['address']	='Address <span class="star_red">*</span> :';	
		$data['longitude']	='Longitude <span class="star_red">*</span> :';
		$data['latitude']	='Latitude <span class="star_red">*</span> :';		
		$data['cDate']     ='created Date <span class="star_red">*</span> :';
		$data['uDate']     ='updated Date <span class="star_red">*</span> :';
		$data['selectedCity'] 		= '';
		$data['selectedTag'] 		= '';	
		$data['fImage']	= array('name'=>'upload', 'id'=>'upload', 'value'=>'');	
		$data['fName']		= array('name'=>'name', 'id'=>'name', 'placeholder'=>'Enter Name', 'value'=>'');
		$data['fDesc']		= array('name'=>'desc', 'id'=>'desc', 'value'=>'', 'rows'=>'8');
		$data['fAddress']	= array('name'=>'address', 'id'=>'address', 'placeholder'=>'Enter address','value'=>'', 'rows'=>'5');
		$data['fCdate']	= array('name'=>'cDate', 'id'=>'cDate', 'placeholder'=>'dd/mm/yyyy', 'value'=>'', 'class'=>'txtFromDate c_in');
		$data['fUdate']	= array('name'=>'uDate', 'id'=>'uDate', 'placeholder'=>'dd/mm/yyyy', 'value'=>'', 'class'=>'txtToDate c_out');
		$data['fLongitude']= array('name'=>'longitude', 'id'=>'longitude', 'value'=>'', 'rows'=>'5');
		$data['fLatitude']	= array('name'=>'latitude', 'id'=>'latitude', 'value'=>'', 'rows'=>'5');
		return $data;
	}
	
	function save($id,$fullpath){ // to save city data
		$finaltagid = '';
		$tagid = '';
		$tags = $this->input->post('tags');
		$insertresult = false;
		foreach($tags as $key){
			$query = $this->db->get_where('tag',array('keyword'=>$key));
			if($query->num_rows() > 0){
				$result = $query->result_array();
				$tagid = $result[0]['id'];
			}else{
				$data = array('keyword'=>$key,'created_date'=>date('Y-m-d'),'updated_date'=>date('Y-m-d'));
				$queryresult = $this->db->insert('tag', $data);
				$insertresult = true;
			}
			if($insertresult){
				$tagid = $this->db->insert_id();
				$insertresult = false;
			}
			 $finaltagid .= $tagid.",";
		}
	
		$city		= $this->input->post('city');
		$tag		= $finaltagid;
		$name 		= $this->input->post('name');
		$desc = preg_replace('/(<[^>]+) style=".*?"/i', '$1', $this->input->post('desc'));
		//$desc 		= $this->input->post('desc');
		$address 	= $this->input->post('address');
		$longitude 	= $this->input->post('longitude');
		$latitude 	= $this->input->post('latitude');
		$cDate 		= $this->input->post('cDate');
		$uDate 		= $this->input->post('uDate');
		if(!empty($cDate)){
			$created = explode('/', $cDate);	
			$cDate = $created[2].'-'.$created[1].'-'.$created[0];
		}
		if(!empty($uDate)){
			$updated = explode('/', $uDate);	
			$uDate = $updated[2].'-'.$updated[1].'-'.$updated[0];
		}
		$data = array(
					'name'=>$name, 
					'description'=>$desc,
					'city'=>$city, 
					'tag'=>$tag,
					'address'=>$address, 
					'image_url'=>$fullpath,
					'longitude'=>$longitude,  
					'latitude'=>$latitude, 
					'created_date'=>$cDate,
					'updated_date'=>$uDate
				);
		if(!empty($id) && $id > 0){
			$query1= $this->db->get_where('activities',array('id'=>$id));
			$result = $query1->result_array();			
			if($fullpath != $result[0]['image_url']){
				$img = explode('upload/',$result[0]['image_url']);
				$file = $_SERVER['DOCUMENT_ROOT'] . '/travel/images/activity/upload/' . $img[1];
				if (file_exists($file)) {
					unlink($file);
				}
			}
			$this->db->where('id',$id);
			$result = $this->db->update('activities',$data);	
		}else{
			$result = $this->db->insert('activities', $data);	
		}
		if($result){
			return $result;
		}else{
			return false;
		}
	}

	function delete($id){ // delete function
		$query1= $this->db->get_where('activities',array('id'=>$id));
		$result = $query1->result_array();
		if($result){
			$img = explode('upload/',$result[0]['image_url']);
			$file = $_SERVER['DOCUMENT_ROOT'] .'/travel/images/activity/upload/' . $img[1];
			// var/www/vhosts/appvantage.sg/appvantage\travel\images\info\upload
			if (file_exists($file)) {
			unlink($file);
			}
		}
		$this->db->where('id',$id);
		$query = $this->db->delete('activities');
		if($query){
			return TRUE;
		}else{
			return FALSE;
		}
	}
}
?>