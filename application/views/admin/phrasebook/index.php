
<div class="title"><?php echo ($id > 0) ? 'Edit '.$title : 'New '.$title; ?></div>
<?php 
$attributes = array('class'=>'eventform');
echo form_open_multipart(base_url().'admin/phrasebook/add/'.$id, $attributes);
if($error != '') $error = $error;
?>
<script type="text/javascript">
	$(document).ready(function(){
		$(".txtFromDate").datepicker({
			minDate: 0,
			maxDate: "+1530D",
			numberOfMonths: 1,
			onSelect: function(selected) {
			  $(".txtToDate").datepicker("option","minDate", selected)
			}
		});
		$(".txtToDate").datepicker({ 
			minDate: 0,
			maxDate:"+1530D",
			numberOfMonths: 1,
			onSelect: function(selected) {
			   $(".txtFromDate").datepicker("option","maxDate", selected)
			}
		});
	});
	tinymce.init({
		mode : "exact",
		elements : "desc",
		forced_root_block: false,
	});
</script>
<table style="width:780px !important;">
	<tr><td class="star_red" colspan="3"><?php echo $error; ?></td><td></td></tr>
    <?php
    if($id > 0){
       // var_dump($fName);
	?>
    <tr>
    	<td>ID : </td>
        <td><?php echo $id; ?></td>
    </tr>
    <?php
	}
	?>
      <tr>
    	<td style="padding-right:80px !important;"><?php echo $name_mm; ?></td>
        <td><?php echo form_input($fName_mm); 
				  echo form_error('namemm', '<div class="error">', '</div>');
		?></td>
    </tr>
     <tr>
    	<td><?php echo $name_en; ?></td>
        <td><?php echo form_input($fName_en);
			echo form_error('nameen', '<div class="error">', '</div>');
		 ?></td>
    </tr>
    <tr>
    	<td valign="top"><?php echo $audio; ?></td>
        <td><?php echo form_upload($fAudio). ' '.$fileName; ?></td>
    </tr>
     <tr>
    	<td valign="top"><?php echo $phonetic; ?></td>
        <td><?php	echo form_input($fPhontic);
					echo form_error('phonetic', '<div class="error">', '</div>');
		 ?></td>
    </tr>
     <tr>
    	<td valign="top"><?php echo $desc; ?></td>
        <td><?php echo form_textarea($fDesc); 
				  echo form_error('desc', '<div class="error">', '</div>');
		?></td>
    </tr>
    <tr>    
    	<td><?php echo $cDate; ?></td>
        <td><?php echo form_input($fCdate);
				  echo form_error('cDate', '<div class="error">', '</div>');
		?></td>
    </tr>
    <tr>
    	<td><?php echo $uDate; ?></td>
        <td><?php echo form_input($fUdate);
				  echo form_error('uDate', '<div class="error">', '</div>');
		?></td>
    </tr>
    <tr>
        <td colspan="2">
        <?php
		$submit = array('name' => 'submit', 'content'=> 'Save', 'id'=> 'submit','type'=> 'submit', 'value' => 'true', 'onclick'=>'', 'class'=>'btn btn-primary');
		echo form_button($submit);
		$cancel = array('name' => 'cancel', 'content'=> 'Cancel', 'id'=> 'submit','type'=> 'submit', 'value' => 'true', 'class'=>'btn');
		echo form_button($cancel);
		?>
        </td>
    </tr>
</table>
<?php echo form_close(); ?>