<?php	
header("Content-type: application/csv");
header("Content-Disposition: attachment; filename=".$fileName.'.csv');
$output= '';
$output .= 'id,name,image_url,updated_date,created_date';
$output .="\r\n";
if(is_array($history) && sizeof($history) > 0){
	foreach($history as $key=>$value){
		$created = explode('-',$value['created_date']);	
		$updated = explode('-', $value['updated_date']);
		$created_date = $created[2].'/'.$created[1].'/'.$created[0];
		$updated_date = $updated[2].'/'.$updated[1].'/'.$updated[0];
				
		$output .= $value['id'].','.$value['name'].','.$value['image_url'].','.$updated_date.','.$created_date;
		$output .="\r\n";
	}
}
echo $output;
exit;
?>
