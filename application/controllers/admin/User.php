<?php
/**	
 * @file
 *  Program		: user.php 
 * 	Author		: CMS
 * 	Date		: 21/7/2015
 * 	Abstract	: user
*/
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class User extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model('admin/user_model');
		
		$this->load->library(array('form_validation'));	
	}
	function index()
    {
		if(!$this->session->userdata('admin_logged_in')){
			redirect(base_url().'admin/login/');
		}else{
		$data['title'] = 'Users List';
        $this->load->view('admin/template/header',$data);
		$this->load->view('admin/user/userlist');
		$this->load->view('admin/template/footer');
		}
    }
	function getdatatable()
    {
		if(!$this->session->userdata('admin_logged_in')){
			redirect(base_url().'admin/login/');
		}else{
			$aColumns = array('id', 'name','country','email','created_date');
			// DB table to use
			$sTable = 'user';
			$iDisplayStart = $this->input->get_post('iDisplayStart', true);
			$iDisplayLength = $this->input->get_post('iDisplayLength', true);
			$iSortCol_0 = $this->input->get_post('iSortCol_0', true);
			$iSortingCols = $this->input->get_post('iSortingCols', true);
			$sSearch = $this->input->get_post('sSearch', true);
			$sEcho = $this->input->get_post('sEcho', true);    
			// Paging
			if(isset($iDisplayStart) && $iDisplayLength != '-1')
			{
				$this->db->limit($this->db->escape_str($iDisplayLength), $this->db->escape_str($iDisplayStart));
			}
			
			// Ordering
			if(isset($iSortCol_0))
			{
				for($i=0; $i<intval($iSortingCols); $i++)
				{
					$iSortCol = $this->input->get_post('iSortCol_'.$i, true);
					$bSortable = $this->input->get_post('bSortable_'.intval($iSortCol), true);
					$sSortDir = $this->input->get_post('sSortDir_'.$i, true);
		
					if($bSortable == 'true')
					{
						$this->db->order_by($aColumns[intval($this->db->escape_str($iSortCol))], $this->db->escape_str($sSortDir));
					}
				}
			}
			//Filtering
			if(isset($sSearch) && !empty($sSearch))
			{
				for($i=0; $i<count($aColumns); $i++)
				{
					$bSearchable = $this->input->get_post('bSearchable_'.$i, true);
					
					// Individual column filtering
					if(isset($bSearchable) && $bSearchable == 'true')
					{
						$this->db->or_like($aColumns[$i], $this->db->escape_like_str($sSearch));
					}
				}
			}
			
			// Select Data
			$this->db->select('SQL_CALC_FOUND_ROWS '.str_replace(' , ', ' ', implode(', ', $aColumns)), false);
			$rResult = $this->db->get($sTable);
		
			// Data set length after filtering
			$this->db->select('FOUND_ROWS() AS found_rows');
			$iFilteredTotal = $this->db->get()->row()->found_rows;
		
			// Total data set length
			$iTotal = $this->db->count_all($sTable);
		
			// Output
			$output = array(
				'sEcho' => intval($sEcho),
				'iTotalRecords' => $iTotal,
				'iTotalDisplayRecords' => $iFilteredTotal,
				'aaData' => array()
			);     
			foreach ($rResult->result_array()  as $r) {
				$data = array();
				$id = $r['id'];
				$baseurl =  base_url();	
				$delUrl =  $baseurl.'admin/user/delete/';
				$idurl = $id.'-'.$delUrl;
				$rr['delete'] = "<a id='" . $idurl. "' class='del' onclick='del(this.id)'> Delete </a>";	
				$data[] =$r['id'];
				$data[] =$r['name'];
				$data[] =$r['country'];
				$data[] =$r['email'];
				$data[] =$r['created_date'];			
				$data[] =  anchor('admin/user/add/' . $r['id'], 'Edit');
				$data[] =  $rr['delete'];
				$output['aaData'][] = $data;
			}
			echo json_encode($output);
		}
    }
	// end server side processing
	// to edit user data
	function add($id=''){ 
		if(!$this->session->userdata('admin_logged_in')){
			redirect(base_url().'admin/login/');
		}else{	
			if($this->input->post('cancel'))
			{
				redirect(base_url().'admin/user/');
			}
			$data = $this->user_model->form();
			$data['title'] = 'user';
			$data['id'] = $id;
			$data['error'] = '';
			$cities = gettable('city');
			foreach ($cities as $city){
				$data['allCity'][$city['id']] = $city['name'];
			}
			$data['selectedCity'] = '';			
			if($id > 0){
				$user = gettable('user','id',$id);
				if($user){
					$created = explode('-', $user[0]['created_date']);
					$updated = explode('-', $user[0]['updated_date']);
					$data['fName']['value'] = $user[0]['name'];
					$data['fEmail']['value'] = $user[0]['email'];
					$data['fCountry']['value'] = $user[0]['country'];
					$data['fCdate']['value'] = $created[2].'/'.$created[1].'/'.$created[0];
					//$data['fUdate']['value'] = $updated[2].'/'.$updated[1].'/'.$updated[0];
				}else{
					redirect(base_url().'admin/user/');
				}
			}
			if($this->input->post('submit')){
				$result = $this->user_model->save($id);
				if($result)
				{
					redirect(base_url().'admin/user/');
				}					
			}
			$this->load->view('admin/template/header', $data); 
			$this->load->view('admin/user/index', $data); 
			$this->load->view('admin/template/footer');
		}
	}
	// export data as csv file
	function export(){
		if(!$this->session->userdata('admin_logged_in')){
			redirect(base_url().'admin/login/');
		}else{
			$startDate = date('Y-m-d 00:00:01');
			$start = strtotime($startDate);
			$endDate = date('Y-m-d 23:59:59');
			$end = strtotime($endDate);
			
			$user = gettable('user');
			$data['fileName'] = 'user'.date('dmy');
			$data['user'] = array();
			$data['user'] = $user;
			$this->load->view('admin/user/export', $data);
		}
		
	}
	// import data as excel file
	function import(){
		if(!$this->session->userdata('admin_logged_in')){
			redirect(base_url().'admin/login/');
		}else{
			if($this->input->post('cancel'))
			{
				redirect(base_url().'admin/user/');
			}
			$data['error'] = '';
			if($this->input->post('submit')){		
				$config['upload_path'] = './images/user/upload/table';
				$config['allowed_types'] = 'csv';
				$config['max_size']	= '1024';
				$config['max_width']  = '1024';
				$config['max_height']  = '768';
				$this->load->library('upload', $config);
				$upload = $this->upload->do_upload('upload');
				if(!$upload){
					$data['error'] = $this->upload->display_errors();
				}else{
					$fileName = $this->upload->data('file_name');
					$this->load->library('csvreader');	
					$fullpath = $this->upload->data('full_path');
					$data['csvData'] = $this->csvreader->parse_file($fullpath);
					
					foreach($data['csvData'] as $cd)
					{
						if (isset($cd['name'],$cd['country'],$cd['email'])){
							//$created_date = datetime($cd['created_date']);
							//$updated_date = datetime($cd['updated_date']);
							$id = $cd['id'];
							$results_array = array(
												  'name' => $cd['name'],
												  'country' => $cd['country'],
												  'email' => $cd['email'],
												  'created_date' => date('Y-m-d'),
												  'updated_date' => date('Y-m-d')
												   );  
						  $query = $this->db->get_where('user',array('id'=>$id));
						  if($query->num_rows() > 0){
							  $this->db->where('id',$id);
							  $result = $this->db->update('user',$results_array);
						  }else{				  
							  $result = $this->db->insert('user', $results_array);
						  }		
					  }else{
							$data['error'] = "Please check  your csv file";
							$result = false;
					  }		  			  
					}	
					if($result){
						 redirect(base_url().'admin/user/');
					}
				}	
			}
			$this->load->view('admin/template/header',$data); 
			$this->load->view('admin/user/import');
			$this->load->view('admin/template/footer');
		}		
	}
	function delete(){
		if(!$this->session->userdata('admin_logged_in')){
			redirect(base_url().'admin/login/');
		}else{
			$id = $_REQUEST['id'];
			$result = $this->user_model->delete($id);
			echo json_encode(array('status'=>$result));		
		}
	}
}
?>