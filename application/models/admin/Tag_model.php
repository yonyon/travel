<?php
/**	
 * @file
 *  Program		: tag_model.php 
 * 	Author		: CMS
 * 	Date		: 20/07/2015
 * 	Abstract	: tag
*/
class Tag_model extends CI_Model{
	function tag_model(){
		parent::__construct();
	}
	// to create form
	function form(){		
		$data['category']    ='Category Name <span class="star_red">*</span> :';	
		$data['keyword']    ='Key Word <span class="star_red">*</span> :';			
		$data['cDate']     ='created Date <span class="star_red">*</span> :';
		$data['uDate']     ='updated Date <span class="star_red">*</span> :';	
		$data['selectedCategory'] = '';	
		$data['fKeyword']		= array('name'=>'keyword', 'id'=>'keyword', 'value'=>'', 'rows'=>'8');		
		$data['fCdate']	= array('name'=>'cDate', 'id'=>'cDate', 'placeholder'=>'dd/mm/yyyy', 'value'=>'', 'class'=>'txtFromDate c_in');
		$data['fUdate']	= array('name'=>'uDate', 'id'=>'uDate', 'placeholder'=>'dd/mm/yyyy', 'value'=>'', 'class'=>'txtToDate c_out');		
		return $data;
	}	
	// to save tag data
	function save($id){
		$category= $this->input->post('category');
		$keyword 		= $this->input->post('keyword');
		$cDate 		= $this->input->post('cDate');
		$uDate 		= $this->input->post('uDate');
		if(!empty($cDate)){
			$created = explode('/', $cDate);	
			$cDate = $created[2].'-'.$created[1].'-'.$created[0];
		}
		if(!empty($uDate)){
			$updated = explode('/', $uDate);	
			$uDate = $updated[2].'-'.$updated[1].'-'.$updated[0];
		}
		$data = array(
					'category'=>$category,
					'keyword'=>$keyword,
					'created_date'=>$cDate,
					'updated_date'=>$uDate
				);
		
		if(!empty($id) && $id > 0){
			$this->db->where('id',$id);
			$result = $this->db->update('tag',$data);	
		}else{
			$result = $this->db->insert('tag', $data);	
		}
		if($result){
			return $result;
		}else{
			return false;
		}
	}

	// delete function
	function delete($id){
		$del = true;
		$atraction_query = $this->db->get_where('attraction',array('tag'=>$id));
		if($atraction_query->num_rows() > 0){
			$del = false;
		}
		$hotel_query = $this->db->get_where('hotel',array('tag'=>$id));
		if($hotel_query->num_rows() > 0){
			$del = false;
		}
		$restaurant_query = $this->db->get_where('restaurant',array('tag'=>$id));
		if($restaurant_query->num_rows() > 0){
			$del = false;
		}
		$activity_query = $this->db->get_where('activities',array('tag'=>$id));
		if($activity_query->num_rows() > 0){
			$del = false;
		}
		$tripplan_query = $this->db->get_where('trip_plan',array('tag'=>$id));
		if($tripplan_query->num_rows() > 0){
			$del = false;
		}
		if($del){
			$this->db->where('id',$id);
			$query = $this->db->delete('tag');
			if($query){
				return TRUE;
			}else{
				return FALSE;
			}
		}else{
			return $del;
		}
	}
}
?>