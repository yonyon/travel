
<!--<div class="container">-->
<style>
div.container div#forgotPwdPage {
	padding:10em 0 9.5em;
}
</style>
<div class="banner-bottom" align="center" id="forgotPwdPage">
    <div class="well span5 center login-box res">               
        <div class="alert alert-info">        	
            <?php if(isset($error)) echo '<span class="error">'.$error.'</span>'; else{ ?>Please enter your email to get new password.<?php } ?>
        </div>
        <?php 
			$attributes = array('class'=>'form-horizontal','method'=>'post');
            echo form_open_multipart(base_url().'forgot_password/email', $attributes);
        ?>
        <table class="tbl-register" style="text-align:left">       
            <tr>
                <td>Email</td>
                <td>
					<?php 
                    $email = array('class'=>'input-large','name'=>'email','id'=>'email','autofocus'=>'autofocus');
                    echo form_input($email); 
					echo form_error('email', '<div class="error">', '</div>');
                    ?>
                </td>
            </tr>    
           
            <tr>
                <td colspan="2">                                    
                    <p class="center span5">
                    	<button type="submit" class="btn btn-primary btnres" name="submit" value="true">Continue</button>
                    </p>
                </td>
            </tr>  
        </table>
        <?php echo form_close(); ?>
        </div><!--/span-->
    </div><!--/row-->
    
    <!--</div>-->
