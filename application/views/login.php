<?php
//include 'description.php';
?>
<style>
div.container div#loginPage {
	padding:8em 0 7em;
}
</style>
<!--<div class="container">-->
<div class="banner-bottom " align="center" id="loginPage">
    <div class="well span5 center login-box res">               
        <div class="alert alert-info">
        	<?php if(isset($error)) echo '<span class="error">'.$error.'</span>'; else{ ?>Please login with your Email and Password.<?php } ?>
        </div>
        <?php 
			$attributes = array('class'=>'form-horizontal','method'=>'post');
			echo form_open_multipart(base_url().'login/save', $attributes);
        ?>  
        <table class="tbl-register" style="text-align:left">        
            <tr>
                <td>Email</td>
                <td>
               	 <div class="input-prepend" title="Username" data-rel="tooltip">
					<?php 
                    $email = array('class'=>'input-large','name'=>'email','id'=>'email','autofocus'=>'autofocus');
                    echo form_input($email,$fEmail); 
					echo form_error('email', '<div class="error">', '</div>'); 
                    ?>
                </td>
                </div>
            </tr>    
            <tr>
                <td> Password </td>
                <td>
					<?php 
                    $password = array('class'=>'input-large','name'=>'password','id'=>'password','autofocus'=>'autofocus');
                    echo form_password($password); 
					echo form_error('password', '<div class="error">', '</div>');
                    ?>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <button type="submit" class="btn btn-primary btnres" name="submit" value="true">Login</button>
                </td>
            </tr>  
        </table>
        <?php echo form_close(); ?>
        <div class="input-prepend" title="Password" data-rel="tooltip">
        <a href="<?php echo base_url() ?>forgot_password">Forgot password?</a><br/>
        Don't have account? Register <a href="<?php echo base_url() ?>register">here</a>
        
        </div>
    </div><!--/span-->
    </div><!--/row-->
    
   <!-- </div>-->
