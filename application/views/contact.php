<style>
#contactMsg{
	color:#9ec503;
	height:30px;
}
</style>
<div class="contact">
	<div class="map">
		<h3>Contact</h3>
		<p class="gal-txt">Nam libero tempore, cum soluta nobis est eligendi optio 
		cumque nihil impedit quo minus id quod maxime placeat facere possimus,
		omnis voluptas assumenda est, omnis dolor repellendus.</p>
		<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2040611.236592339!2d-56.9479281!3d-2.6446517!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x926eca1645365b6b%3A0xabfc431d20b2b474!2sAmazon+River%2C+Brazil!5e0!3m2!1sen!2sin!4v1432120609770" frameborder="0" style="border:0"></iframe>
		<!--<iframe width="600" height="450" frameborder="0" style="border:0" src="https://www.google.com/maps/embed/v1/search?q=Appvantage%20Pte%20Ltd%20Singapore&key=AIzaSyDw1Mi7peQmPOhAGhMlQLTp4gRpjZSfL50"></iframe>-->
	</div>
	<div class="contact-form">
		<div class="col-md-4 contact-form-left">
			<h4>CONTACT INFO</h4>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, 
				sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
			<h5>Address:</h5>
			<p>Eiusmod Tempor inc</p>
			<p>9560 St Dolore Place,</p>
			<p>Telephone: +1 800 603 6035</p>
			<p>FAX: +1 800 889 9898</p>
			<a href="mailto:example@email.com">mail@example.com</a>
		</div>
		<div class="col-md-8 contact-form-right">
			<div id="contactMsg"><?php echo $msg; ?></div>
			<h4>CONTACT FORM</h4>
			<form class="" name="" method="post">
				<input type="text" name="name"  placeholder="Name"  required />
				<input type="email" name="email"  placeholder="Email"  required />
				<input type="text" name="phone" class="" placeholder="Telephone" />
				<textarea name="message" value="message" cols="40" rows="6" placeholder="Message" required ></textarea>
				<!--<input type="text" value="Name" onFocus="this.value = '';" onBlur="if (this.value == '') {this.value = 'Name';}" required>
				<input type="email" value="Email" onFocus="this.value = '';" onBlur="if (this.value == '') {this.value = 'Email';}" required>
				<input type="text" value="Telephone" onFocus="this.value = '';" onBlur="if (this.value == '') {this.value = 'Telephone';}" required>
				<textarea type="text"  onfocus="this.value = '';" onBlur="if (this.value == '') {this.value = 'Message...';}" required="">Message...</textarea>-->
				<input type="submit" value="Submit" >
			</form>
		</div>
		<div class="clearfix"></div>
	</div>
</div>
<!--https://console.developers.google.com/home/dashboard?project=vantagetrip-->