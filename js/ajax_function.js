function del(idurl){
	var idurl = idurl.split("-");
	var id = idurl[0] ; 
	var url = idurl[1];
	var confirmDel =confirm("Are you sure to delete? ");
	if (confirmDel)
	{
	 	$.ajax({
			type: 'post',
			url: url,
			data: 'id='+id,
			success:function(data){
				var data = JSON.parse(data);
				if(data.status){
					location.reload(); 
				}else{
					alert('you can not delete this row');
					return false;
				}
				
			}
		});
	}else{
		return false;
	}
}


