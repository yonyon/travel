<style>
#subscribe .success{
	color:#A5C81D;
}
#subscribe .error{
	display:block;
}
#subscribe .success, #subscribe .error{
	padding-top:20px;
}
</style>
<?php 
$url = '';
if($error == true) $class = 'error';
else $class = 'success';
if($message == '') $class = '';
?>
<div class="banner-bottom" id="event">
	<div id="listArea" class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
		<?php
			if($results != false){// check data is exist or not
				//echo '<pre>';print_r($results);echo '</pre>';
				$detailurl1 = '';
				foreach($results as $result)
				{                     
					if(isset ($result['tag'])){
					$keyword = getcolname('tag','keyword',$result['tag']); }
					$id1 = $result['id'];
					$result['description'] = strip_tags($result['description']);
					$desc = substr($result['description'] , 0,250)."....";
					$url = $result['image_url'];
					//$detailurl1 =  base_url().$detailurl.$id."/".$id1."/";
					$detailurl1 =  base_url().$detailurl.$id1."/";
					?>
					<div class="col-md-0 row" style="margin:0 auto;">
						<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 panel-body">
						<img class="img-responsive" src="<?php echo $url; ?>" />
						</div>
						<div class="col-xs-12 col-sm-8 col-md-8 col-lg-8 panel-body">
							<h4>
								<span style="font-size: 18pt; color: #ec534d;"><?php echo ucwords(strtolower($result['name'])); ?>
								</span>
							</h4>
							<p><?php echo $desc; ?></p>
							<p>
								<a href="<?php echo $detailurl1; ?>" class="desmore">More</a>
								<?php 
								 if(isset ($result['tag'])){
									$keyword = explode(',',$result['tag']);
									foreach ($keyword as $value){
										$query_tag = $this->db->get_where('tag',array('id'=>$value));
										$result_tag =  $query_tag->result_array();
										if($result_tag){
										?>
											<span style="border:1px solid black;border-radius:3px;padding:3px;margin-right:6px;display:inline-block;" class="keyword"><a href='<?php echo base_url().$tagurl.$value.'/'.$id; ?>' style="text-decoration:none;"><?php echo $result_tag[0]['keyword']; ?> </a></span>
										<?php
										}
									}
								}									
								?>
							</p>
						</div>
					</div>
					
		<?php
			}
		?>
			<div class="div-pagination"><?php echo $links; ?></div>
		<?php
		}else{
		?>
			<div class="error-page">
				<div class="error-info">
					<h3></h3>
					<p>There is no list.</p>
				</div>
			</div>
		<?php
		}
		?>
					
		
	</div>
	<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 sing-img-text-left" id="subscribe">
		<div class="blog-right1">
			<div class="search">
				<h3>NEWSLETTER</h3>
				<form method="post" action="">
					<input type="text" value="Email..." onFocus="this.value = '';" onBlur="if (this.value == '') {this.value = 'Email...';}" id="email" name="email" required="">
				<div class="<?php echo $class; ?> subscribeMsg"><?php echo $message; ?></div>
				<input type="submit" value="Subscribe">
				</form>
			</div>
			<?php
			if($type == 'city'){
			?>
			<div class="categories">
				<h3>Categories</h3>
				<ul>
					<?php 
					
					/*if($type == 'city') $id = $id;
					else $id = $info[0]['city'];*/
					
					?>
					<li><a href="<?php echo base_url(); ?>wheretostay/show/<?php echo $id; ?>">WHERE TO STAY</a></li>
					<li><a href="<?php echo base_url(); ?>wheretovisit/show/<?php echo $id; ?>">WHERE TO VISIT</a></li>
					<li><a href="<?php echo base_url(); ?>wheretoeat/show/<?php echo $id; ?>">WHERE TO EAT</a></li>
					<li><a href="<?php echo base_url(); ?>whattodo/show/<?php echo $id; ?>">WHAT TO DO</a></li>
				</ul>
			</div>
			<?php
			}
			?>
		</div>
	</div>
	<span class="clearfix"></span>
</div>