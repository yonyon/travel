<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE html>
<html>
<head>
<title>VantageTrip : <?php echo ucfirst($menu); ?></title>
<link href="<?php echo base_url();?>css/style2.css" rel="stylesheet" type="text/css" media="all" />
<link href="<?php echo base_url();?>css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
<link href="<?php echo base_url();?>css/bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url();?>css/navbar.css" rel="stylesheet">
<!--<link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Roboto+Slab:300,700,100,400' rel='stylesheet' type='text/css'>-->
<!-- js -->
<script src="<?php echo base_url();?>js/jquery.min.js"></script>
<script src="<?php echo base_url();?>js/bootstrap.min.js"></script>
<!-- //js -->
<!-- for-mobile-apps -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Eco Travel Responsive web template, Bootstrap Web Templates, Flat Web Templates, Andriod Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyErricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
		function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- //for-mobile-apps -->
<!-- start-smoth-scrolling -->
<script type="text/javascript" src="<?php echo base_url();?>js/move-top.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/easing.js"></script>
<script type="text/javascript">
	jQuery(document).ready(function($) {
		$(".scroll").click(function(event){		
			event.preventDefault();
			$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
		});
	});
</script>
<!-- start-smoth-scrolling -->
</head>
	
<body>
<!-- banner -->
	<div class="banner-with-text1">
		<div class="container">
<!-- header -->	
		<div class="header">
			<div class="header-top pull-right">
				<input type="text" placeholder="Search" required=" ">
			</div>
			<div class="clearfix"> </div>
			<div class="header-bottom">
				<div class="header-bottom-left">
					<a href="index.html">Vantage<br>Trip</span></a>
				</div>
				<div class="header-bottom-right">
					<span class="menu">MENU</span>
					<ul class="nav1">
						<li class="active"><a href="<?php echo base_url(); ?>home">Home</a></li>
                        <li class="dropdown upp <?php if($menu == 'city') echo 'selected'; ?>"><a href="#" class="dropdown-toggle" data-toggle="dropdown">Cities<b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                <?php 
									$query = $this->db->get('city');			
									$results = $query->result_array();
									$rsize = sizeof($results);
									$i = 1;
									$addClass = '';
									foreach($results as $result)
									{
										if($i == $rsize) $addClass = 'class="last"';
										$linkpage=preg_replace('/\s+/', '', $result['name']);
                            			$linkpage = 'Details/show/city';
										echo '<li '.$addClass.'>';
										$linkpage = base_url().$linkpage;
										echo '<a href="' .$linkpage .'/'.$result['id']. '">' .$result['name'] . '</a>';
										echo '</li>';
										$i++;
									}
								?>
                            </ul>
                        </li>
						<li class="<?php if($menu == 'essential information') echo 'selected'; ?>"><a href="<?php echo base_url(); ?>essinfo/show/1">Essential Information</a></li>
                        <li class="<?php if($menu == 'history') echo 'selected'; ?>"><a href="<?php echo base_url(); ?>history/show/1">History</a></li>
                        <li class="<?php if($menu == 'culture') echo 'selected'; ?>"><a href="<?php echo base_url(); ?>culture/show/1">Culture</a></li>
                        <li class="<?php if($menu == 'trip plan') echo 'selected'; ?>"><a href="<?php echo base_url(); ?>tripplan/show/1">Trip plan</a></li>
                        <li class="<?php if($menu == 'tips') echo 'selected'; ?>"><a href="<?php echo base_url(); ?>tip/show/1">Tips</a></li>
                      	<?php  if ($this->session->userdata('user_logged_in')){ ?>
                        <li class="<?php if($menu == 'my account') echo 'selected'; ?>"><a href="<?php echo base_url(); ?>myaccount/show">My account</a></li>
                         <li><a href="<?php echo base_url(); ?>login/out">logout</a></li>
            			<?php }else{?>
                        <li class="<?php if($menu == 'login') echo 'selected'; ?>"><a href="<?php echo base_url(); ?>login">Login</a></li>
                        <li class="<?php if($menu == 'register') echo 'selected'; ?>"><a href="<?php echo base_url(); ?>register">Register</a></li>
						<li class="<?php if($menu == 'contact') echo 'selected'; ?>"><a href="<?php echo base_url(); ?>contact">Contact</a></li>
                        <?php }?>
					</ul>
					<!-- script for menu -->
						<script> 
							$( "span.menu" ).click(function() {
							$( "ul.nav1" ).slideToggle( 300, function() {
							 // Animation complete.
							});
							});
						</script>
					<!-- //script for menu -->
				</div>
				<div class="clearfix"> </div>
			</div>
		</div>	
	</div>
</div>
<!-- //header -->
<!-- single -->
	<div class="banner-bdy sig">
		<div class="container">