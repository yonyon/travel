<?php
/**	
 * @file
 *  Program		: category.php 
 * 	Author		: CMS
 * 	Date		: 17/7/2015
 * 	Abstract	:category
*/
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Category extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model('admin/category_model');
		$this->load->library(array('form_validation'));	
	}
	function index()
    {
		if(!$this->session->userdata('admin_logged_in')){
			redirect(base_url().'admin/login/');
		}else{
		$data['title'] = 'Category List';
        $this->load->view('admin/template/header',$data);
		$this->load->view('admin/category/categorylist');
		$this->load->view('admin/template/footer');
		}
    }
	function getdatatable()
    {
		if(!$this->session->userdata('admin_logged_in')){
			redirect(base_url().'admin/login/');
		}else{
      
			$aColumns = array('id', 'name','code','updated_date','created_date');        
			// DB table to use
			$sTable = 'category';
			
			$iDisplayStart = $this->input->get_post('iDisplayStart', true);
			$iDisplayLength = $this->input->get_post('iDisplayLength', true);
			$iSortCol_0 = $this->input->get_post('iSortCol_0', true);
			$iSortingCols = $this->input->get_post('iSortingCols', true);
			$sSearch = $this->input->get_post('sSearch', true);
			$sEcho = $this->input->get_post('sEcho', true);
		
			// Paging
			if(isset($iDisplayStart) && $iDisplayLength != '-1')
			{
				$this->db->limit($this->db->escape_str($iDisplayLength), $this->db->escape_str($iDisplayStart));
			}
			
			// Ordering
			if(isset($iSortCol_0))
			{
				for($i=0; $i<intval($iSortingCols); $i++)
				{
					$iSortCol = $this->input->get_post('iSortCol_'.$i, true);
					$bSortable = $this->input->get_post('bSortable_'.intval($iSortCol), true);
					$sSortDir = $this->input->get_post('sSortDir_'.$i, true);
		
					if($bSortable == 'true')
					{
						$this->db->order_by($aColumns[intval($this->db->escape_str($iSortCol))], $this->db->escape_str($sSortDir));
					}
				}
			}
		   //Filtering
			if(isset($sSearch) && !empty($sSearch))
			{
				for($i=0; $i<count($aColumns); $i++)
				{
					$bSearchable = $this->input->get_post('bSearchable_'.$i, true);
					
					// Individual column filtering
					if(isset($bSearchable) && $bSearchable == 'true')
					{
						$this->db->or_like($aColumns[$i], $this->db->escape_like_str($sSearch));
					}
				}
			}
			
			// Select Data
			$this->db->select('SQL_CALC_FOUND_ROWS '.str_replace(' , ', ' ', implode(', ', $aColumns)), false);
			$rResult = $this->db->get($sTable);
		
			// Data set length after filtering
			$this->db->select('FOUND_ROWS() AS found_rows');
			$iFilteredTotal = $this->db->get()->row()->found_rows;
		
			// Total data set length
			$iTotal = $this->db->count_all($sTable);
		
			// Output
			$output = array(
				'sEcho' => intval($sEcho),
				'iTotalRecords' => $iTotal,
				'iTotalDisplayRecords' => $iFilteredTotal,
				'aaData' => array()
			);     
			foreach ($rResult->result_array()  as $r) {
				$data = array();
				$id = $r['id'];
				$baseurl =  base_url();	
				$delUrl =  $baseurl.'admin/category/delete/';
				$idurl = $id.'-'.$delUrl;
				$rr['delete'] = "<a id='" . $idurl. "' class='del' onclick='del(this.id)'> Delete </a>";	
				$data[] =$r['id'];
				$data[] =$r['name'];
				$data[] =$r['code'];
				$data[] =$r['updated_date'];
				$data[] =$r['created_date'];
				$data[] =  anchor('admin/category/add/' . $r['id'], 'Edit');
				$data[] =  $rr['delete'];
				$output['aaData'][] = $data;
			}
			echo json_encode($output);
		}
    }
	// end server side processing	
	// to add/edit category data
	function add($id=''){ 
		if(!$this->session->userdata('admin_logged_in')){
			redirect(base_url().'admin/login/');
		}else{	
			if($this->input->post('cancel'))
			{
				redirect(base_url().'admin/category/');
			}
			$data = $this->category_model->form();
			$data['title'] = 'category ';
			$data['id'] = $id;
			$data['error'] = '';			
			$data['allMapicon'] = array('1'=>'Embassy','2'=>'Hospital','4'=>'Money Changer','5'=>'Police Office','6'=>'Post Office');
			
			if($id > 0){
				$category = gettable('category','id',$id);
				if($category){
					$created = explode('-', $category[0]['created_date']);
					$updated = explode('-', $category[0]['updated_date']);
					$data['fName']['value'] = $category[0]['name'];
					$data['fCode']['value'] = $category[0]['code'];
					$data['fDesc']['value'] = $category[0]['description'];
					$data['fCdate']['value'] = $created[2].'/'.$created[1].'/'.$created[0];
					$data['fUdate']['value'] = $updated[2].'/'.$updated[1].'/'.$updated[0];
					$this->form_validation->set_rules('code','code','trim|required|xss_clean');
				}else{
					redirect(base_url().'admin/category/');
				}
			}else{
				$this->form_validation->set_rules('code','code','trim|required|xss_clean|is_unique[category.code]');
			}
			$this->form_validation->set_rules('name','Name','trim|required|xss_clean');
			$this->form_validation->set_rules('desc','Description','trim|required|xss_clean');
			$this->form_validation->set_rules('cDate','Start Date','trim|required|xss_clean');
			$this->form_validation->set_rules('uDate','End Date','trim|required|xss_clean');
			
			if($this->input->post('submit')){	
				if($this->form_validation->run()===TRUE){			
					$result = $this->category_model->save($id);
					if($result)
					{
						redirect(base_url().'admin/category/');
					}
				}
				$data['fName']['value'] 	= $this->input->post('name');
				$data['fDesc']['value'] 	= $this->input->post('desc');
				$data['fCdate']['value'] 	= $this->input->post('cDate');
				$data['fUdate']['value'] 	= $this->input->post('uDate');
			}
			$this->load->view('admin/template/header', $data); 
			$this->load->view('admin/category/index', $data); 
			$this->load->view('admin/template/footer');
		}
	}
	// export data as csv file
	function export(){
		if(!$this->session->userdata('admin_logged_in')){
			redirect(base_url().'admin/login/');
		}else{
			$startDate = date('Y-m-d 00:00:01');
			$start = strtotime($startDate);
			$endDate = date('Y-m-d 23:59:59');
			$end = strtotime($endDate);
			$category = gettable('category');
			$data['fileName'] = 'category'.date('dmy');
			$data['category'] = array();
			$data['category'] = $category;
			$this->load->view('admin/category/export', $data);
		}		
	}
	// import data as excel file
	function import(){
		if(!$this->session->userdata('admin_logged_in')){
			redirect(base_url().'admin/login/');
		}else{
			if($this->input->post('cancel'))
			{
				redirect(base_url().'admin/category/');
			}
			$data['error'] = '';
			if($this->input->post('submit')){		
				$config['upload_path'] = './images/category/upload/table';
				$config['allowed_types'] = 'csv';
				$config['max_size']	= '1024';
				$config['max_width']  = '1024';
				$config['max_height']  = '768';
				$this->load->library('upload', $config);
				$upload = $this->upload->do_upload('upload');
				if(!$upload){
					$data['error'] = $this->upload->display_errors();
				}else{
					$fileName = $this->upload->data('file_name');
					$this->load->library('csvreader');	
					$fullpath = $this->upload->data('full_path');
					$data['csvData'] = $this->csvreader->parse_file($fullpath);
					
					foreach($data['csvData'] as $cd)
					{
						if (isset($cd['name'],$cd['description'])){
							//$created_date = datetime($cd['created_date']);
							//$updated_date = datetime($cd['updated_date']);						
							$id = $cd['id'];
							$results_array = array(
												  'name' => $cd['name'],										  
												  'description' => $cd['description'],
												  'created_date' => date('Y-m-d'),
												  'updated_date' => date('Y-m-d')
												   );  
						  $query = $this->db->get_where('category',array('id'=>$id));
						  if($query->num_rows() > 0){
							  $this->db->where('id',$id);
							  $result = $this->db->update('category',$results_array);
						  }else{				  
							  $result = $this->db->insert('category', $results_array);
						  }
					  }else{
							$data['error'] = "Please check the column of your csv file";
							$result = false;
					  }				  			  
					}	
					if($result){
						 redirect(base_url().'admin/category/');
					}
				}	
			}
			$this->load->view('admin/template/header',$data); 
			$this->load->view('admin/category/import');
			$this->load->view('admin/template/footer');
		}
		
	}
	function delete(){
		if(!$this->session->userdata('admin_logged_in')){
			redirect(base_url().'admin/login/');
		}else{
			$id = $_REQUEST['id'];
			$result = $this->category_model->delete($id);
			echo json_encode(array('status'=>$result));	
		}
	}
}
?>