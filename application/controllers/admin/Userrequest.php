<?php
/**	
 * @file
 *  Program		: userrequest.php 
 * 	Author		: CMS
 * 	Date		: 17/7/2015
 * 	Abstract	: userrequest
*/
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Userrequest extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model('admin/userrequest_model');
		$this->load->library(array('form_validation'));	
	}
	function index()
    {
		if(!$this->session->userdata('admin_logged_in')){
			redirect(base_url().'admin/login/');
		}else{
		$data['title'] = 'User request List';
        $this->load->view('admin/template/header',$data);
		$this->load->view('admin/userrequest/userrequestlist');
		$this->load->view('admin/template/footer');
		}
    }
	function getdatatable()
    { 
		if(!$this->session->userdata('admin_logged_in')){
			redirect(base_url().'admin/login/');
		}else{     
			$aColumns = array('id','type','city', 'logo','logo_url','name','image','image_url','status','created_date');        
			// DB table to use
			$sTable = 'user_request';
		
			$iDisplayStart = $this->input->get_post('iDisplayStart', true);
			$iDisplayLength = $this->input->get_post('iDisplayLength', true);
			$iSortCol_0 = $this->input->get_post('iSortCol_0', true);
			$iSortingCols = $this->input->get_post('iSortingCols', true);
			$sSearch = $this->input->get_post('sSearch', true);
			$sEcho = $this->input->get_post('sEcho', true);
			// Paging
			if(isset($iDisplayStart) && $iDisplayLength != '-1')
			{
				$this->db->limit($this->db->escape_str($iDisplayLength), $this->db->escape_str($iDisplayStart));
			}        
			// Ordering
			if(isset($iSortCol_0))
			{
				for($i=0; $i<intval($iSortingCols); $i++)
				{
					$iSortCol = $this->input->get_post('iSortCol_'.$i, true);
					$bSortable = $this->input->get_post('bSortable_'.intval($iSortCol), true);
					$sSortDir = $this->input->get_post('sSortDir_'.$i, true);
		
					if($bSortable == 'true')
					{
						$this->db->order_by($aColumns[intval($this->db->escape_str($iSortCol))], $this->db->escape_str($sSortDir));
					}
				}
			}
		   // Filtering
			if(isset($sSearch) && !empty($sSearch))
			{
				for($i=0; $i<count($aColumns); $i++)
				{
					$bSearchable = $this->input->get_post('bSearchable_'.$i, true);
					
					// Individual column filtering
					if(isset($bSearchable) && $bSearchable == 'true')
					{
						$this->db->or_like($aColumns[$i], $this->db->escape_like_str($sSearch));
					}
				}
			}
			
			// Select Data
			$this->db->select('SQL_CALC_FOUND_ROWS '.str_replace(' , ', ' ', implode(', ', $aColumns)), false);
			$rResult = $this->db->get($sTable);
		
			// Data set length after filtering
			$this->db->select('FOUND_ROWS() AS found_rows');
			$iFilteredTotal = $this->db->get()->row()->found_rows;
		
			// Total data set length
			$iTotal = $this->db->count_all($sTable);
		
			// Output
			$output = array(
				'sEcho' => intval($sEcho),
				'iTotalRecords' => $iTotal,
				'iTotalDisplayRecords' => $iFilteredTotal,
				'aaData' => array()
			);     
			foreach ($rResult->result_array()  as $r) {
				$strhref1 = "<a href='" . $r['image_url'] . "' target='_blank'>";
				$strImage = $strhref1 . "<img src='" . $r['image_url'] . "' width='20' height='20' border='0' /> </a> ";
				
				$strhref2 = "<a href='" . $r['logo_url'] . "' target='_blank'>";
				$strLogo = $strhref2 . "<img src='" . $r['logo_url'] . "' width='20' height='20' border='0' /> </a> ";
				$status = $r['status'];
				if($status == 1){
					$status = '<input type="checkbox" disabled="disabled"  checked>';
				}else{
					$status = '<input type="checkbox" disabled="disabled" >';
				}
				
				$cityid = $r['city'];
				$city = $this->db->get_where('city',array('id'=>$cityid));
				$result = $city->result_array();		
				
				if($result){
					$cityname = $result[0]['name'];
				}else{
					$cityname = $r['city'];
				}		
						
				$data = array();
				$id = $r['id'];
				$baseurl =  base_url();	
				$delUrl =  $baseurl.'admin/userrequest/publish/';
				$pubUrl =  $baseurl.'admin/userrequest/publish/'.$id;
				$idurl = $id.'-'.$delUrl;
				$rr['delete'] = "<a id='" . $idurl. "' class='del' onclick='del(this.id)'> Delete </a>";
				$rr['publish'] = "<a href='" . $pubUrl. "' class='del'> publish </a>";	
				$data[] =$r['id'];
				$data[] =$r['type'];
				$data[] =$cityname;
				$data[] =$r['name'];
				$data[] =$strImage;
				$data[] =$strLogo;
				$data[] =$r['created_date'];
				$data[] =$status;	
				$data[] =  anchor('admin/userrequest/add/' . $r['id'], 'Edit');
				$data[] =  $rr['publish'];
				$data[] =  $rr['delete'];
				$output['aaData'][] = $data;
			}
			echo json_encode($output);
		}
    }
	// end server side processing
	// to add/edit userrequest data
	function add($id=''){ 	
		if(!$this->session->userdata('admin_logged_in')){
			redirect(base_url().'admin/login/');
		}else{	
			if($this->input->post('cancel'))
			{
				redirect(base_url().'admin/userrequest/');
			}
			$data = $this->userrequest_model->form();
			$cities = gettable('city');
			foreach ($cities as $city){
				$data['allCity'][$city['id']] = $city['name'];
			}
			$tags = gettable('tag');
			foreach ($tags as $tag){
				$data['allTag'][$tag['id']] = $tag['keyword'];
			}
			$data['allType'] = array('restaurant'=>'restaurant','hotel'=>'hotel');
			$data['title'] = 'User Request form';
			$data['id'] = $id;
			$data['error'] = '';
			$data['fileName'] = '';
			$data['logoName'] = '';
			if($id > 0){
				$userrequest = gettable('user_request','id',$id);
				if($userrequest){
					$created = explode('-', $userrequest[0]['created_date']);
					$data['fName']['value'] = $userrequest[0]['name'];
					$data['fPrice']['value'] = $userrequest[0]['price'];
					$data['fPhone']['value'] = $userrequest[0]['phone'];
					$data['fFax']['value'] = $userrequest[0]['fax'];
					$data['fEmail']['value'] = $userrequest[0]['email'];
					$data['fWebsite']['value'] = $userrequest[0]['website'];
					$data['fAddress']['value'] = $userrequest[0]['address'];
					$data['fImage']['value'] = $userrequest[0]['image'];
					$data['selectedType'] = $userrequest[0]['type'];
					$data['fLogo']['value'] = $userrequest[0]['logo'];
					$data['fCdate']['value'] = $created[2].'/'.$created[1].'/'.$created[0];
					$data['selectedCity'] 		= $userrequest[0]['city'];
					$filename = explode('upload/',$userrequest[0]['image_url']);
					$data['fileName'] = $filename[1];
				}else{
					redirect(base_url().'admin/userrequest/');
				}
			}
			$this->form_validation->set_rules('name','Name','trim|required|xss_clean');
			$this->form_validation->set_rules('address','Address','trim|required|xss_clean');
			$this->form_validation->set_rules('email','Email','trim|required|xss_clean');
			$this->form_validation->set_rules('phone','Phone','trim|required|xss_clean');
			$this->form_validation->set_rules('fax','Fax','trim|required|xss_clean');
			$this->form_validation->set_rules('website','Website','trim|required|xss_clean');
			$this->form_validation->set_rules('price','Price range','trim|required|xss_clean');
			
			if($this->input->post('submit')){					
				if($this->form_validation->run()===TRUE){					
					if($id == 0 || ($id > 0 && $_FILES['upload']['name'] != '')||($_FILES['logo']['name'] != '')){
						$config['upload_path'] = './images/userrequest/upload';
						$config['allowed_types'] = 'gif|jpg|png';
						$config['max_size']	= '1024';
						$config['max_width']  = '1024';
						$config['max_height']  = '768';
						$this->load->library('upload', $config);
						$upload = $this->upload->do_upload('upload');
						
						foreach ($_FILES as $key => $value) {				
							if (!empty($value['tmp_name'])) {					
								if ( ! $this->upload->do_upload($key)) {									
									$data['error'] = $this->upload->display_errors();
								} else {
									$data =  $this->upload->data();
									$image[]= $data['file_name'];
									$fullpath[]= $data['full_path'];
									$aa [] = $this->upload->data();
								}
							}
					   }
						if($_FILES['upload']['name'] != '' && $_FILES['logo']['name'] != ''){
							$fullpath1 = $fullpath[0];				
							$fullpath1 = explode('travel/',$fullpath1);
							$fullpath1 = base_url().$fullpath1[1];				
							$fullpath2 = $fullpath[1];
							$fullpath2 = explode('travel/',$fullpath2);
							$fullpath2 = base_url().$fullpath2[1];
							$name1 = $image[0];
							$name2 = $image[1];
						}else if($_FILES['logo']['name'] != ''){
							$name1 = $userrequest[0]['image'];
							$fullpath1 = $userrequest[0]['image_url'];				
							$fullpath2 = $fullpath[0];
							$fullpath2 = explode('travel/',$fullpath2);
							$fullpath2 = base_url().$fullpath2[1];	
							$name2 = $image[0];
						}else if($_FILES['upload']['name'] != ''){
							$name2 = $userrequest[0]['logo'];
							$fullpath2 = $userrequest[0]['logo_url'];				
							$fullpath1 = $fullpath[0];
							$fullpath1 = explode('travel/',$fullpath1);
							$fullpath1 = base_url().$fullpath1[1];	
							$name1 = $image[0];
						}
						$result = $this->userrequest_model->save($id,$name1,$name2,$fullpath1,$fullpath2);
						if($result)
						{
							redirect(base_url().'admin/userrequest/');
						}								
							
					}else{
						$image1 = $userrequest[0]['image'];	
						$fullpath1 = $userrequest[0]['image_url'];
						$image2 = $userrequest[0]['logo'];
						$fullpath2 = $userrequest[0]['logo_url'];
						$result = $this->userrequest_model->save($id,$image1,$image2,$fullpath1,$fullpath2);
						if($result)
						{
							redirect(base_url().'admin/userrequest/');
						}
					}
				}
				$data['fName']['value'] 	= $this->input->post('name');
				$data['fAddress']['value'] 	= $this->input->post('address');
				$data['fLongitude']['value'] 	= $this->input->post('longitude');
				$data['fLatitude']['value'] 	= $this->input->post('latitude');
				$data['fDesc']['value'] 	= $this->input->post('desc');
				$data['fCdate']['value'] 	= $this->input->post('cDate');
				$data['fUdate']['value'] 	= $this->input->post('uDate');	
			}
			$this->load->view('admin/template/header', $data); 
			$this->load->view('admin/userrequest/index', $data); 
			$this->load->view('admin/template/footer');
		}
	}	
	function delete(){
		if(!$this->session->userdata('admin_logged_in')){
			redirect(base_url().'admin/login/');
		}else{
			$id = $_REQUEST['id'];
			$result = $this->userrequest_model->delete($id);
			echo json_encode(array('status'=>$result));	
		}
	}
	function publish($id){
		if(!$this->session->userdata('admin_logged_in')){
			redirect(base_url().'admin/login/');
		}else{
			$result = $this->userrequest_model->publish($id);
			if($result == 'publish'){
				$data['error'] 	= 'This record is already published . You can not publish again';
			}elseif($result == 'unpublish'){
				$data['msg'] 	= 'you have successfully published this record ';
				
			}else{
				$data['msg'] 	= 'you can not publish';
			}
			$this->session->set_flashdata('data', $data);
			redirect(base_url().'admin/userrequest/');
		}
	}
}
?>