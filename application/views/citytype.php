<?php
	$query = $this->db->get_where('city',array('id'=>$id));
	$result =  $query->result_array();
	$desc = $result[0]['description'];
	$name = $result[0]['name'];
	
	$query_hotel = $this->db->get_where('hotel',array('city'=>$id));
	$result_hotle =  $query_hotel->result_array();
	
	$query_attraction = $this->db->get_where('attraction',array('city'=>$id));
	$result_attraction =  $query_attraction->result_array();
	
	$query_restaurant = $this->db->get_where('restaurant',array('id'=>$id));
	$result_restaurant =  $query->result_array();
	
	$query_activity = $this->db->get_where('activities',array('id'=>$id));
	$result_activity =  $query_activity->result_array();
	include 'description.php';
?>
<div class="container">
<div class="banner-bottom">
    <div class="sidebar-offcanvas" id="sidebar" role="navigation">
        <div class="list-group">
            <a href="<?php echo base_url(); ?>wheretostay/show/<?php echo $id;?>" class="list-group-item ">WHERE TO STAY</a>
            <a href="<?php echo base_url(); ?>wheretovisit/show/<?php echo $id;?>" class="list-group-item ">WHERE TO VISIT</a>
            <a href="<?php echo base_url(); ?>wheretoeat/show/<?php echo $id;?>" class="list-group-item">WHERE TO EAT</a>
            <a href="<?php echo base_url(); ?>whattodo/show/<?php echo $id;?>" class="list-group-item">WHAT TO DO</a>            
        </div>        
    </div>
    <div class="col-md-0 banner-bottom-grid" style="padding-bottom:93px !important;">
    <div class="header-desc-yangon">
        <h2><?php echo $name;?> city </h2>
        <?php echo $desc;?>
        <br/> 
        	<a>READ MORE</a>
    </div>
    </div>
</div>
