<?php
/**	
 * @file
 *  Program		: activity.php 
 * 	Author		: CMS
 * 	Date		: 17/7/2015
 * 	Abstract	: activity form
*/
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Activity extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model('admin/activity_model');
		$this->load->library(array('form_validation'));	
	}
	function index()
    {
		if(!$this->session->userdata('admin_logged_in')){
			redirect(base_url().'admin/login/');
		}else{
		$data['title'] = 'Activity List';
        $this->load->view('admin/template/header',$data);
		$this->load->view('admin/activity/activitylist');
		$this->load->view('admin/template/footer');
		}
    }
	function getdatatable()
    { 
		if(!$this->session->userdata('admin_logged_in')){
			redirect(base_url().'admin/login/');
		}else{
			$aColumns = array('id', 'name','city','image_url','address','longitude','latitude','latitude','latitude');        
			// DB table to use
			$sTable = 'activities';
				
			$iDisplayStart = $this->input->get_post('iDisplayStart', true);
			$iDisplayLength = $this->input->get_post('iDisplayLength', true);
			$iSortCol_0 = $this->input->get_post('iSortCol_0', true);
			$iSortingCols = $this->input->get_post('iSortingCols', true);
			$sSearch = $this->input->get_post('sSearch', true);
			$sEcho = $this->input->get_post('sEcho', true);
		
			// Paging
			if(isset($iDisplayStart) && $iDisplayLength != '-1')
			{
				$this->db->limit($this->db->escape_str($iDisplayLength), $this->db->escape_str($iDisplayStart));
			}        
			// Ordering
			if(isset($iSortCol_0))
			{
				for($i=0; $i<intval($iSortingCols); $i++)
				{
					$iSortCol = $this->input->get_post('iSortCol_'.$i, true);
					$bSortable = $this->input->get_post('bSortable_'.intval($iSortCol), true);
					$sSortDir = $this->input->get_post('sSortDir_'.$i, true);
		
					if($bSortable == 'true')
					{
						$this->db->order_by($aColumns[intval($this->db->escape_str($iSortCol))], $this->db->escape_str($sSortDir));
					}
				}
			}
			
			/* 
			 * Filtering
			 */
			if(isset($sSearch) && !empty($sSearch))
			{
				for($i=0; $i<count($aColumns); $i++)
				{
					$bSearchable = $this->input->get_post('bSearchable_'.$i, true);                
					// Individual column filtering
					if(isset($bSearchable) && $bSearchable == 'true')
					{
						$this->db->or_like($aColumns[$i], $this->db->escape_like_str($sSearch));
					}
				}
			}
			
			// Select Data
			$this->db->select('SQL_CALC_FOUND_ROWS '.str_replace(' , ', ' ', implode(', ', $aColumns)), false);
			$rResult = $this->db->get($sTable);
		
			// Data set length after filtering
			$this->db->select('FOUND_ROWS() AS found_rows');
			$iFilteredTotal = $this->db->get()->row()->found_rows;
		
			// Total data set length
			$iTotal = $this->db->count_all($sTable);
		
			// Output
			$output = array(
				'sEcho' => intval($sEcho),
				'iTotalRecords' => $iTotal,
				'iTotalDisplayRecords' => $iFilteredTotal,
				'aaData' => array()
			);     
			foreach ($rResult->result_array()  as $r) {
				
				$strhref1 = "<a href='" . $r['image_url'] . "' target='_blank'>";
				$strImage = $strhref1 . "<img src='" . $r['image_url'] . "' width='20' height='20' border='0' /> </a> ";			
				$data = array();
				$id = $r['id'];
				$cityid = $r['city'];
				$city = $this->db->get_where('city',array('id'=>$cityid));
				$result = $city->result_array();		
				
				if($result){
					$cityname = $result[0]['name'];
				}else{
					$cityname = $r['city'];
				}
				
				$baseurl =  base_url();	
				$delUrl =  $baseurl.'admin/activity/delete/';
				$idurl = $id.'-'.$delUrl;
				$rr['delete'] = "<a id='" . $idurl. "' class='del' onclick='del(this.id)'> Delete </a>";	
				$data[] =$r['id'];
				$data[] =$r['name'];
				$data[] =$cityname;
				$data[] =$strImage;
				$data[] =$r['address'];
				$data[] =$r['longitude'];
				$data[] =$r['latitude'];			
				$data[] =  anchor('admin/activity/add/' . $r['id'], 'Edit');
				$data[] =  $rr['delete'];
				$output['aaData'][] = $data;
			}
			echo json_encode($output);
		}
    }
	// end server side processing	
	// to add/edit activity data
	function add($id=''){ 
		if(!$this->session->userdata('admin_logged_in')){
			redirect(base_url().'admin/login/');
		}else{		
			if($this->input->post('cancel'))
			{
				redirect(base_url().'admin/activity/');
			}
			$data = $this->activity_model->form();
			$cities = gettable('city');
			foreach ($cities as $city){
				$data['allCity'][$city['id']] = $city['name'];
			}
			$tags = gettable('tag');
			foreach ($tags as $tag){
				$data['allTag'][$tag['id']] = $tag['keyword'];
			}
			$data['title'] = 'Activity ';
			$data['id'] = $id;
			$data['error'] = '';
			$data['fileName'] = '';
			if($id > 0){
				$activity = gettable('activities','id',$id);
				if($activity){
					$created = explode('-', $activity[0]['created_date']);
					$updated = explode('-', $activity[0]['updated_date']);
					$data['fName']['value'] = $activity[0]['name'];
					$data['fAddress']['value'] = $activity[0]['address'];
					$data['fDesc']['value'] = $activity[0]['description'];
					$data['fLongitude']['value'] = $activity[0]['longitude'];
					$data['fLatitude']['value'] = $activity[0]['latitude'];					
					$data['selectedCity'] 		= $activity[0]['city'];
					$data['selectedTagId'] 		= $activity[0]['tag'];
					$data['fCdate']['value'] = $created[2].'/'.$created[1].'/'.$created[0];
					$data['fUdate']['value'] = $updated[2].'/'.$updated[1].'/'.$updated[0];
					$data['selectedTag'] = getcolname('tag','keyword',$data['selectedTagId']);
					$filename = explode('upload/',$activity[0]['image_url']);
					$data['fileName'] = $filename[1];
				}else{
					redirect(base_url().'admin/activity/');
				}		
			}
			$this->form_validation->set_rules('name','Name','trim|required|xss_clean');
			$this->form_validation->set_rules('address','Address','trim|required|xss_clean');
			$this->form_validation->set_rules('longitude','Longitude','trim|required|xss_clean');
			$this->form_validation->set_rules('latitude','Latitude','trim|required|xss_clean');
			$this->form_validation->set_rules('desc','Description','trim|required|xss_clean');
			$this->form_validation->set_rules('cDate','created Date','trim|required|xss_clean');
			$this->form_validation->set_rules('uDate','Updated Date','trim|required|xss_clean');
			
			if($this->input->post('submit')){					
				if($this->form_validation->run()===TRUE){		
					if($id == 0 || ($id > 0 && $_FILES['upload']['name'] != '')){
						$config['upload_path'] = './images/activity/upload';
						$config['allowed_types'] = 'gif|jpg|png';
						$config['max_size']	= '1024';
						$config['max_width']  = '1024';
						$config['max_height']  = '768';
						$this->load->library('upload', $config);
						$upload = $this->upload->do_upload('upload');
						if(!$upload){
							$data['error'] = $this->upload->display_errors();
						}else{	
							$fullpath = $this->upload->data('full_path');
							$fullpath = explode('travel/',$fullpath);
							$fullpath = base_url().$fullpath[1];
							$result = $this->activity_model->save($id,$fullpath);
							if($result)
							{
								redirect(base_url().'admin/activity/');
							}								
						}
					}else{	
						$fullpath = $activity[0]['image_url'];
						$result = $this->activity_model->save($id,$fullpath);
						if($result)
						{
							redirect(base_url().'admin/activity/');
						}
					}
				}
				$data['fName']['value'] 	= $this->input->post('name');
				$data['fAddress']['value'] 	= $this->input->post('address');
				$data['fDesc']['value'] 	= $this->input->post('desc');
				$data['fLatitude']['value'] 	= $this->input->post('latitude');
				$data['fLongitude']['value'] 	= $this->input->post('longitude');
				$data['selectedTag'] 	= $this->input->post('tags');
			}
			$this->load->view('admin/template/header', $data); 
			$this->load->view('admin/activity/index', $data); 
			$this->load->view('admin/template/footer');
		}
	}
	// export data as csv file
	function export(){
		if(!$this->session->userdata('admin_logged_in')){
			redirect(base_url().'admin/login/');
		}else{
			$startDate = date('Y-m-d 00:00:01');
			$start = strtotime($startDate);
			$endDate = date('Y-m-d 23:59:59');
			$end = strtotime($endDate);
			$activity = gettable('activities');
			$data['fileName'] = 'activity'.date('dmy');
			$data['activity'] = array();
			$data['activity'] = $activity;
			$this->load->view('admin/activity/export', $data);
		}
		
	}
	// import data as excel file
	function import(){
		if(!$this->session->userdata('admin_logged_in')){
			redirect(base_url().'admin/login/');
		}else{
			if($this->input->post('cancel'))
			{
				redirect(base_url().'admin/activity/');
			}
			$data['error'] = '';
			if($this->input->post('cancel'))
			{
				redirect(base_url().'admin/activity/');
			}
			if($this->input->post('submit')){		
				$config['upload_path'] = './images/activity/upload/table';
				$config['allowed_types'] = 'csv';
				$config['max_size']	= '1024';
				$config['max_width']  = '1024';
				$config['max_height']  = '768';
				$this->load->library('upload', $config);
				$upload = $this->upload->do_upload('upload');
				if(!$upload){
					$data['error'] = $this->upload->display_errors();
				}else{
					$fileName = $this->upload->data('file_name');
					$this->load->library('csvreader');	
					$fullpath = $this->upload->data('full_path');
					$data['csvData'] = $this->csvreader->parse_file($fullpath);
						foreach($data['csvData'] as $cd)
						{
							if (isset($cd['cityId'],$cd['tagId'],$cd['description'],$cd['address'],$cd['name'],$cd['longitude'],$cd['latitude'])){
								$id = $cd['id'];
								$results_array = array(
											  'city' => $cd['cityId'],
											  'tag' => $cd['tagId'],
											  'description' => $cd['description'],
											  'address' => $cd['address'],
											  'name' => $cd['name'],
											  'longitude' => $cd['longitude'],
											  'latitude' => $cd['latitude'],
											  'created_date' => date('Y-m-d'),
											  'updated_date' => date('Y-m-d')
											   ); 
						
							  $query = $this->db->get_where('activities',array('id'=>$id));
							  if($query->num_rows() > 0){
								  $this->db->where('id',$id);
								  $result = $this->db->update('activities',$results_array);
							  }else{				  
								  $result = $this->db->insert('activities', $results_array);
							  }								  
							}else{
								$data['error'] = "Please check the column of your csv file";
								$result = false;
							}
						}
						if($result){
							 redirect(base_url().'admin/activity/');
						}
				}	
			}	
			$this->load->view('admin/template/header',$data); 
			$this->load->view('admin/activity/import');
			$this->load->view('admin/template/footer');	
		}
	}
	function delete(){
		if(!$this->session->userdata('admin_logged_in')){
			redirect(base_url().'admin/login/');
		}else{
			$id = $_REQUEST['id'];
			$result = $this->activity_model->delete($id);
			echo json_encode(array('status'=>$result));
		}
	}
}
?>