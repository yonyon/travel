<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Details extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model('details_model');
		$this->load->helper(array('html', 'url'));
		$this->load->library(array('form_validation'));
	}
	public function show($type, $id)
	{
		//echo $type.', '.$id;
		if($type == 'tripplan') $type = 'trip_plan';
		elseif($type == 'essinfo') $type = 'information';
		$data = '';
		$data['info'] = $this->details_model->getData($type, $id);
		if($type == 'information' || $type == 'trip_plan' || $type == 'hotel' || $type == 'attraction' || $type == 'restaurant' || $type == 'activities'){
			//$data['essInfo'] = $this->details_model->getData('information', 0);
			$cityId = $data['info'][0]['city'];
			$data['releatedInfo'] = $this->details_model->getData($type, $id, $cityId);
		}
		
		$data['id'] = $id;
		$data['type'] = $type;
		if($type == 'trip_plan') $data['menu'] = 'trip plan';
		else if($type == 'information') $data['menu'] = 'essential information';
		else if($type == 'tip') $data['menu'] = 'tips';
		else $data['menu'] = $type;
		$data['message'] = '';
		$data['error'] = false;
		if($this->input->post()){
			$this->form_validation->set_rules('email', 'Email', 'required|valid_email');
			if($this->form_validation->run() === TRUE){	
				$checkEmail = $this->details_model->checkEmail($this->input->post('email'));
				if($checkEmail){
					$subscribeSuccess = $this->details_model->subscribe($this->input->post('email'));
					if($subscribeSuccess) $data['message'] = 'Thanks! You have subscribed successfully.';
				}else{
					$data['error'] = true;
					$data['message'] = 'Email already exist.Please try other one.';
				}
				
			}else{
				$data['error'] = true;
				$data['message'] = 'Invalid email address.';
			}
		}
		//echo '<pre>';print_r($data);echo '</pre>';
		$this->load->view('template/header', $data);
		$this->load->view('details', $data);
		$this->load->view('template/footer');
	}
}
?>