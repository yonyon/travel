<?php
/**	
 * @file
 *  Program		: user_model.php 
 * 	Author		: CMS
 * 	Date		: 23/07/2015
 * 	Abstract	: user
*/
class User_model extends CI_Model{
	function user_model(){
		parent::__construct();
	}
	// to create form
	function form(){		
		$data['name']    ='Name<span class="star_red">*</span> :';	
		$data['country']    ='Country<span class="star_red">*</span> :';	
		$data['email']    ='Email<span class="star_red">*</span> :';			
		$data['cDate']     ='created Date <span class="star_red">*</span> :';
		$data['uDate']     ='updated Date <span class="star_red">*</span> :';	
		
		$data['fName']		= array('name'=>'name', 'id'=>'name', 'value'=>'', 'rows'=>'8');	
		$data['fCountry']		= array('name'=>'country', 'id'=>'country', 'value'=>'', 'rows'=>'8');	
		$data['fEmail']		= array('name'=>'email', 'id'=>'email', 'placeholder'=>'Enter your Email','value'=>'', 'rows'=>'8');		
		$data['fCdate']	= array('name'=>'cDate', 'id'=>'cDate', 'placeholder'=>'dd/mm/yyyy', 'value'=>'', 'class'=>'txtFromDate c_in');
		$data['fUdate']	= array('name'=>'uDate', 'id'=>'uDate', 'placeholder'=>'dd/mm/yyyy', 'value'=>'', 'class'=>'txtToDate c_out');		
		return $data;
	}
	// to save user data
	function save($id){
		$name 	= $this->input->post('name');
		$email 	= $this->input->post('email');
		$country 	= $this->input->post('country');
		$cDate 		= $this->input->post('cDate');
		$uDate 		= $this->input->post('uDate');
		if(!empty($cDate)){
			$created = explode('/', $cDate);	
			$cDate = $created[2].'-'.$created[1].'-'.$created[0];
		}
		if(!empty($uDate)){
			$updated = explode('/', $uDate);	
			$uDate = $updated[2].'-'.$updated[1].'-'.$updated[0];
		}
		$data = array(
					'name'=>$name,
					'email'=>$email,
					'country'=>$country,
					'created_date'=>$cDate,
					'updated_date'=>$uDate
				);
		if(!empty($id) && $id > 0){
			$this->db->where('id',$id);
			$result = $this->db->update('user',$data);	
		}else{
			$result = $this->db->insert('user', $data);	
		}
		if($result){
			return $result;
		}else{
			return false;
		}
	}
	// delete function
	function delete($id){
		$this->db->where('id',$id);
		$query = $this->db->delete('user');
		if($query){
			return TRUE;
		}else{
			return FALSE;
		}
	}
}
?>