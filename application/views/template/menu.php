<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">
<link type="image/x-icon" href="<?php echo base_url();?>images/logo.jpg" rel="icon">
<title>Travel Myanmar</title>
<link href="<?php echo base_url();?>css/bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url();?>css/navbar.css" rel="stylesheet">
<script src="<?php echo base_url();?>js/jquery.min.js"></script>
<script src="<?php echo base_url();?>js/bootstrap.min.js"></script>
<script src=" <?php echo base_url() ?>js/map.js"></script>
</head>
<body>
    <div class="travel-myanmar-header travel-myanmar" style="color:white !important; z-index:10;">
        Travel<span class='myanmar' style="display:block;margin-top:-13px !important;">Myanmar</span>
    </div>
    <div class="col-md-3 pull-right">
        <form class="navbar-form" role="search">
            <div class="input-group">
                <input type="text" class="form-control" placeholder="Search" name="srch-term" id="srch-term">                
            </div>
        </form>
    </div>
    <div class="navbar navbar-fixed-top navbar-default" role="navigation">
        <div class="navbar-header">                    
            <span class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">MENU</span>                  
        </div><!-- end of navbar-header-->
        <div class="navbar-collapse collapse navbar-right">
            <ul class="nav navbar-nav">
                <li><a href="<?php echo base_url(); ?>home">Home</a></li>
                <li class="dropdown" class="upp"><a href="#" class="dropdown-toggle" data-toggle="dropdown">Cities <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <?php 
                            $query = $this->db->get('city');			
                            $results = $query->result_array();
                            foreach($results as $result)
                            {
								$linkpage=preg_replace('/\s+/', '', $result['name']);
								//$linkpage = strtolower($linkpage);
								$linkpage = 'citytype/show';
								$linkpage = base_url().$linkpage;
								echo '<li>';
								//echo '<img src="' . $movie->name . '" />';
								echo '<a href="' . $linkpage.'/'.$result['id'] . '">' .$result['name'] . '</a>';
								echo '</li>';
                            }
                        ?>
                    </ul>
                </li>
                <li><a href="#">History</a></li>
                <li><a href="#">Culture</a></li>
                <li><a href="#">Trip plan</a></li>
                <li><a href="#">Tips</a></li>
                <?php  if ($this->session->userdata('user_logged_in')){ ?>
                	<li><a href="<?php echo base_url(); ?>myaccount/show">My account</a></li>
                    <li><a href="<?php echo base_url(); ?>login/out">logout</a></li>
            		<?php }else{?>
                <li><a href="<?php echo base_url(); ?>login">Login</a></li>
                <li><a href="<?php echo base_url(); ?>register">Register</a></li>
                <?php }?>
            </ul>
        </div><!--/.nav-collapse -->
    </div><!-- end of navbar navbar-default-->
  
