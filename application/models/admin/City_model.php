<?php
/**	
 * @file
 *  Program		: city_model.php 
 * 	Author		: CMS
 * 	Date		: 24/07/2015
 * 	Abstract	: city
*/
class City_model extends CI_Model{
	function city_model(){
		parent::__construct();
	}
	// create form
	function form(){	
		$data['name']   ='Name <span class="star_red">*</span>:';
		$data['image']  ='Image <span class="star_red">*</span> :';
		$data['desc']	='Description <span class="star_red">*</span> :';
		$data['cDate'] ='created Date <span class="star_red">*</sfpan> :';
		$data['uDate'] ='updated Date <span class="star_red">*</span> :';	
		$data['fName']		= array('name'=>'name', 'id'=>'name', 'value'=>'', 'rows'=>'8');
		$data['fDesc']= array('name'=>'desc', 'id'=>'desc', 'value'=>'', 'rows'=>'8');
		$data['fImage']	= array('name'=>'upload', 'id'=>'upload', 'value'=>'');
		$data['fCdate']	= array('name'=>'cDate', 'id'=>'cDate', 'placeholder'=>'dd/mm/yyyy', 'value'=>'', 'class'=>'txtFromDate c_in');
		$data['fUdate']	= array('name'=>'uDate', 'id'=>'uDate', 'placeholder'=>'dd/mm/yyyy', 'value'=>'', 'class'=>'txtToDate c_out');
		return $data;
	}
	// save city data
	function save($id,$fullpath){
		$name 		= $this->input->post('name');
		$desc 		= $this->input->post('desc');
		$cDate 		= $this->input->post('cDate');
		$uDate 		= $this->input->post('uDate');
		if(!empty($cDate)){
			$created = explode('/', $cDate);	
			$cDate = $created[2].'-'.$created[1].'-'.$created[0];
		}
		if(!empty($uDate)){
			$updated = explode('/', $uDate);	
			$uDate = $updated[2].'-'.$updated[1].'-'.$updated[0];
		}
		$data = array(
					'name'=>$name, 
					'description'=>$desc,
					'image_url'=>$fullpath,
					'created_date'=>$cDate,
					'updated_date'=>$uDate
				);
		
		if(!empty($id) && $id > 0){
			$query1= $this->db->get_where('city',array('id'=>$id));
			$result = $query1->result_array();			
			if($fullpath != $result[0]['image_url']){
				$img = explode('upload/',$result[0]['image_url']);
				$file = $_SERVER['DOCUMENT_ROOT'] . '/travel/images/city/upload/' . $img[1];
				if (file_exists($file)) {
					unlink($file);
				}
			}
			$this->db->where('id',$id);
			$result = $this->db->update('city',$data);	
		}else{
			$result = $this->db->insert('city', $data);	
		}
		if($result){
			return $result;
		}else{
			return false;
		}
	}
		
	// delete function
	function delete($id){
		
		$del = true;
		$atraction_query = $this->db->get_where('attraction',array('city'=>$id));
		if($atraction_query->num_rows() > 0){
			$del = false;
		}
		$hotel_query = $this->db->get_where('hotel',array('city'=>$id));
		if($hotel_query->num_rows() > 0){
			$del = false;
		}
		$restaurant_query = $this->db->get_where('restaurant',array('city'=>$id));
		if($restaurant_query->num_rows() > 0){
			$del = false;
		}
		$activity_query = $this->db->get_where('activities',array('city'=>$id));
		if($activity_query->num_rows() > 0){
			$del = false;
		}
		$tripplan_query = $this->db->get_where('trip_plan',array('city'=>$id));
		if($tripplan_query->num_rows() > 0){
			$del = false;
		}
		if($del){
			$query1= $this->db->get_where('city',array('id'=>$id));
			$result = $query1->result_array();
			if($result){
				$img = explode('upload/',$result[0]['image_url']);
				$file = $_SERVER['DOCUMENT_ROOT'] .'/travel/images/city/upload/' . $img[1];
				if (file_exists($file)) {
				unlink($file);
				}
			}
			$this->db->where('id',$id);
			$query = $this->db->delete('city');
			if($query){
				return TRUE;
			}else{
				return FALSE;
			}					
		}else{
			return $del;
		}
		
	}
}
?>