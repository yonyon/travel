<?php
/**	
 * @file
 *  Program		: restaurant.php 
 * 	Author		: CMS
 * 	Date		: 17/7/2015
 * 	Abstract	: restaurant
*/
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Restaurant extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model('admin/restaurant_model');
		$this->load->library(array('form_validation'));	
	}
	function index()
    {
		if(!$this->session->userdata('admin_logged_in')){
			redirect(base_url().'admin/login/');
		}else{
		$data['title'] = 'Restaurant List';
        $this->load->view('admin/template/header',$data);
		$this->load->view('admin/restaurant/restaurantlist');
		$this->load->view('admin/template/footer');
		}
    }
	function getdatatable()
    {
		if(!$this->session->userdata('admin_logged_in')){
			redirect(base_url().'admin/login/');
		}else{
			$aColumns = array('id', 'city','tag', 'name','image_url','logo_url','price_range','latitude','longitude','updated_date');        
			// DB table to use
			$sTable = 'restaurant';			
			$iDisplayStart = $this->input->get_post('iDisplayStart', true);
			$iDisplayLength = $this->input->get_post('iDisplayLength', true);
			$iSortCol_0 = $this->input->get_post('iSortCol_0', true);
			$iSortingCols = $this->input->get_post('iSortingCols', true);
			$sSearch = $this->input->get_post('sSearch', true);
			$sEcho = $this->input->get_post('sEcho', true);
		
			// Paging
			if(isset($iDisplayStart) && $iDisplayLength != '-1')
			{
				$this->db->limit($this->db->escape_str($iDisplayLength), $this->db->escape_str($iDisplayStart));
			}
			
			// Ordering
			if(isset($iSortCol_0))
			{
				for($i=0; $i<intval($iSortingCols); $i++)
				{
					$iSortCol = $this->input->get_post('iSortCol_'.$i, true);
					$bSortable = $this->input->get_post('bSortable_'.intval($iSortCol), true);
					$sSortDir = $this->input->get_post('sSortDir_'.$i, true);
		
					if($bSortable == 'true')
					{
						$this->db->order_by($aColumns[intval($this->db->escape_str($iSortCol))], $this->db->escape_str($sSortDir));
					}
				}
			}
		   //Filtering
			if(isset($sSearch) && !empty($sSearch))
			{
				for($i=0; $i<count($aColumns); $i++)
				{
					$bSearchable = $this->input->get_post('bSearchable_'.$i, true);
					
					// Individual column filtering
					if(isset($bSearchable) && $bSearchable == 'true')
					{
						$this->db->or_like($aColumns[$i], $this->db->escape_like_str($sSearch));
					}
				}
			}
			
			// Select Data
			$this->db->select('SQL_CALC_FOUND_ROWS '.str_replace(' , ', ' ', implode(', ', $aColumns)), false);
			$rResult = $this->db->get($sTable);
		
			// Data set length after filtering
			$this->db->select('FOUND_ROWS() AS found_rows');
			$iFilteredTotal = $this->db->get()->row()->found_rows;
		
			// Total data set length
			$iTotal = $this->db->count_all($sTable);
		
			// Output
			$output = array(
				'sEcho' => intval($sEcho),
				'iTotalRecords' => $iTotal,
				'iTotalDisplayRecords' => $iFilteredTotal,
				'aaData' => array()
			);     
			foreach ($rResult->result_array()  as $r) {
				$strhref1 = "<a href='" . $r['image_url'] . "' target='_blank'>";
				$strImage = $strhref1 . "<img src='" . $r['image_url'] . "' width='20' height='20' border='0' /> </a> ";
				
				$strhref2 = "<a href='" . $r['logo_url'] . "' target='_blank'>";
				$strLogo = $strhref2 . "<img src='" . $r['logo_url'] . "' width='20' height='20' border='0' /> </a> ";
				$cityid = $r['city'];
				$city = $this->db->get_where('city',array('id'=>$cityid));
				$result = $city->result_array();
				if($result){
					$cityname = $result[0]['name'];
				}else{
					$cityname = $r['city'];
				}
				$data = array();
				$id = $r['id'];
				$baseurl =  base_url();	
				$delUrl =  $baseurl.'admin/restaurant/delete/';
				$idurl = $id.'-'.$delUrl;
				$rr['delete'] = "<a id='" . $idurl. "' class='del' onclick='del(this.id)'> Delete </a>";	
				$data[] =$r['id'];
				$data[] =$cityname;
				$data[] =$r['name'];
				$data[] =$strImage;
				$data[] =$strLogo;
				$data[] =$r['price_range'];
				$data[] =$r['latitude'];	
				$data[] =$r['longitude'];
				$data[] =$r['updated_date'];			
				$data[] =  anchor('admin/restaurant/add/' . $r['id'], 'Edit');
				$data[] =  $rr['delete'];
				$output['aaData'][] = $data;
			}
			echo json_encode($output);
		}
    }
	// end server side processing
	
	// to add/edit restaurant data
	function add($id=''){ 
		if(!$this->session->userdata('admin_logged_in')){
			redirect(base_url().'admin/login/');
		}else{	
			if($this->input->post('cancel'))
			{
				redirect(base_url().'admin/restaurant/');
			}
			$data = $this->restaurant_model->form();
			$data['title'] = 'restaurant ';
			$data['id'] = $id;
			$data['error'] = '';
			$data['fileName'] = '';
			$data['logoName'] = '';
			
			$cities = gettable('city');
			foreach ($cities as $city){
				$data['allCity'][$city['id']] = $city['name'];
			}
			$tags = gettable('tag');
			foreach ($tags as $tag){
				$data['allTag'][$tag['id']] = $tag['keyword'];
			}
			$data['selectedTag'] = '';
			$data['selectedCity'] = '';
			if($id > 0){
				$restaurant =  gettable('restaurant','id',$id);
				if($restaurant){
					$created = explode('-', $restaurant[0]['created_date']);
					$updated = explode('-', $restaurant[0]['updated_date']);
					$data['fClass']['value'] = $restaurant[0]['classification'];
					$data['fName']['value'] = $restaurant[0]['name'];
					$data['fDesc']['value'] = $restaurant[0]['description'];
					$data['fPromo']['value'] = $restaurant[0]['promotion'];
					$data['fAddress']['value'] = $restaurant[0]['address'];
					$data['fPrice']['value'] = $restaurant[0]['price_range'];				
					$data['fPhone']['value'] = $restaurant[0]['phone'];
					$data['fFax']['value'] = $restaurant[0]['fax'];
					$data['fEmail']['value'] = $restaurant[0]['email'];
					$data['fWebsite']['value'] = $restaurant[0]['website'];
					$data['fLatitude']['value'] = $restaurant[0]['latitude'];
					$data['fLongitude']['value'] = $restaurant[0]['longitude'];
					$data['selectedTagId'] = $restaurant[0]['tag'];
					$data['fCdate']['value'] = $created[2].'/'.$created[1].'/'.$created[0];
					$data['fUdate']['value'] = $updated[2].'/'.$updated[1].'/'.$updated[0];	
					$data['selectedTag'] = getcolname('tag','keyword',$data['selectedTagId']);
					$filename = explode('upload/',$restaurant[0]['image_url']);
					$data['fileName'] = $filename[1];
				}else{
					redirect(base_url().'admin/restaurant/');
				}
			}
			$this->form_validation->set_rules('name','Name','trim|required|xss_clean');
			$this->form_validation->set_rules('address','Address','trim|required|xss_clean');
			$this->form_validation->set_rules('price','Price','trim|required|xss_clean');
			$this->form_validation->set_rules('longitude','longitude','trim|required|xss_clean');
			$this->form_validation->set_rules('latitude','latitude','trim|required|xss_clean');
			$this->form_validation->set_rules('desc','Description','trim|required|xss_clean');
			$this->form_validation->set_rules('cDate','created Date','trim|required|xss_clean');
			$this->form_validation->set_rules('uDate','Updated Date','trim|required|xss_clean');
			if($this->input->post('submit')){
				if($this->form_validation->run()===TRUE){	
					if($id == 0 || ($id > 0 && $_FILES['upload']['name'] != '')){
						$config['upload_path'] = './images/restaurant/upload';
						$config['allowed_types'] = 'gif|jpg|png';
						$config['max_size']	= '1024';
						$config['max_width']  = '1024';
						$config['max_height']  = '768';
						$this->load->library('upload');
						$this->upload->initialize($config);
						$upload = $this->upload->do_upload('upload');
						if(!$upload){					
							$data['error'] = $this->upload->display_errors();							
						}else{
							$fullpath1 = $this->upload->data('full_path');					
							$configlogo['upload_path'] = './images/restaurant/upload/logo/';
							$configlogo['allowed_types'] = 'gif|jpg|png';
							$configlogo['max_size']	= '800';
							$configlogo['max_width']  = '800';
							$configlogo['max_height']  = '800';
							$this->load->library('upload');
							$this->upload->initialize($configlogo);
							$logoupload = $this->upload->do_upload('logo');				
							if(!$logoupload){	
								$data['error'] = $this->upload->display_errors();								
							}else{	
								$fullpath2 = $this->upload->data('full_path');			
								$fullpath1 = explode('travel/',$fullpath1);
								$fullpath1 = base_url().$fullpath1[1];				
								$fullpath2 = explode('travel/',$fullpath2);
								$fullpath2 = base_url().$fullpath2[1];
								$result = $this->restaurant_model->save($id,$fullpath1,$fullpath2);
								if($result)
								{
									redirect(base_url().'admin/restaurant/');
								}	
							}													
						}
						
					}else{
						$fullpath1 =$restaurant[0]['image_url'];
						$fullpath2 =$restaurant[0]['image_url'];					
						$result = $this->restaurant_model->save($id,$fullpath1,$fullpath2);
						if($result)
						{
							redirect(base_url().'admin/restaurant/');
						}						
					}
				}
				$data['fName']['value'] 	= $this->input->post('name');
				$data['fAddress']['value'] 	= $this->input->post('address');
				$data['fPhone']['value'] = $this->input->post('phone');
				$data['fFax']['value'] = $this->input->post('fax');
				$data['fEmail']['value'] = $this->input->post('email');
				$data['fWebsite']['value'] = $this->input->post('website');
				$data['fPrice']['value'] 	= $this->input->post('price');
				$data['fLongitude']['value'] 	= $this->input->post('longitude');
				$data['fLatitude']['value'] 	= $this->input->post('latitude');
				$data['fDesc']['value'] 	= $this->input->post('desc');
				$data['fCdate']['value'] 	= $this->input->post('cDate');
				$data['fUdate']['value'] 	= $this->input->post('uDate');
				$data['selectedTag'] 	= $this->input->post('tags');
			}
			$this->load->view('admin/template/header', $data); 
			$this->load->view('admin/restaurant/index', $data); 
			$this->load->view('admin/template/footer');
		}
	}
	// export data as csv file
	function export(){
		if(!$this->session->userdata('admin_logged_in')){
			redirect(base_url().'admin/login/');
		}else{
			$startDate = date('Y-m-d 00:00:01');
			$start = strtotime($startDate);
			$endDate = date('Y-m-d 23:59:59');
			$end = strtotime($endDate);
			
			$restaurant =  gettable('restaurant');
			$data['fileName'] = 'restaurant'.date('dmy');
			$data['restaurant'] = array();
			$data['restaurant'] = $restaurant;
			$this->load->view('admin/restaurant/export', $data);
		}		
	}
	// to import table
	function import(){
		if(!$this->session->userdata('admin_logged_in')){
			redirect(base_url().'admin/login/');
		}else{
			if($this->input->post('cancel'))
			{
				redirect(base_url().'admin/restaurant/');
			}
			$data['error'] = " ";
			if($this->input->post('submit')){		
				$config['upload_path'] = './images/restaurant/upload/table';
				$config['allowed_types'] = 'csv';
				$config['max_size']	= '1024';
				$config['max_width']  = '1024';
				$config['max_height']  = '768';
				$this->load->library('upload', $config);
				$upload = $this->upload->do_upload('upload');
				if(!$upload){
					$data['error'] = $this->upload->display_errors();
				}else{
					$fileName = $this->upload->data('file_name');
					$this->load->library('csvreader');	
					$fullpath = $this->upload->data('full_path');
					$data['csvData'] = $this->csvreader->parse_file($fullpath);				
					foreach($data['csvData'] as $cd)
					{
						if (isset($cd['cityId'],$cd['tagId'],$cd['classification'],$cd['name'],$cd['description'],$cd['promotion'],$cd['address'],$cd['price_range'],$cd['longitude'],$cd['longitude'],$cd['image_url'],$cd['phone'],$cd['fax'],$cd['email'],$cd['website'],$cd['logo_url'])){
							$id = $cd['id'];
							$results_array = array(
												  'city' => $cd['cityId'],
												  'tag' => $cd['tagId'],
												  'classification' => $cd['classification'],
												  'name' => $cd['name'],
												  'description' => $cd['description'],
												  'promotion' => $cd['promotion'],
												  'address' => $cd['address'],
												  'price_range' => $cd['price_range'],
												  'longitude' => $cd['longitude'],
												  'latitude' => $cd['latitude'],
												  'phone' => $cd['phone'],
												  'email' => $cd['email'],
												  'fax' => $cd['fax'],											  
												  'website' => $cd['website'],											 
												  'logo_url' => $cd['logo_url'],
												  'image_url' => $cd['image_url'],
												  'created_date' => date('Y-m-d'),
												  'updated_date' => date('Y-m-d')
												   );  
						  $query = $this->db->get_where('restaurant',array('id'=>$id));
						  if($query->num_rows() > 0){
							  $this->db->where('id',$id);
							  $result = $this->db->update('restaurant',$results_array);
						  }else{				  
							  $result = $this->db->insert('restaurant', $results_array);
						  }
					  }else{
							$data['error'] = "Please check the column name of your csv file";
							$result = false;
					  }					  			  
					}	
					if($result){
						 redirect(base_url().'admin/restaurant/');
					}
				}	
			}
			$this->load->view('admin/template/header',$data); 
			$this->load->view('admin/restaurant/import');
			$this->load->view('admin/template/footer');	
		}
	}
	function delete(){
		if(!$this->session->userdata('admin_logged_in')){
			redirect(base_url().'admin/login/');
		}else{
			$id = $_REQUEST['id'];
			$result = $this->restaurant_model->delete($id);
			echo json_encode(array('status'=>$result));	
		}
	}
}
?>