<?php	
header("Content-type: application/csv");
header("Content-Disposition: attachment; filename=".$fileName.'.csv');
$output= '';
$output .= 'id,cityId,city_name,tagId,tag_keyword,description,name,updated_date,created_date';
$output .="\r\n";
if(is_array($tripplan) && sizeof($tripplan) > 0){
	foreach($tripplan as $key=>$value){
		
		$created_date = redatetime($value['created_date']);
		$updated_date = redatetime($value['updated_date']);
		
		$cityid = $value['city'];
		$city = $this->db->get_where('city',array('id'=>$cityid));
		$result = $city->result_array();
		if($result){
			$cityname = $result[0]['name'];
		}else{
			$cityname = $cityid;
		}
		$tagid = $value['tag'];
		$tag = $this->db->get_where('tag',array('id'=>$tagid));	
		$tag_result = $tag->result_array();
		if($tag_result){
			$keyword = $tag_result[0]['keyword'];
		}else{
			$keyword = $tagid;
		}
		$desc = strip_tags($value['description']);
		$desc = str_replace(",","-",$desc);
		$output .= $value['id'].','.$cityid.','.$cityname.','.$tagid.','.$keyword.','.$desc.','.$value['name'].','.$updated_date.','.$created_date;
		$output .="\r\n";
	}
}
echo $output;
exit;
?>
