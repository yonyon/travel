<?php
header('Content-Type: application/json; charset=utf-8');
header('content-type:text/html;charset=utf-8');
if(isset($success)){
	$result = json_encode($result); // can see myanmar font
	$result = preg_replace_callback(
		'/\\\\u([0-9a-f]{4})/i',
		function ($matches) {
			$sym = mb_convert_encoding(
					pack('H*', $matches[1]), 
					'UTF-8', 
					'UTF-16'
					);
			return $sym;
		},
		$result
	);
	$result =  $result . PHP_EOL;
	$code = str_replace("\\/", "/", $result);
	$code = strip_tags($code);
	echo $code1 = '{"code":"200","description":"Success","result":'.$code.'}';
}elseif(isset($login)){
	echo $code1 = '{"code":"200","description":"you are logged in"}';
}elseif(isset($register)){
	echo $code1 = '{"code":"200","description":"your account has been registered"}';
}elseif(isset($insert)){
	echo $code1 = '{"code":"200","description":"your favourite has been record"}';
}elseif(isset($insertrating)){
	echo $code1 = '{"code":"200","description":"your rating has been record"}';
}elseif(isset($login)){
	echo $code1 = '{"code":"200","description":"logged in"}';
}elseif(isset($invalidApi)){
	echo $code1 = '{"code":"201","description":"Invalid token key","result":"'.$result.'"}';
}elseif(isset($invaliddate)){
	echo $code1 = '{"code":"202","description":"Invalid date format","result":"'.$result.'"}';
}elseif(isset($error)){
	echo '<h1>Not Found</h1>';
	echo 'The requested document was not found on this server. ';
}elseif(isset($empty_id)){
	echo $code1 = '{"code":"203","description":"General Error","result":"'.$result.'"}';
}elseif(isset($no_id)){
	echo $code1 = '{"code":"204","description":"There is no result for this request","result":"'.$result.'"}';
}elseif(isset($alreadyfavourite)){
	echo $code1 = '{"code":"205","description":"you have already added this to your favourites "}';
}elseif(isset($email_err)){
	echo $code1 = '{"code":"206","description":"your email is not valid or already exist "}';
}elseif(isset($pass_err)){
	echo $code1 = '{"code":"207","description":"password and confirm password must be same"}';
}elseif(isset($wrong_pass)){
	echo $code1 = '{"code":"208","description":"your password is incorrect"}';
}elseif(isset($wrong_email)){
	echo $code1 = '{"code":"209","description":"your email does not exist in myanmartravel.com"}';
}elseif(isset($required_field)){
	echo $code1 = '{"code":"210","description":"All fields are required","result":"'.$result.'"}';
}elseif(isset($required_field_token)){
	echo $code1 = '{"code":"211","description":"Token field is required","result":"'.$result.'"}';
}elseif(isset($required_field_id_token)){
	echo $code1 = '{"code":"212","description":"Token and User id field are required","result":"'.$result.'"}';
}elseif(isset($required_field_api_token)){
	echo $code1 = '{"code":"213","description":"Token and api field are required","result":"'.$result.'"}';
}
?>