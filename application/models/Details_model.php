<?php
/**	
 * @file
 *  Program		: Details_model.php 
 * 	Author		: ETK
 * 	Date		: 01/03/2016
 * 	Abstract	: details model
*/
class Details_model extends CI_Model{
	function details_model(){
		parent::__construct();
	}
	function getData($type, $id, $cityId = ''){
		if($cityId != ''){
			$query = $this->db->limit(4, 0);
			$query = $this->db->get_where($type, array('city'=>$cityId, 'id!='=>$id));
			
		}else{
			if($id > 0) $query = $this->db->get_where($type, array('id'=>$id));
			else $query = $this->db->get($type);
		}
		$result = $query->result_array();
		//echo '<pre>';print_r($this->db->last_query());echo '</pre>';
		return $result;
	}
	function checkEmail($email){
		$query = $this->db->get_where('subscribe', array('email'=>$email));
		$result = $query->result_array();
		if(sizeof($result) > 0) return false;
		else return true;
	}
	function subscribe($email){
		$data = array(
					'email'=>$email
				);
		$result = $this->db->insert('subscribe', $data);
		if($result){
			return true;
		}else{
			return false;
		}	
	}
}
?>