<div class="title"><?php echo ($id > 0) ? 'Edit '.$title : 'New '.$title; ?></div>
<?php 
$attributes = array('class'=>'eventform');
echo form_open_multipart(base_url().'admin/activity/add/'.$id, $attributes);
if($error != '') $error = $error;
$implpde_tag = implode(" ",$allTag);
?>
<script type="text/javascript">
	$(document).ready(function(){
		$(".txtFromDate").datepicker({
			minDate: 0,
			maxDate: "+1530D",
			numberOfMonths: 1,
			onSelect: function(selected) {
			  $(".txtToDate").datepicker("option","minDate", selected)
			}
		});
		$(".txtToDate").datepicker({ 
			minDate: 0,
			maxDate:"+1530D",
			numberOfMonths: 1,
			onSelect: function(selected) {
			   $(".txtFromDate").datepicker("option","maxDate", selected)
			}
		});
	});
	tinymce.init({
		selector: "textarea",
		forced_root_block: false,
    });
	 $(function(){		 
		var tag = '<?php echo $implpde_tag; ?>';
	 	tag = tag.split(' ');
            $('#myULTags').tagit({				
                availableTags: tag,
                itemName: 'item',
                fieldName: 'tags[]',
				id: 'tags'
            });
		
        });
</script>
<table style="width:710px !important;">
	<tr><td class="star_red" colspan="3"><?php echo $error; ?></td><td></td></tr>
    <?php
    if($id > 0){
	?>
    <tr>
    	<td>ID : </td>
        <td><?php echo $id; ?></td>
    </tr>
    <?php
	}
	?>
      <tr>
    	<td style="padding-right:80px !important;"><?php echo $name; ?></td>
        <td>
		<?php echo form_input($fName); 
		echo form_error('name', '<div class="error">', '</div>');
		?> 
        </td>
    </tr>
    <tr>
    	<td><?php echo $city; ?></td>
        <td><?php echo form_dropdown('city', $allCity, $selectedCity); ?></td>
    </tr>
    <tr>
        <td><?php echo $tag; ?></td>
        <td>
		<?php
				if(!is_array($selectedTag)){
  				   $selectedkeyword = explode(',',$selectedTag);
			   }else{
				   $selectedkeyword = $selectedTag;
			   }
			   echo '<ul id= myULTags name=myULTags >';
			   if($selectedTag!=''){
					foreach($selectedkeyword as $key => $value) {
						echo '<li>' . $value . '</li>';	
					}
				}else{
					echo '<li> </li>';	
				}
				echo '</ul>';
			   
			?>
        </td>
    </tr> 
    <tr>
    	<td valign="top"><?php echo $address; ?></td>
        <td><?php echo form_input($fAddress); 
			echo form_error('address', '<div class="error">', '</div>');
			?>
        </td>
    </tr>
    <tr>
    	<td valign="top"><?php echo $image; ?></td>
        <td><?php echo form_upload($fImage). ' '.$fileName; ?></td>
    </tr>
    <tr>
    	<td valign="top"><?php echo $longitude; ?></td>
        <td><?php echo form_input($fLongitude);
			echo form_error('longitude', '<div class="error">', '</div>');
			?></td>
    </tr>
    <tr>    
    	<td><?php echo $latitude; ?></td>
        <td><?php echo form_input($fLatitude);
			echo form_error('latitude', '<div class="error">', '</div>');
			?></td>
    </tr>
     <tr>
    	<td valign="top"><?php echo $desc; ?></td>
        <td><?php echo form_textarea($fDesc); 
			echo form_error('desc', '<div class="error">', '</div>');
			?>
        </td>
    </tr>
    <tr>    
    	<td><?php echo $cDate; ?></td>
        <td><?php echo form_input($fCdate);
			echo form_error('cDate', '<div class="error">', '</div>');
			?></td>
    </tr>
    <tr>
    	<td><?php echo $uDate; ?></td>
        <td><?php echo form_input($fUdate);
			echo form_error('uDate', '<div class="error">', '</div>');
			?></td>
    </tr>
    
   
    
    <tr>
        <td colspan="2">
        <?php
		$submit = array('name' => 'submit', 'content'=> 'Save', 'id'=> 'submit','type'=> 'submit', 'value' => 'true', 'onclick'=>'', 'class'=>'btn btn-primary');
		echo form_button($submit);
		$cancel = array('name' => 'cancel', 'content'=> 'Cancel', 'id'=> 'submit','type'=> 'submit', 'value' => 'true', 'class'=>'btn');
		echo form_button($cancel);
		?>
        </td>
    </tr>
</table>
<?php echo form_close(); ?>