<?php
/**	
 * @file
 *  Program		: user_model.php 
 * 	Author		: CMS
 * 	Date		: 23/07/2015
 * 	Abstract	: user
*/
class Forgot_password_model extends CI_Model{
	function Forgot_password_model(){
		parent::__construct();
		$this->load->library('phpass');
	}
	function checkemail($email,$random){
		$query = $this->db->get_where('user',array('email'=>$email));
		if($query->num_rows() > 0){
			$this->db->where('email',$email);
			$data = array('pw_reset_code'=>$random,'pw_reset_status'=>1);
			$result = $this->db->update('user',$data);
			return TRUE;
		}else{
			return FALSE;
		}			
	}
	
	function checkrandom($email,$random){
		$query = $this->db->get_where('user',array('email'=>$email,'pw_reset_code'=>$random));
		$result = $query->result_array();
		if($result){
			if($result[0]['pw_reset_status'] == 1){
				if($query->num_rows() > 0){
					///$this->db->where('email',$email);
					//$data = array('pw_reset_code'=>$random);
					//$result = $this->db->update('user',$data);
					return TRUE;
				}else{
					return FALSE;
				}	
			}else{
				return FALSE;
			}
		}else{
			return FALSE;
		}
	}
	
	function changepassword($email){
		$password = $this->input->post('password');		
		$password = $this->phpass->hash($password);
		$this->db->where('email',$email);
		$data = array('password'=>$password,'pw_reset_status'=>0);
		$result = $this->db->update('user',$data);
		if($result){
			return TRUE;
		}else{
			return FALSE;
		}			
	}
}
?>