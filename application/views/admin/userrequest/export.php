<?php	
header("Content-type: application/csv");
header("Content-Disposition: attachment; filename=".$fileName.'.csv');
$output= '';
$output .= 'id,cityId,city_name,tagId,tag_keyword,name,favourite_user,rating,description,address,image,image_url,longitude,latitude,updated_date,created_date';
$output .="\r\n";
if(is_array($attraction) && sizeof($attraction) > 0){
	foreach($attraction as $key=>$value){
		$created = explode('-',$value['created_date']);	
		$updated = explode('-', $value['updated_date']);
		$created_date = $created[2].'/'.$created[1].'/'.$created[0];
		$updated_date = $updated[2].'/'.$updated[1].'/'.$updated[0];
		
		$cityid = $value['city'];
		$city = $this->db->get_where('city',array('id'=>$cityid));
		$result = $city->result_array();
		if($result){
			$cityname = $result[0]['name'];
		}else{
			$cityname = $cityid;
		}
		
		$tagid = $value['tag'];
		$tag = $this->db->get_where('tag',array('id'=>$tagid));
		$tag_result = $tag->result_array();
		if($tag_result){
			$keyword = $tag_result[0]['keyword'];
			$keyword = str_replace(",","-",$keyword);
		}else{
			$keyword = $tagid;
		}
				
		$output .= $value['id'].','.$cityid.','.$cityname.','.$tagid.','.$keyword.','.$value['name'].','.$value['favourite_user'].','.$value['rating'].','.$value['description'].','.$value['address'].','.$value['image'].','.$value['image_url'].','.$value['longitude'].','.$value['latitude'].','.$updated_date.','.$created_date;
		$output .="\r\n";
	}
}
echo $output;
exit;
?>
