<?php
/**	
 * @file
 *  Program		: City.php 
 * 	Author		: CMS
 * 	Date		: 17/7/2015
 * 	Abstract	: event form, event list
*/
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Upload extends CI_Controller {
	public function __construct(){
		parent::__construct();
		//$this->load->model('admin/city_model');
		$this->load->helper(array('html', 'url'));
		$this->load->library(array('form_validation'));	
		$this->load->library('upload', $this->config);
	}
	
if($this->upload->do_upload())
{
    echo "file upload success";
}
else
{
   echo "file upload failed";
}
	function index()
    {
		echo "upload";
		//echo "Hello";
		$this->load->view('upload');
	}
}
?>