<div class="title">Activity Table</div>
<?php 
$attributes = array('class'=>'eventform');
echo form_open_multipart(base_url().'admin/activity/addmapicon/', $attributes);
?>
<table>
	<tr><td class="star_red" colspan="3">
	<?php if(isset($error)){echo $error;}
	 ?></td><td></td></tr>   
      <tr>
    	<td>Map Icon Id</td>
        <td>
		<?php 
		$map = array('name'=>'map', 'id'=>'map', 'value'=>'');
		echo form_input($map);
		?> 
        </td>
    </tr>    
    <tr>
        <td colspan="2">
        <?php
		$submit = array('name' => 'submit', 'content'=> 'Save', 'id'=> 'submit','type'=> 'submit', 'value' => 'true', 'onclick'=>'', 'class'=>'btn btn-primary');
		echo form_button($submit);
		$cancel = array('name' => 'cancel', 'content'=> 'Cancel', 'id'=> 'submit','type'=> 'submit', 'value' => 'true', 'class'=>'btn');
		echo form_button($cancel);
		?>
        </td>
    </tr>
</table>
<?php echo form_close(); ?>