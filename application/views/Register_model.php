<?php
/**	
 * @file
 *  Program		: user_model.php 
 * 	Author		: CMS
 * 	Date		: 23/07/2015
 * 	Abstract	: user
*/
class Register_model extends CI_Model{
	function Register_model(){
		parent::__construct();
		$this->load->library('phpass');
	}
	function getById($id){
		$query = $this->db->get_where('user',array('id'=>$id));
		if($query->num_rows() > 0):
			return $query->result_array();
		endif;
	}
	function save($id=''){
		$name 	= $this->input->post('name');
		$email 	= $this->input->post('email');
		$country = $this->input->post('country');
		$password = $this->input->post('password');	
		$password = $this->phpass->hash($password);
		$query = $this->db->get_where('user',array('email'=>$email));
		if($query->num_rows() > 0){
			return 'email already exist';
		}else{
			$data = array(
					'name'=>$name,
					'email'=>$email,
					'password'=>$password,
					'country'=>$country,
					'created_date'=>date('Y-m-d')
				);
			$result = $this->db->insert('user', $data);
			if($result){
				return $result;
			}else{
				return false;
			}
		}
	}
}
?>